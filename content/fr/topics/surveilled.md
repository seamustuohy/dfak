---
layout: page
title: "Je pense que je suis surveillé"
author: Carlos Guerra, Peter Steudtner, Ahmad Gharbeia
language: fr
summary: "If you think you may be a target of digital surveillance, this workflow will guide you through questions to identify possible indicators of surveillance."
date: 2023-05
permalink: /fr/topics/surveilled
parent: /fr/
---

# I think I'm being surveilled

Récemment, nous avons vu apparaître de nombreux cas de surveillance des acteurs de la société civile, depuis l'utilisation de logiciels de harcèlement par des partenaires abusifs et la surveillance des employés par les entreprises, jusqu'à la surveillance de masse par l'État et les campagnes d'espionnage ciblées contre les activistes et les journalistes. Étant donné le large éventail de techniques et de précédents observés, il est impossible de couvrir tous les aspects de la surveillance, en particulier pour l'aide d'urgence.

Cela dit, ce flux de travail se concentrera sur les cas courants observés dans la sphère numérique et liés aux appareils des utilisateurs et à leurs comptes en ligne qui représentent des situations d'urgence pour lesquelles il est possible d'agir. Lorsqu'il n'existe pas de solution envisageable, nous ne considérons pas qu'il s'agit d'une urgence et celle-ci ne sera pas traitée en profondeur (des références seront plutôt partagées).

Voici quelques-uns des cas que ce questionnaire ne couvre pas :

- Surveillance physique. Si vous craignez d'être la cible d'une surveillance physique et que vous souhaitez obtenir de l'aide, vous pouvez vous adresser à des organisations spécialisées dans la sécurité physique dans la trousse de premiers soins numériques [page d'aide](../../support).
- Surveillance massive à l'aide de matériel tel que des caméras de surveillance dans les espaces publics, les entreprises et les lieux privés.
- Bugs matériels ne dépendant pas d'une connexion internet

Un deuxième avertissement pertinent est que, de nos jours, tout le monde est surveillé par défaut, à un degré ou à un autre :

- L'infrastructure des téléphones portables est conçue pour recueillir une quantité massive de données sur nous-mêmes, qui peuvent être utilisées par plusieurs acteurs pour connaître des détails tels que notre localisation, notre réseau de contacts, etc.
- Les services en ligne tels que les réseaux sociaux et les fournisseurs de courrier électronique recueillent également une grande quantité de données sur nous, qui peuvent être liées par exemple à des campagnes publicitaires pour nous montrer des annonces liées aux sujets de nos messages ou même en nous envoyant des messages individuels.
- De plus en plus de pays disposent de nombreux systèmes interconnectés qui utilisent notre identité et recueillent des informations sur nos interactions avec diverses institutions publiques, ce qui permet de créer un "profil" sur la base de demandes de documents concernant les impôts ou les informations sur la santé.

Ces exemples de surveillance de masse, auxquels s'ajoutent les nombreux cas d'opérations ciblées, donnent le sentiment que tout le monde (en particulier dans l'espace militant) est surveillé, ce qui peut conduire à la paranoïa et affecter notre capacité émotionnelle à faire face aux menaces réelles auxquelles nous pouvons être confrontés. Cela dit, il existe des cas spécifiques où - en fonction de notre profil de risque, du travail que nous effectuons et des capacités de nos adversaires potentiels - nous pouvons avoir des soupçons tout à fait fondés vis à vis du fait d'être surveillés. Si c'est votre cas, poursuivez votre lecture.

Troisièmement, de nombreux systèmes de surveillance (en particulier les plus ciblés) ne laissent que peu ou pas d'indicateurs accessibles aux victimes, ce qui rend la détection difficile, voire impossible. Gardez cela à l'esprit lorsque vous naviguez dans le questionnaire, et si vous pensez toujours que votre cas n'est pas couvert ou qu'il est plus complexe, veuillez contacter les organisations listées à la fin du questionnaire.

Enfin, n'oubliez pas que les efforts de surveillance peuvent être massifs ou ciblés. Les opérations de surveillance massive couvrent généralement des populations entières ou de grandes parties de celles-ci, comme tous les utilisateurs d'une plateforme, tous les habitants d'une région spécifique, etc. Ces opérations s'appuient généralement davantage sur la technologie et sont habituellement moins dangereuses que la surveillance ciblée, qui vise une personne ou un petit nombre de personnes, nécessite des ressources spécifiques (argent, temps et/ou capacité) et est habituellement plus dangereuse que la surveillance massive en raison des motivations qui sous-tendent l'affectation de ressources à la surveillance de l'activité et des communications de personnes spécifiques.

Si vous avez trouvé un indice selon lequel vous pensez être sous surveillance, cliquez sur "Démarrer" pour lancer le processus.

## Workflow

### start

> Si vous êtes ici, vous avez probablement des raisons de penser que vos communications, votre localisation ou d'autres activités en ligne peuvent être surveillées. Vous trouverez ci-dessous une série d'indicateurs courants d'activités suspectes liées à la surveillance. Cliquez sur votre cas pour continuer.

Compte tenu des informations fournies dans l'introduction, quel type de problème rencontrez-vous ?

 - [J'ai trouvé un dispositif de suivi de ma localisation près de moi](#tracking-device-intro)
 - [Mon appareil agit de manière suspecte](#device-behaviour-intro)
 - [Je suis redirigé vers des sites web http non protégés pour installer des mises à jour ou des applications](#suspicious-redirections-intro)
 - [Je reçois des messages de sécurité et des alertes de la part de services fiables](#security-alerts-intro)
 - [J'ai trouvé un appareil suspect connecté à mon ordinateur ou à mon réseau](#suspicious-device-intro)
 - [Des informations que j'ai partagées en privé par courrier électronique, messagerie ou autre sont connues de l'adversaire](#leaked-internet-info-intro)
 - [Des informations confidentielles partagées par le biais d'appels téléphoniques ou de SMS semblent avoir été divulguées à l'adversaire (en particulier lors d'événements sensibles)](#leaked-phone-info-intro)
 - [Des personnes de mon entourage ont été ciblées avec succès par des mesures de surveillance](#peer-compromised-intro)
 - [Autre chose / Je ne sais pas](#autre-intro)

### tracking-device-intro

> Si vous trouvez un appareil inattendu près de vous et que vous pensez qu'il s'agit d'un dispositif de surveillance, la première étape consisterait à confirmer que l'appareil est bien là pour vous suivre et qu'il ne remplit pas d'autres fonctions pertinentes. Avec l'introduction des appareils de l'internet des objets (IOT), il est plus courant de trouver des appareils effectuant un large éventail d'actions que nous pouvons oublier après les avoir utilisés pendant un certain temps. L'une des premières étapes pour s'assurer qu'il s'agit bien d'un dispositif de surveillance consiste à inspecter l'appareil pour y trouver des informations sur la marque, le modèle, etc. Parmi les dispositifs de surveillance courants disponibles à bas prix sur le marché, citons les Airtags d'Apple, les SmartTag de Samsung Galaxy ou les traceurs Tile. Ces dispositifs sont vendus à des fins légitimes, par exemple pour aider à retrouver des objets personnels. Toutefois, des acteurs malveillants peuvent en abuser pour pister d'autres personnes sans leur consentement.
>
> Pour vérifier si nous sommes suivis par un traqueur inattendu, outre sa localisation physique, il peut être utile d'envisager certaines solutions spécifiques :
>
> - Pour Apple Airtags, dans iOS [vous devriez recevoir une notification automatique après que l'appareil inconnu a été détecté pendant un certain temps](https://support.apple.com/fr-fr/HT212227).
> - Pour Android, il existe une [application permettant de vérifier la présence d'Airtags à proximité](https://play.google.com/store/apps/details?id=com.apple.trackerdetect).
> - Pour les appareils Tile, l'[application Tile](https://fr.tile.com/) dispose d'une fonction permettant de vérifier la présence de traceurs inconnus.
> - Pour les SmartTags Samsung Galaxy, vous pouvez utiliser l'[application SmartThings](https://www.samsung.com/fr/smartthings/do-the-smartthings/) pour une fonction similaire.
>
> Les traceurs GPS sont un autre type de dispositif que vous pouvez trouver. Ils enregistrent fréquemment l'emplacement de la cible et, dans certains cas, peuvent transmettre les informations par l'intermédiaire d'une ligne téléphonique ou stocker les emplacements dans une sorte de mémoire interne qui peut être collectée physiquement plus tard pour être téléchargée et analysée. Si vous trouvez un appareil de ce type, il est essentiel de comprendre son mode de fonctionnement pour avoir une meilleure idée de qui a installé l'appareil et de la manière dont il obtient les informations.
>
> Pour tout type de dispositif de surveillance que vous trouvez, vous devez tout documenter : la marque, le modèle, s'il se connecte au réseau sans fil, l'endroit exact où vous l'avez trouvé, tout nom de l'appareil dans l'application concernée, etc.

Après avoir identifié le dispositif, vous pouvez suivre certaines stratégies. Que voulez-vous faire de l'appareil ?

 - [Je veux le désactiver](#tracking-disable-device)
 - [Je voudrais l'utiliser pour tromper l'opérateur](#tracking-use-device)

### tracking-disable-device

> Selon le type et la marque de l'appareil que vous avez trouvé, vous pourrez peut-être l'éteindre ou le désactiver complètement. Dans certains cas, cela peut se faire par le biais d'applications de traçage telles que celles décrites à l'étape précédente, mais aussi par le biais d'interrupteurs physiques, ou même en endommageant l'appareil. Gardez à l'esprit que si vous souhaitez mener une enquête plus approfondie sur cet incident, la désactivation de l'appareil pourrait altérer les preuves pertinentes stockées dans l'appareil.

Souhaitez-vous prendre d'autres mesures pour identifier le propriétaire de l'appareil ? (*veuillez noter que vous devrez garder le dispositif de traçage en ligne pour ce faire*)

 - [Oui](#identify-device-owner)
 - [Non](#pre-closure)

### tracking-use-device

> Une stratégie courante pour lutter contre les dispositifs de surveillance consiste à leur faire suivre quelque chose de différent de ce qui était prévu. Par exemple, vous pouvez le laisser dans une position statique sûre pendant que vous vous rendez dans d'autres endroits, ou vous pouvez même le faire suivre une cible différente dans un parcours qui n'a rien à voir avec vous. N'oubliez pas que, dans certains de ces scénarios, vous pourriez perdre l'accès à l'appareil et que le fait de faire suivre une autre personne par le traceur ou de le faire suivre à un endroit spécifique peut avoir des implications juridiques et de sécurité.

Souhaitez-vous prendre d'autres mesures pour identifier le propriétaire de l'appareil ? (*veuillez noter que vous devrez garder le dispositif de traçage en ligne pour ce faire*)

 - [Oui](#identify-device-owner)
 - [Non](#pre-closure)

### identify-device-owner

> L'identification du propriétaire du dispositif de surveillance peut s'avérer difficile dans certains cas, et cela dépend en grande partie du type de dispositif utilisé. Voici quelques exemples :
>
> - Pour les traceurs sans fil grand public :
>     - Comment apparaissent-ils dans les applications de contrôle ?
>     - Quand avez-vous reçu une notification ?
>     - Existe-t-il un moyen d'extraire davantage de données de l'appareil susceptibles de contenir des informations pertinentes ?
> - Pour les traceurs plus spécialisés, comme les traceurs GPS :
>     - Disposent-ils d'une mémoire interne ? Certains modèles sont dotés d'une carte SD qui peut être extraite pour voir quand l'appareil a commencé à capturer des données, ou toute autre information pertinente.
>     - Disposent-ils d'une ligne téléphonique ? Pouvez-vous extraire une carte SIM de l'appareil et essayer de l'insérer dans un autre téléphone pour obtenir le numéro ? Avec cette information, pouvez-vous savoir qui est le propriétaire de cette ligne téléphonique ?
> - En général
>     - Recherchez des indices physiques, tels que des noms inscrits sur l'appareil, des numéros d'inventaire, etc.
>     - Si l'appareil se connecte au réseau, a-t-il un nom spécifique qui peut contenir des données d'identification ?
>     - Qui a pu accéder à l'endroit où vous avez trouvé l'appareil ? En connaissant l'endroit, quand a-t-il pu y être placé ?

Souhaitez-vous engager une action en justice contre le propriétaire du dispositif de surveillance ?

 - [Oui](#legal-action-device-owner)
 - [Non](#pre-closure)

### legal-action-device-owner

> Si vous souhaitez intenter une action en justice contre le propriétaire du dispositif de surveillance, vous devez vérifier quelle est la procédure à suivre dans votre juridiction. Dans certains cas, il faudra faire appel à un avocat, dans d'autres, il suffira de s'adresser à un poste de police pour entamer la procédure. Le seul conseil commun à tous les cas est que la solidité d'un dossier dépend des preuves que vous réunissez. Un bon point de départ consiste à suivre les conseils fournis dans les étapes précédentes de ce questionnaire sur les éléments à documenter. Vous trouverez plus d'informations et d'exemples dans la trousse de premiers soins numériques [documenter les urgences numériques](/../../documentation).

Avez-vous besoin d'aide pour engager une action en justice contre le propriétaire du dispositif de traçage ?

 - [Oui, j'ai besoin d'un soutien juridique](#legal_end)
 - [Non, je pense avoir résolu mes problèmes](#resolved_end)
 - [Non, mais j'aimerais envisager d'autres scénarios de surveillance](#pre-closure)

### device-behaviour-intro

> L'une des techniques de surveillance ciblée les plus courantes à ce jour consiste à infecter des téléphones ou des ordinateurs avec des logiciels espions - des logiciels malveillants conçus pour surveiller et transmettre l'activité à d'autres personnes. Les logiciels espions peuvent être implantés par différents acteurs et selon différentes méthodes, comme par exemple le téléchargement manuel d'une application d'espionnage (communément appelée "stalkerware" ou "spouseware") dans l'appareil par un partenaire violentmal intentionné, ou l'envoi par le crime organisé de liens d'hameçonnage conduisant au téléchargement d'une application malveillante.
>
> Parmi les indicateurs de surveillance, on peut citer les applications suspectes que vous avez trouvées sur votre appareil et qui disposent d'autorisations pour le microphone, la caméra, l'accès au réseau, etc. L'activation de l'indicateur de la webcam sans  que l'application qui devrait la faire fonctionner soit en service, ou la "fuite" de fichiers de l'appareil.
>
> Pour de tels cas, vous pouvez vous référer au questionnaire de la trousse de premiers soins numériques "Mon appareil agit de manière suspecte".

Que voulez-vous faire ?

 - [Me conduire au questionnaire sur l'appareil agissant de manière suspecte](../../../device-acting-suspiciously)
 - [Passez à l'étape suivante ici](#pre-closure)

### suspicious-redirections-intro

> Même si c'est rare, nous voyons parfois des alertes de sécurité lorsque nous essayons de mettre à jour des applications ou même le système d'exploitation de nos appareils. Les raisons en sont multiples et peuvent être légitimes ou malveillantes. Voici quelques questions que nous devrions nous poser :
>
> - La date et l'heure de mon appareil sont-elles exactes ? La manière dont nos appareils font confiance aux serveurs pour fonctionner de manière plus sûre dépend de la vérification des certificats de sécurité qui sont valides dans un délai spécifique. Si, par exemple, votre ordinateur est configuré avec une année antérieure, ces contrôles de sécurité échoueront pour un certain nombre de sites web et de services. Si la date et l'heure sont exactes, cela peut également indiquer que le fournisseur n'a pas mis à jour ses certificats ou qu'il y a quelque chose de suspect. Dans tous les cas, il est conseillé de ne jamais mettre à jour ou utiliser les applications de votre appareil si vous voyez ces avertissements de sécurité ou ces redirections.
> - Le problème est-il apparu après un événement inhabituel, comme un clic sur une publicité, l'installation d'une application ou l'ouverture d'un document ? De nombreuses attaques reposent sur l'usurpation d'identité lors des installations et des mises à jour de logiciels légitimes ; il se peut donc que votre appareil s'en aperçoive et émette un avertissement ou arrête complètement le processus.
> Le processus qui vous pose problème se déroule-t-il d'une manière inhabituelle, par exemple si le processus de mise à jour de cette application suit normalement un processus différent de celui que vous observez habituellement ?
>
> Dans tous les cas, n'oubliez pas de tout documenter de la manière la plus détaillée possible, en particulier si vous souhaitez recevoir une aide spécialisée, mener une enquête plus approfondie ou entreprendre une action en justice.
>
> Dans la plupart des cas, il suffit de s'attaquer à la cause première du problème pour que l'appareil et la navigation reviennent à la normale. Toutefois, d'autres problèmes peuvent survenir si vous avez installé ou mis à jour une application (ou le système d'exploitation) dans des circonstances suspectes.

Avez-vous cliqué sur un logiciel ou une mise à jour et l'avez-vous téléchargé ou installé dans des conditions suspectes ?

 - [Oui](#pre-closure)
 - [Non](#suspicious-software-installed)

### suspicious-software-installed

> Dans la plupart des cas, les attaques qui détournent les processus d'installation ou de mise à jour des applications ou des systèmes d'exploitation visent à installer des logiciels malveillants dans le système. Si vous pensez que c'est le cas, nous vous recommandons de consulter le questionnaire de la trousse de premiers soins numériques "Mon appareil agit de manière suspecte".

Que voulez-vous faire ?

 - [Me conduire au questionnaire sur l'appareil agissant de manière suspecte](../../../device-acting-suspiciously)
 - [Passez à l'étape suivante ici](#pre-closure)

### security-alerts-intro

> Si vous recevez des messages ou des alertes de notification provenant de services de messagerie, de plateformes de réseaux sociaux ou de tout autre service en ligne que vous utilisez, et qu'ils sont liés à des problèmes de sécurité tels que de nouvelles connexions suspectes à partir de nouveaux appareils ou emplacements, vous pouvez d'abord vérifier si le message est légitime ou s'il s'agit d'un message d'hameçonnage. Pour ce faire, nous vous recommandons de consulter le questionnaire de la trousse de premiers soins numériques [J'ai reçu un message suspect](../../../suspicious-messages) avant d'aller de l'avant.
>
> Si le message est légitime, la question suivante que vous devez vous poser est de savoir s'il y a une raison pour laquelle ce message pourrait être légitime. Par exemple, si vous utilisez un service VPN ou Tor, et que vous naviguez sur la version web d'un service, il pensera que vous êtes situé dans le pays de votre serveur VPN ou de votre nœud de sortie Tor, déclenchant une alerte de nouvelle connexion suspecte, alors qu'il s'agit simplement d'une solution de prévention des intrusions.
>
> Il est également utile de se demander quel type d'alerte de sécurité nous recevons : s'agit-il d'une personne qui tente de se connecter à votre compte ou d'une personne qui y parvient ? En fonction de la réponse, la réaction requise peut être très différente.
>
> Si nous avons reçu une notification concernant des tentatives de connexion infructueuses et que nous disposons d'un bon mot de passe (long, non partagé avec d'autres services en ligne moins sécurisés, etc.) et que nous utilisons l'authentification multifactorielle (également connue sous le nom de MFA ou 2FA), nous ne devrions pas nous inquiéter outre mesure de la compromission de notre compte. Quelle que soit la gravité de la menace de compromission d'un compte, il est toujours bon de vérifier les journaux d'activité récente, où l'on peut généralement consulter l'historique des connexions, y compris l'emplacement et le type d'appareil.
>
> Si vous voyez quelque chose de suspect dans vos journaux d'activité, les stratégies les plus courantes, en fonction du compte, sont les suivantes :
>
> - Changer de mot de passe
> - Activer le MFA
> - Essayer de déconnecter tous les appareils du compte
> - Documenter les indicateurs de la tentative de piratage (au cas où nous voudrions chercher de l'aide, mener une enquête plus approfondie ou entamer une action en justice)
>
> Une autre question importante pour évaluer ces notifications est de savoir si nous partageons des appareils ou l'accès au compte avec quelqu'un d'autre qui pourrait avoir déclenché l'alerte. Si c'est le cas, faites attention au niveau d'accès que vous donnez à d'autres personnes à vos informations et répétez les étapes ci-dessus si vous souhaitez révoquer l'accès de cette tierce personne.

Si vous pensez qu'il existe d'autres menaces potentielles répertoriées au début du questionnaire et que vous souhaitez les vérifier, ou si votre cas est plus complexe et que vous souhaitez demander de l'aide, cliquez sur Suivant.

 - [Suivant](#pre-closure)


### suspicious-device-intro

> Si vous avez trouvé un appareil suspect, la première chose à se demander est si cet appareil est malveillant ou s'il est censé se trouver là où il est. En effet, ces dernières années, le nombre d'appareils dotés de capacités de communication dans nos foyers et sur nos lieux de travail a considérablement augmenté : imprimantes, lampes intelligentes, machines à laver, thermostats, etc. Étant donné le grand nombre de ces appareils, nous pourrions finir par nous inquiéter d'un appareil que nous ne nous souvenons pas avoir configuré ou qui a été configuré par quelqu'un d'autre mais qui n'est pas malveillant.
>
> Si vous ne savez pas si l'appareil que vous avez trouvé est malveillant, l'une des premières choses à faire est d'obtenir des informations à son sujet : marque, modèle, s'il est connecté au réseau câblé ou sans fil, s'il indique ce qu'il fait, s'il est sous tension, etc. Si vous ne savez toujours pas ce que fait l'appareil, vous pouvez utiliser ces informations pour effectuer une recherche sur le web et en apprendre davantage sur ce qu'est et ce que fait l'appareil.

Après avoir examiné l'appareil, pensez-vous qu'il est légitime ?

 - [C'était un dispositif légitime](#pré-fermeture)
 - [J'ai vérifié mais je ne sais toujours pas ce que fait le dispositif ou s'il est malveillant](#dispositifsuspicieux2)


### suspicious-device2

> Si vous êtes toujours préoccupé par l'appareil après un premier examen, vous devriez envisager la possibilité qu'il s'agisse d'un dispositif de surveillance. En général, ces dispositifs surveillent l'activité du réseau auquel ils sont connectés, ou peuvent même enregistrer et transmettre des données audio et/ou vidéo, comme le font les logiciels espions. Il est important de comprendre que ce scénario est très rare et qu'il est généralement lié à des profils à très haut risque. Toutefois, si vous vous sentez concerné·e, voici ce que vous pouvez faire :
>
> - Déconnectez l'appareil du réseau/de l'alimentation et vérifier que tous les services habituels fonctionnent sans problème. Ensuite, vous pouvez faire analyser l'appareil par quelqu'un d'expérimenté.
> - Faire des recherches supplémentaires et demander de l'aide (cela implique généralement des tests plus avancés comme la cartographie du réseau ou la recherche de signaux inhabituels).
> - En tant que mesure préventive pour atténuer toute surveillance du réseau si vous laissez l'appareil connecté, utilisez un VPN, Tor ou un outil similaire qui chiffre votre navigation sur le web, ainsi que des outils de communication avec un chiffrement de bout en bout.
> - Comme mesure préventive pour atténuer les risques d'enregistrement audio/vidéo si vous laissez l'appareil connecté, isolez physiquement l'appareil afin qu'il ne capture pas de données audio ou vidéo sensibles.

Si vous pensez qu'il existe d'autres menaces potentielles répertoriées au début et que vous souhaitez les vérifier, ou si vous souhaitez obtenir de l'aide, cliquez sur Suivant.

 - [Suivant](#pre-closure)
 - [Je pense avoir résolu mes problèmes](#resolved_end)

### leaked-internet-info-intro

> Cette partie du questionnaire couvre les fuites de données provenant de services et d'activités sur Internet. Pour les informations provenant de communications téléphoniques, veuillez vous référer à la section ["Des informations confidentielles partagées par le biais d'appels téléphoniques ou de SMS semblent avoir été divulguées à l'adversaire"](#leaked-phone-info-intro).
>
> En général, les fuites provenant de services sur internet peuvent se produire pour trois raisons :
>
> 1. l'information était publique au départ, ce qui ne sera pas abordé dans cette ressource.
> 2. Les informations ont été divulguées directement à partir du compte (plus souvent par des adversaires plus "petits" et plus proches de la victime).
> 3. les informations ont été divulguées à partir de la plateforme sur laquelle elles ont été téléchargées en premier lieu (ce qui est plus courant pour les "grands" adversaires tels que les gouvernements et les grandes entreprises).
>
> Nous couvrirons les deux derniers cas, car ils sont davantage liés à des situations d'urgence courantes dans la société civile. Vous pouvez suivre le processus à partir de la question ci-dessous :

Partagez-vous des appareils ou des comptes avec votre adversaire ?

 - [Oui](#shared-devices-or-accounts)
 - [Non](#leaked-internet-info2)

### shared-devices-or-accounts

> Dans certains cas, le partage de l'accès à un compte avec d'autres personnes peut permettre à des tiers non désirés d'accéder à des informations sensibles, qui peuvent ensuite être facilement divulguées. En outre, lorsque nous partageons des comptes, nous devons généralement rendre le processus pratique, ce qui affecte souvent la sécurité des mécanismes d'accès au compte, comme le fait de rendre les mots de passe plus faibles pour qu'ils puissent être partagés facilement et de désactiver l'authentification multifactorielle (MFA ou 2FA) pour permettre à de nombreuses personnes de se connecter au même compte.
>
> Un premier conseil serait d'évaluer qui devrait avoir accès aux comptes ou aux ressources concernés (comme les documents stockés dans le nuage) et de limiter cet accès au plus petit nombre possible de personnes. Un autre principe utile est d'avoir un compte pour chaque personne, ce qui permet à chacun de configurer ses paramètres d'accès de la manière la plus sûre possible.
>
> Il est également recommandé de consulter la section ["Je reçois des messages de sécurité et des alertes de services de confiance"](#security-alerts-intro), où vous trouverez des conseils pour renforcer la sécurité des comptes en cas d'accès suspect.

Cliquez sur Suivant pour étudier votre situation plus en détail.

 - [Suivant](#leaked-internet-info2)

### leaked-internet-info2

Votre adversaire peut-il contrôler ou accéder aux services en ligne que vous utilisez et stocker les informations divulguées (*Cela s'applique généralement aux gouvernements, aux organismes chargés de l'application de la loi ou aux fournisseurs eux-mêmes*) ?

 - [Oui](#control-of-online-services)
 - [Non](#leaked-internet-info3)

### control-of-online-services

> Parfois, les plateformes que nous utilisons sont facilement accessibles, voire contrôlées par des adversaires. Voici quelques questions courantes que vous pouvez vous poser :
>
> - À qui appartient le service ?
> - S'agit-il d'un adversaire ?
> - L'adversaire a-t-il accès aux serveurs ou aux données par le biais d'enquêtes légales ?
>
> Dans ce cas, il est conseillé de déplacer les informations vers une plateforme différente, non contrôlée par l'adversaire, afin d'éviter de futures fuites.
>
> D'autres indicateurs utiles pourraient être :
>
> - l'activité de connexion (y compris les emplacements géographiques), si elle est disponible
> - des comportements étranges à propos des éléments lus et non lus
> - des configurations affectant le traitement des messages ou des informations, telles que des redirections, des règles de renvoi des courriels, etc.
>
> Vous pouvez également vérifier si vos messages sont lus par quelqu'un d'autre en utilisant les jetons Canary, comme ceux que nous pouvons générer dans [canarytokens.org](https://canarytokens.org/generate).

Cliquez sur Suivant pour étudier votre situation plus en détail.

 - [Suivant](#leaked-internet-info3)

### leaked-internet-info3

Votre adversaire a-t-il accès à des équipements de surveillance et a-t-il la capacité de les déployer à proximité de vous ?

 - [Oui](#leaked-internet-info4)
 - [Non](#pre-closure)

### leaked-internet-info4

> Des adversaires plus puissants peuvent avoir la capacité de déployer des équipements de surveillance spécialisés autour de leurs victimes, par exemple en installant des dispositifs de géolocalisation ou en infectant vos propres appareils avec des logiciels espions.

Pensez-vous que vous puissiez être suivi par un dispositif de géolocalisation ou que votre ordinateur ou votre téléphone portable ait été infecté par un logiciel malveillant destiné à vous espionner ?

 - [J'aimerais en savoir plus sur les dispositifs de traçage](#tracking-device-intro)
 - [J'aimerais en savoir plus sur la possibilité que mon appareil soit compromis par un logiciel malveillant](#device-behaviour-intro)

### leaked-phone-info-intro

> Comme décrit dans l'introduction de ce questionnaire, la surveillance massive des téléphones est une menace omniprésente dans la plupart des régions et pays (si ce n'est tous). Pour commencer, nous devons être conscients qu'il est très facile pour les opérateurs (et toute personne qui les contrôle ou y a accès) de vérifier des informations telles que :
>
> - l'historique de la localisation (chaque fois que le téléphone est allumé et connecté au réseau), les données des tours de téléphonie mobile auxquelles le téléphone est connecté
> - les journaux d'appels (qui appelle qui, quand et pendant combien de temps)
> - les SMS (à la fois les journaux et le contenu réel des messages)
> - le trafic Internet (moins courant et moins utile pour les adversaires, mais il serait possible d'en déduire quelles applications sont utilisées, à quel moment, quels sites web sont consultés, etc.)
>
> Un autre scénario encore observé dans certains contextes, bien que peu courant, est l'utilisation de dispositifs d'interception physique appelés stingrays ou IMSI-Catchers. Ces dispositifs usurpent l'identité d'une tour de réseau cellulaire légitime pour rediriger par leur intermédiaire le trafic téléphonique des personnes se trouvant dans une zone d'accès physique proche. Bien que les vulnérabilités de ces dispositifs soient résolues ou atténuées par des protocoles plus récents (4G/5G), il est toujours possible de brouiller le signal pour faire croire aux téléphones que le seul protocole disponible est le 2G (le protocole le plus utilisé par ces dispositifs). Dans ce cas, il existe des indicateurs assez fiables, notamment le fait d'être "rétrogradé" en 2G dans des zones où il devrait y avoir de la 4G ou de la 5G, ou lorsque d'autres personnes autour sont connectées aux protocoles plus récents sans problème. Pour plus d'informations, vous pouvez consulter le [site web du projet FADe](https://fadeproject.org/?page_id=36). Vous pouvez également vous demander si votre adversaire a un accès possible à ce type d'appareil, entre autres choses.
>
> En tant que recommandation générale pour ces scénarios, il est conseillé aux personnes à risque de migrer leurs communications vers des canaux cryptés reposant sur l'internet plutôt que sur des appels téléphoniques et des SMS conventionnels. Il en va de même pour l'utilisation de VPN ou de Tor pour le trafic internet sensible (y compris les applications). Le principal aspect non résolu serait le suivi de la localisation de l'utilisateur, qui est inévitable lorsque l'on reste connecté au réseau téléphonique.
>
> En ce qui concerne la collecte massive de données par les opérateurs, dans certaines juridictions, les utilisateurs peuvent demander quelles données les concernant ont été partagées dans le passé. Vous pouvez rechercher si cela s'applique à vous et si ces informations seront fiables pour détecter une surveillance potentielle. N'oubliez pas qu'il s'agit généralement d'une enquête juridique.

Si vous souhaitez considérer d'autres menaces potentielles ou si votre cas est plus complexe et que vous souhaitez obtenir de l'aide, cliquez sur Suivant.

 - [Suivant](#pre-closure)


### peer-compromised-intro

> Si une personne ou une organisation qui vous est proche a été victime de surveillance, nous vous recommandons d'essayer de comprendre les techniques qui ont été utilisées à son égard et de sélectionner dans la liste précédente celles qui s'en rapprochent le plus, afin de bénéficier d'un contexte plus pertinent et de décider si vous pourriez vous aussi être victime de techniques similaires.

Que souhaitez-vous faire ?

 - [Me ramener à l'étape précédente](#start)
 - [Je ne suis pas sûr(e)](#pre-closure)

### other-intro

> La liste que vous avez vue précédemment comprenait les cas les plus courants rencontrés dans la société civile par les personnes qui aident dans ce type de contexte, mais il se peut que votre situation ne soit pas couverte par cette liste. Si c'est le cas, cliquez sur "J'ai besoin d'aide" ; sinon, vous pouvez revenir à la liste des cas les plus courants et choisir le scénario le plus proche de votre cas pour obtenir des conseils plus spécifiques.

Que voulez-vous faire ?

 - [Me conduire à la liste des menaces potentielles](#start)
 - [J'ai besoin d'aide](#help_end)

### pre-closure

> Nous espérons que ce questionnaire et ses recommandations vous ont été utiles jusqu'à présent. Vous pouvez choisir de revenir à la liste initiale de scénarios pour vérifier d'autres menaces qui pourraient vous préoccuper, ou aller à la liste des organisations que vous pourriez contacter pour vous aider dans des cas plus complexes.

Que souhaitez-vous faire ?

 - [J'aimerais en savoir plus sur d'autres techniques de surveillance](#start)
 - [Je pense que mon besoin n'est pas couvert par cette section ou qu'il est trop complexe](#help_end)
 - [Je pense avoir une idée claire de ce qui m'arrive](#resolved_end)


### help_end

> Si vous avez besoin d'une aide supplémentaire pour traiter les cas de surveillance, vous pouvez contacter les organisations énumérées ci-dessous.
>
> *Note : lorsque vous contactez une organisation pour demander de l'aide, veuillez partager le parcours que vous avez suivi ici et tout indice que vous avez trouvé pour faciliter l'étape suivante.*

:[](organisations?services=legal&services=vulnerabilities_malware&services=forensic)

### legal_end

> Si vous avez besoin d'aide pour poursuivre quelqu'un qui vous espionne illégalement, vous pouvez vous adresser à l'une des organisations ci-dessous.

:[](organisations?services=legal)

### resolved_end

Nous espérons que cette trousse de premiers soins numériques vous a été utile. N'hésitez pas à nous faire part de vos commentaires [via email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- Transférez toutes les communications des canaux non sécurisés tels que les appels téléphoniques et les SMS vers des canaux de communication chiffrés reposant sur internet, tels que les applications de messagerie chiffrées de bout en bout et les sites web protégés par le protocole HTTPS.
- Si vous craignez que votre trafic internet soit surveillé et que vous pensez que cela peut constituer un risque pour votre sécurité, envisagez de vous connecter à  internet par l'intermédiaire d'un outil de chiffrement tel qu'un VPN ou Tor.
- Gardez une trace de tous les appareils présents dans votre espace, en particulier ceux qui sont connectés à internet, et essayez de savoir ce qu'ils font.
- Évitez de partager des informations sensibles par l'intermédiaire de services contrôlés par un adversaire.
- Réduisez autant que possible le partage de vos comptes ou de vos appareils.
- Soyez attentif aux messages, sites web, téléchargements et applications suspects.

#### resources

- [Je sécurise ma localisation -  par le Centre Hubertine Auclert
à partir des travaux du guide Chayn](https://www.guide-protection-numerique.com/je-securise-ma-localisation)
- [Les Guides sur la sécurité d’appareils informatiques de l'association Echap avec notamment des conseils pour identifier des logiciels espion sur mobile](https://echap.eu.org/ressources/)
- [Apple’s Android App to Scan for AirTags is a Necessary Step Forward, But More Anti-Stalking Mitigations Are Needed](https://www.eff.org/deeplinks/2021/12/apples-android-app-scan-airtags-necessary-step-forward-more-anti-stalking).
- [Privacy for Students](https://ssd.eff.org/fr/module/privacy-students): With references of surveillance techniques in schools that can apply to many other scenarios.
- [Security in a Box: Protect the privacy of your online communication](https://securityinabox.org/en/communication/private-communication/)
- [Security in a Box: Visit blocked websites and browse anonymously](https://securityinabox.org/en/internet-connection/anonymity-and-circumvention/)
- [Security in a Box: Recommendations for encrypted chat tools](https://securityinabox.org/en/communication/tools/#more-secure-text-voice-and-video-chat-applications)
