---
layout: page
title: "Êtes-vous victime de harcèlement en ligne ?"
author: Flo Pagano, Natalia Krapiva
language: fr
summary: "Êtes-vous victime de harcèlement en ligne ?"
date: 2023-04
permalink: /fr/topics/harassed-online/
parent: /fr/
---

# Are you being targeted by online harassment?

Internet, et les plateformes de médias sociaux en particulier, sont devenus un espace vital pour les membres et les organisations de la société civile, en particulier pour les femmes, les personnes LGBTQIA+ et autres groupes marginalisés, afin qu'ils puissent s'exprimer et faire entendre leur voix. Mais en même temps, ils sont aussi devenus des espaces où ces groupes sont facilement ciblés pour le fait d'exprimer leurs points de vue. La violence et les abus en ligne privent les femmes, les personnes LGBTQIA+ et de nombreuses autres personnes défavorisées du droit de s'exprimer de manière égale, libre et sans crainte.

La violence et les abus en ligne revêtent de nombreuses formes, et des personnes ou organisations malveillantes peuvent souvent compter sur l'impunité, également en raison de l'absence de lois protégeant les victimes de harcèlement dans de nombreux pays, et surtout parce qu'il n'existe pas de stratégies de protection universelles : elles doivent être adaptées de manière créative en fonction du type d'attaque. Il est donc important d'identifier la typologie de l'attaque qui vous vise pour décider des mesures à prendre.

Cette section de la trousse de premiers soins numériques vous guidera à travers quelques étapes de base pour planifier la façon de vous protéger contre l'attaque dont vous souffrez et planifier la façon de vous en protéger, ou trouvez un service d'assistance en matière de sécurité numérique qui puisse vous aider.

Si vous êtes victime de harcèlement en ligne, nous vous suggérons de lire les recommandations sur [comment prendre soin de vous](/../../self-care) et [comment documenter les attaques](/../../documentation), puis de suivre ce questionnaire pour identifier la nature de votre problème et trouver des solutions possibles.

## Workflow

### physical-wellbeing

Craignez-vous pour votre intégrité physique ou votre bien-être ?

- [Oui](#physical-risk_end)
- [Non](#no-physical-risk)

### location

L'attaquant semble-t-il connaître votre emplacement physique ?

- [Oui](#location_tips)
- [Non](#device)

### location_tips

> Vérifiez vos publications récentes sur les médias sociaux : votre position exacte y figure-t-elle ? Si c'est le cas, désactivez l'accès au GPS pour les applications de médias sociaux et autres services sur votre téléphone afin que votre position ne soit pas affichée lorsque vous publiez de nouveaux contenus.
>
> Vérifiez les photos de vous que vous avez publiées en ligne : comportent-elles des détails sur des lieux clairement reconnaissables et permettant de savoir où vous vous trouvez ? Pour vous protéger d'éventuels harceleurs, il est préférable de ne pas montrer des détails permettant de déduire votre position exacte lorsque vous publiez des photos ou des vidéos de vous.
>
> Par mesure de précaution supplémentaire, il est également conseillé de ne pas utiliser le GPS en permanence, sauf lorsque vous avez besoin de retrouver votre position sur une carte.

L'agresseur aurait-il pu vous suivre grâce aux informations que vous avez publiées en ligne ? Ou avez-vous toujours l'impression que l'agresseur connaît votre position physique par d'autres moyens ?

 - [Non, je pense avoir résolu mes problèmes](#resolved_end)
 - [Oui, je pense toujours que l'agresseur sait où je suis](#device)
 - [Non, mais j'ai d'autres problèmes](#account)

### device

Pensez-vous que l'attaquant a accédé ou accède à votre appareil ?

 - [Oui](#device-compromised)
 - [Non](#account)

### device-compromised

> Modifiez le mot de passe pour accéder à votre appareil pour un mot de passe unique, long et complexe :
>
> - [Mac OS](https://support.apple.com/fr-fr/HT202860)
> - [Windows](https://support.microsoft.com/fr-fr/help/4490115/windows-change-or-reset-your-password)
> - [iOS - Apple ID](https://support.apple.com/fr-fr/HT201355)
> - [Android](https://support.google.com/accounts/answer/41078?co=GENIE.Platform%3DAndroid&hl=en=GENIE.Platform%3DAndroid&hl=fr)

Avez-vous toujours l'impression que l'attaquant peut contrôler votre appareil ?

 - [Non, je pense avoir résolu mes problèmes](#resolved_end)
 - [Non, mais j'ai d'autres problèmes](#account)
 - [Non](#info_stalkerware)


### info_stalkerware

> [Stalkerware](https://fr.wikipedia.org/wiki/Stalkerware) est un logiciel utilisé pour surveiller l'activité ou la localisation d'une personne dans le but de la harceler ou de la contrôler.
>
> Si vous pensez que quelqu'un vous espionne par le biais d'une application qu'il a installée sur votre appareil mobile, [ce flux de travail de la trousse de premiers soins numériques vous aidera à déterminer si votre appareil est infecté par un logiciel malveillant et à prendre les mesures nécessaires pour le nettoyer](../../../device-acting-suspiciously).

Avez-vous besoin d'aide pour comprendre l'attaque dont vous êtes victime ?

 - [Non, je pense avoir résolu mes problèmes](#resolved_end)
 - [Oui, j'ai encore des problèmes que j'aimerais résoudre](#account)

### account

> Que quelqu'un ait eu accès à votre appareil ou non, il est possible qu'il ait accédé à vos comptes en ligne en les piratant ou en connaissant ou en déchiffrant votre mot de passe.
>
> Si quelqu'un a accès à un ou plusieurs de vos comptes en ligne, il peut lire vos messages privés, identifier vos contacts, publier vos messages, photos ou vidéos privés, ou commencer à se faire passer pour vous.
>
> Examinez l'activité de vos comptes en ligne et votre boîte aux lettres (y compris les dossiers Envoyés et Corbeille) pour détecter d'éventuelles activités suspectes.

Avez-vous remarqué que des messages ont disparu ou que d'autres activités vous donnent de bonnes raisons de penser que votre compte a été compromis ?

 - [Oui](#account-compromised)
 - [Non](#private-contact)


### changer les mots de passe

> Essayez de changer le mot de passe de chacun de vos comptes en ligne avec un mot de passe fort et unique.
>
> Vous pouvez en apprendre davantage sur la création et la gestion des mots de passe dans ce [Guide d'autodéfense en matière de surveillance](https://ssd.eff.org/fr/module/cr%C3%A9er-des-mots-de-passe-robustes).
>
> C'est également une très bonne idée d'ajouter une deuxième couche de protection à vos comptes en ligne en activant l'authentification à deux facteurs (2FA) chaque fois que cela est possible.
>
> Découvrez comment activer l'authentification à 2 facteurs dans ce [Guide d'autodéfense en matière de surveillance](https://ssd.eff.org/fr/module/guide-pratique-activer-l%E2%80%99authentification-%C3%A0-deux-facteurs).

Avez-vous toujours l'impression que quelqu'un pourrait avoir accès à votre compte ?

 - [Oui](#hacked-account)
 - [Non](#private-contact)


### hacked-account

> Si vous ne parvenez pas à accéder à votre compte, il est possible qu'il ait été piraté et que le pirate ait changé votre mot de passe.

Si vous pensez que votre compte a été piraté, essayez de suivre le questionnaire de la trousse de premiers soins numériques qui peut vous aider à résoudre les problèmes d'accès à votre compte.

 - [Veuillez m'amener au questionnaire pour résoudre les problèmes d'accès au compte](../../../account-access-issues)
 - [Je pense avoir résolu mes problèmes](#resolved_end)
 - [Mon compte va bien, mais j'ai d'autres problèmes](#private-contact)


### private-contact

Recevez-vous des appels téléphoniques ou des messages indésirables sur une application de messagerie ?

 - [Oui](#change_contact)
 - [Non](#threats)

### change_contact

> Si vous recevez des appels téléphoniques, des SMS ou des messages indésirables sur des applications associées à votre numéro de téléphone, à votre adresse électronique ou à d'autres coordonnées privées, vous pouvez essayer de modifier votre numéro, votre carte SIM, votre adresse électronique ou d'autres coordonnées associées au compte.
>
> Vous pouvez également envisager de signaler et de bloquer les messages et le compte associé à la plateforme concernée.

Avez-vous réussi à mettre fin aux appels ou aux messages indésirables ?

 - [Oui](#legal)
 - [Non, j'ai d'autres problèmes](#threats)
 - [Non, j'ai besoin d'aide](#harassment_end)

### menaces

Êtes-vous victime de chantage ou de menaces par le biais d'e-mails ou de messages sur un compte de média social ?

 - [Oui](#threats_tips)
 - [Non](#impersonation)

### conseils_menaces

> Si vous recevez des messages contenant des menaces, y compris des menaces de violence physique ou sexuelle, ou du chantage, vous devez documenter autant que possible ce qui s'est passé, notamment en enregistrant les liens et les captures d'écran, en signalant la personne à la plateforme ou au fournisseur de services concerné, et en bloquant l'attaquant.

Avez-vous réussi à mettre fin aux menaces ?

 - [Oui](#legal)
 - [Oui, mais j'ai d'autres problèmes](#impersonation)
 - [Non, j'ai besoin d'une assistance juridique](#legal-warning)

### impersonation

> Une autre forme de harcèlement dont vous pourriez avoir été victime est l'usurpation d'identité.
>
> Par exemple, quelqu'un peut avoir créé un compte sous votre nom et envoyer des messages privés ou attaquer publiquement quelqu'un sur les plateformes de médias sociaux, diffuser de la désinformation ou des discours haineux, ou agir d'une autre manière pour vous rendre, votre organisation ou d'autres personnes proches de vous, vulnérables à des risques de réputation et de sécurité.
>
> Si vous pensez que quelqu'un se fait passer pour vous ou pour votre organisation, vous pouvez suivre cette trousse de premiers soins numériques, qui vous guidera à travers les différentes formes d'usurpation d'identité afin d'identifier la meilleure stratégie possible pour faire face à votre problème.

Que voulez-vous faire ?

 - [Je suis victime d'une usurpation d'identité et j'aimerais trouver une solution](../../../impersonated)
 - [Je suis confronté·e à une autre forme de harcèlement](#defamation)

### defamation

Quelqu'un essaie-t-il de nuire à votre réputation en diffusant de fausses informations ?

 - [Oui](#defamation-yes)
 - [Je suis confronté(e) à une autre forme de harcèlement](#doxing)

### defamation-yes

> La diffamation est généralement définie comme une déclaration qui porte atteinte à la réputation de quelqu'un. La diffamation peut être verbale (calomnie) ou écrite (libellé). Les lois des différents pays peuvent différer quant à la définition de la diffamation en vue d'une action en justice. Par exemple, certains pays ont des lois très protectrices de la liberté d'expression qui permettent aux médias et aux particuliers de dire des choses embarrassantes ou préjudiciables sur des personnes publiques, pour autant qu'ils croient que ces informations sont vraies. D'autres pays peuvent vous permettre de poursuivre plus facilement d'autres personnes pour avoir diffusé des informations à votre sujet qui ne vous plaisent pas.
>
> Si quelqu'un tente de nuire à votre réputation ou à celle de votre organisation, vous pouvez suivre le questionnaire de la trousse de premiers soins numériques sur la diffamation, qui vous guide à travers diverses stratégies pour faire face aux campagnes de diffamation.

Que souhaitez-vous faire ?

 - [Je voudrais trouver une stratégie contre une campagne de diffamation](../../../defamation)
 - [Je suis confronté à une autre forme de harcèlement](#doxing)

### doxing

Quelqu'un a-t-il publié des informations personnelles ou des photos sans votre consentement ?

- [Oui](#doxing-yes)
- [Non](#hate-speech)

### doxing-yes

> Si quelqu'un a publié des informations privées vous concernant ou fait circuler des vidéos, des photos ou d'autres médias à votre sujet, vous pouvez suivre le questionnaire de la trousse de premiers soins numériques qui peut vous aider à comprendre ce qui se passe et à réagir à une telle attaque.

Que voulez-vous faire ?

 - [J'aimerais comprendre ce qui se passe et ce que je peux faire](../../../doxing)
 - [J'aimerais recevoir de l'aide](#harassment_end)

### hate-speech

L'attaque est-elle fondée sur la diffusion de messages haineux liés à des attributs comme la race, le sexe ou la religion ?

- [Oui](#one-more-persons)
- [Non](#harassment_end)


### one-more-persons

Avez-vous été attaqué par une ou plusieurs personnes ?

- [J'ai été attaqué par une seule personne](#one-person)
- [J'ai été attaqué par plusieurs personnes](#campaign)

### one-person

> Si le discours haineux provient d'une seule personne, le moyen le plus simple et le plus rapide de contenir l'attaque et d'empêcher l'utilisateur de continuer à vous envoyer des messages haineux est de le signaler et de le bloquer. N'oubliez pas que si vous bloquez l'utilisateur, vous ne pouvez pas accéder à son contenu pour le documenter. Avant de bloquer, lisez nos [conseils sur la documentation des attaques numériques](/../../documentation).
>
> Que vous sachiez ou non qui est votre harceleur, c'est toujours une bonne idée de le bloquer sur les plateformes de réseaux sociaux lorsque c'est possible.
>
> - [Facebook](https://www.facebook.com/help/168009843260943)
> - [Google](https://support.google.com/accounts/answer/6388749)
> - [Instagram](https://help.instagram.com/426700567389543)
> - [TikTok](https://support.tiktok.com/fr/using-tiktok/followers-and-following/blocking-the-users)
> - [Twitter](https://help.twitter.com/fr/using-twitter/blocking-and-unblocking-accounts)
> - [Whatsapp](https://faq.whatsapp.com/1142481766359885/?cms_platform=android)

Avez-vous bloqué efficacement votre harceleur ?

- [Oui](#legal)
- [Non](#campaign)


### legal

> Si vous savez qui vous harcèle, vous pouvez le signaler aux autorités de votre pays si vous cela jugez sûr et approprié dans votre contexte. Chaque pays dispose de lois différentes pour protéger les personnes contre le harcèlement en ligne. Nous vous conseillons d'étudier la législation de votre pays ou de demander des conseils juridiques pour vous aider à décider de ce qu'il convient de faire.
>
> Si vous ne savez pas qui vous harcèle, vous pouvez, dans certains cas, retrouver l'identité de l'agresseur grâce à une analyse précise des traces qu'il a pu laisser derrière lui.
>
> En tout état de cause, si vous envisagez d'engager une action en justice, il sera très important de conserver des preuves des attaques dont vous avez fait l'objet. Il est donc fortement recommandé de suivre les [recommandations de la page de la trousse de premiers soins numériques sur l'enregistrement des informations relatives aux attaques](/../../documentation).

Que souhaitez-vous faire ?

 - [Je voudrais recevoir une aide juridique pour poursuivre mon agresseur en justice](#legal_end)
 - [Je pense avoir résolu mes problèmes](#resolved_end)


### campaign

> Si vous êtes attaqué par plus d'une personne, vous êtes peut-être la cible d'un discours de haine ou d'une campagne de harcèlement, et vous devrez réfléchir à la meilleure stratégie à adopter dans votre cas.
>
> Pour connaître toutes les stratégies possibles, lisez [la page de Take Back The Tech sur les stratégies contre le discours de haine](https://www.takebackthetech.net/fr/be-safe/discours-haineux-strat%C3%A9gies).

Avez-vous identifié la meilleure stratégie pour vous ?

 - [Oui](#resolved_end)
 - [Non, j'ai besoin d'aide pour résoudre d'autres problèmes](#harassment_end)
 - [Non, j'ai besoin d'une assistance juridique](#legal_end)

### harassment_end

> Si vous êtes toujours victime de harcèlement et que vous avez besoin d'une solution personnalisée, veuillez communiquer avec les organismes ci-dessous qui peuvent vous aider.

:[](organisations?services=harassment)


### physical-risk_end

> Si vous courez un risque physique, veuillez entrer en contact avec l'un de ces organismes qui peuvent vous aider.

:[](organisations?services=physical_security)


### legal_end

> Si vous avez besoin d'un soutien juridique, veuillez entrer en contact avec les organismes ci-dessous qui peuvent vous aider.

:[](organisations?services=legal)


### resolved_end

Nous espérons que ce guide de dépannage a été utile. Donnez nous votre avis [par email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com).


### final_tips

- **Documenter le harcèlement:** Il est utile de documenter les attaques ou tout autre incident dont vous êtes témoin : faites des captures d'écran, enregistrez les messages que vous recevez des harceleurs, etc. Si possible, créez un journal dans lequel vous pourrez systématiser cette documentation en enregistrant les dates, les heures, les plateformes et les URL, l'identifiant de l'utilisateur, les captures d'écran, la description de ce qui s'est passé, etc. Les journaux peuvent vous aider à détecter des schémas récurrents et des indications possibles sur vos agresseurs éventuels. Si vous vous sentez dépassé, essayez de penser à une personne de confiance qui pourrait documenter les incidents pour vous pendant un certain temps. Vous devez faire profondément confiance à la personne qui gérera cette documentation, car vous devrez lui remettre les accès à vos comptes personnels. Avant de partager votre mot de passe avec cette personne, changez-le et partagez-le par un moyen sécurisé - comme l'utilisation d'un outil avec [chiffrement de bout en bout](https://www.frontlinedefenders.org/fr/resource-publication/guide-secure-group-chat-and-conferencing-tools). Lorsque vous pensez pouvoir reprendre le contrôle de votre compte, n'oubliez pas de changer votre mot de passe pour un mot de passe unique, [sécurisé](https://ssd.eff.org/fr/module/cr%C3%A9er-des-mots-de-passe-robustes), que vous seul connaissez.

    - Suivez les [recommandations de la page de la trousse de premiers soins numériques sur l'enregistrement des informations relatives aux attaques](/../../documentation) pour stocker les informations relatives à votre attaque de la meilleure façon possible.

- **Configurez l'authentification à 2 facteurs** sur tous vos comptes. L'authentification à 2 facteurs peut être très efficace pour empêcher quelqu'un d'accéder à vos comptes sans votre permission. Si vous pouvez choisir, n'utilisez pas l'authentification par SMS à 2 facteurs et choisissez une autre option, basée sur une application téléphonique ou sur une clé de sécurité.

    - Si vous ne savez pas quelle solution vous convient le mieux, vous pouvez consulter les documents suivants : [Access Now's "Quel est le meilleur type d’authentification multifactorielle pour moi" infographie](https://www.accessnow.org/cms/assets/uploads/2017/09/Choose-the-Best-MFA-for-you.png) et [EFF's "Guide des types les plus communs d’authentification à double facteur sur l’internet"](https://www.eff.org/deeplinks/2017/09/guide-common-types-two-factor-authentication-web).
    - Vous trouverez des instructions pour mettre en place une authentification à double facteurs sur les principales plateformes dans [The 12 Days of 2FA : Comment activer l'authentification à double facteurs de vos comptes en ligne](https://www.eff.org/deeplinks/2016/12/12-days-2fa-how-enable-two-factor-authentication-your-online-accounts).

- **Cartographiez votre présence en ligne**. L'autodoxage consiste à explorer les sources ouvertes de renseignements sur soi-même afin d'empêcher les acteurs malveillants de trouver et d'utiliser ces informations pour usurper votre identité. Pour en savoir plus sur la manière de rechercher vos traces en ligne, consultez le [Guide d'Access Now Helpline pour prévenir le doxing](https://guides.accessnow.org/self-doxing.html).
- **N'acceptez pas les messages provenant d'expéditeurs inconnus.** Certaines plateformes de messagerie, comme WhatsApp, Signal ou Facebook Messenger, vous permettent de prévisualiser les messages avant d'accepter l'expéditeur comme digne de confiance. Apple iMessage vous permet également de modifier les paramètres pour filtrer les messages provenant d'expéditeurs inconnus. N'acceptez jamais le message ou le contact s'ils vous semblent suspects ou si vous ne connaissez pas l'expéditeur.

#### Resources

- [La documentation publique d’assistance à la sécurité numérique d’Access Now : Guide pour la prévention du doxing](https://guides.accessnow.org/self-doxing.html)
- [La documentation publique d’assistance à la sécurité numérique d’Access Now : FAQ - Harcèlement en ligne ciblant un membre de la société civile](https://accessnowhelpline.gitlab.io/community-documentation/234-FAQ-Online_Harassment.html)
- [PEN America: Manuel de défense contre le cyberharcèlement](https://onlineharassmentfieldmanual.pen.org/fr/)
- [Equality Labs: Guide anti-doxing pour les activistes confrontés aux attaques de l’extrême droite](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
- [FemTechNet: Verrouiller votre identité numérique](https://femtechnet.org/csov/lock-down-your-digital-identity/)
- [National Network to End Domestic Violence : Conseils de documentation pour les survivants d'abus et de harcèlement technologiques](https://www.techsafety.org/documentationtips)
