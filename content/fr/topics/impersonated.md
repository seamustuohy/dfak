---
layout: page
title: "Quelqu'un se fait passer pour moi en ligne"
author: Floriana Pagano, Alexandra Hache
language: fr
summary: "Quelqu'un s'est fait usurper son identité par le biais d'un compte de média social, d'une adresse e-mail, d'une clé PGP, d'un faux site Web ou d'une fausse application"
date: 2023-04
permalink: /fr/topics/impersonated
parent: /fr/
---

# Someone is Impersonating Me Online

Une menace à laquelle sont confrontés de nombreuses personnes militantes, défenseuses des droits humains, ONG, médias indépendants et blogueuses, est que leur(s) adversaire(s) usurpe(nt) leur identité, pour créer de faux profils, sites web ou courriels en leur nom. Cette usurpation est quelque fois utilisée pour créer des campagnes de diffamation, divulguer des informations trompeuses, d'ingénierie sociale ou pour faire du bruit, créer des problèmes de confiance et des violations de données, qui ont tous un impact sur la réputation des individus et des collectifs dont on a volé l’identité numérique. Dans d'autres cas, un adversaire peut usurper l'identité d'une personne en ligne pour des raisons financières, comme la collecte de fonds, le vol de justificatifs de paiement, la réception de paiements, etc.

C'est un problème frustrant qui peut, à différents niveaux, affecter votre capacité à communiquer et à informer. Il peut également avoir des causes différentes selon le lieu et la manière dont vous êtes imité.

Il est important de savoir qu'il existe de nombreuses façons de se faire passer pour quelqu'un (faux profils dans les médias sociaux, sites Web clonés, courriels frauduleux, publication non consensuelle d'images et de vidéos personnelles). Les stratégies peuvent aller de la soumission d'avis de retrait, à la preuve de la propriété originale, en passant par la revendication des droits d'auteur du site Web ou de l'information originale, ou l'avertissement de vos réseaux et contacts personnels par des communications publiques ou confidentielles. Diagnostiquer le problème et trouver des solutions possibles à l'usurpation d'identité peut s'avérer compliqué. Parfois, il sera presque impossible de pousser une petite société d'hébergement à faire tomber un site Web, et une action en justice peut s'avérer nécessaire. Il est recommandé d'établir des alertes et de surveiller Internet pour savoir si vous ou votre organisation avez été usurpé.

Cette section de la trousse de premiers soins numériques vous guidera à travers quelques étapes de base pour diagnostiquer les moyens potentiels de se faire passer pour vous et les stratégies d'atténuation potentielles pour supprimer les comptes, sites Web et courriels qui se font passer pour vous ou votre organisation.

Si votre identité a été usurpée, suivez ce questionnaire pour identifier la nature de votre problème et trouver des solutions possibles.


## Workflow

### urgent-question

Craignez-vous pour votre intégrité physique ou votre bien-être ?

 - [Oui](#physical-sec_end)
 - [Non](#diagnostic-start1)

### diagnostic-start1

L'usurpation d'identité vous affecte-t-elle en tant qu'individu (quelqu'un utilise votre nom et prénom légal, ou le surnom sur lequel vous basez votre réputation) ou en tant qu'organisation/collectif ?

- [En tant que personne](#individual)
- [En tant qu'organisation](#organization)

### individual

> Si vous êtes affecté en tant qu'individu, vous voudrez peut-être alerter vos contacts. Effectuez cette étape à l'aide d'un compte de messagerie, d'un profil ou d'un site Web qui est entièrement sous votre contrôle.

- Une fois que vous avez informé vos contacts que votre identité a été usurpée, passez à l'[étape suivante](#diagnostic-start2)


### organization

> Si vous êtes affecté en tant que groupe, considérez faire une communication publique. Pour ce faire, utilisez un compte de messagerie, un profil ou un site Web qui est entièrement sous votre contrôle.

- Une fois que vous avez informé votre communauté que votre identité a été usurpée, passez à l'étape suivante (#diagnostic-start2).


### diagnostic-start2

Comment est-ce que quelqu'un se fait passer pour vous ?

 - [Un faux site Web se fait passer pour moi ou pour mon groupe](#fake-website)
 - [Par le biais d'un compte de réseau social](#social-network)
 - [Par le partage non consensuel de vidéos ou d'images](#doxing)
 - [Par mon adresse e-mail ou une adresse similaire](#spoofed-email1)
 - [Par une clé PGP connectée à mon adresse email](#PGP)
 - [Par une fausse application qui imite mon application](#app1)


### social-network

> Vous pouvez décider de signaler le compte ou le contenu usurpé à la plateforme concernée par l'usurpation d'identité.
>
> ***Note:*** *Toujours [documenter](/../../documentation) avant de prendre des mesures telles que la suppression de messages ou de journaux de conversation ou le blocage de profils. Si vous envisagez une action en justice, vous devriez consulter les informations sur la [documentation juridique](/../../documentation#legal).*

Sur quelle plateforme de réseautage social quelqu'un se fait-il passer pour vous ?

- [Facebook](#facebook)
- [Instagram](#instagram)
- [TikTok](#tiktok)
- [Twitch](#twitch)
- [Twitter](#twitter)
- [YouTube](#youtube)m)

### facebook

> Veuillez suivre les instructions ["Comment signaler un compte ou une Page Facebook qui se fait passer pour moi ou pour quelqu'un d'autre ?"](https://www.facebook.com/help/174210519303259) pour demander à ce que le compte soit supprimé.
>
> Veuillez noter qu'il peut s'écouler un certain temps avant que vous ne receviez une réponse à votre demande. Sauvegardez cette page dans vos signets et revenez à ce déroulé dans quelques jours.

Est-ce que ça a marché ?

- [Oui](#resolved_end)
- [Non](#account_end)

### twitter

> Veuillez suivre les étapes ["Signaler un compte usurpant une identité"](https://help.twitter.com/forms/impersonation) pour demander à ce que le compte soit supprimé.
>
> Veuillez noter qu'il peut s'écouler un certain temps avant que vous ne receviez une réponse à votre demande. Sauvegardez cette page dans vos signets et revenez à ce déroulé dans quelques jours.

Cela a-t-il fonctionné ?

- [Oui](#resolved_end)
- [Non](#account_end)


### instagram


> Veuillez suivre les instructions ["Comptes usurpés"](https://help.instagram.com/446663175382270) pour demander à ce que le compte soit supprimé.
>
> Veuillez noter qu'il peut s'écouler un certain temps avant que vous ne receviez une réponse à votre demande. Sauvegardez cette page dans vos signets et revenez à ce déroulé dans quelques jours.

Cela a-t-il fonctionné ?

- [Oui](#resolved_end)
- [Non](#account_end)

### tiktok

> Suivez les instructions dans ["Signaler un compte pour usurpation d'identité"](https://support.tiktok.com/fr/safety-hc/report-a-problem/report-an-impersonation-account) pour demander la suppression du compte usurpant l'identité.
>
> Veuillez noter que la réponse à votre demande peut prendre un certain temps. Enregistrez cette page dans vos signets et revenez à ce questionnaire dans quelques jours.

Cela a-t-il fonctionné ?

- [Oui](#resolved_end)
- [Non](#caccount_end)

### youtube

> Suivez les instructions de la section ["Signaler des vidéos, des chaînes et d'autres contenus inappropriés sur YouTube"](https://support.google.com/youtube/answer/2802027) pour signaler le compte d'usurpation d'identité. Choisissez "Signaler une atteinte à la vie privée" parmi les motifs de signalement possibles.
>
> Veuillez noter que la réponse à votre demande peut prendre un certain temps. Enregistrez cette page dans vos signets et revenez à cette procédure dans quelques jours.

Cela a-t-il fonctionné ?

- Oui](#resolved_end)
- Non](#compte_fin)

### twitch

> Suivez les instructions de ["Comment déposer un rapport d'utilisateur"](https://help.twitch.tv/s/article/how-to-file-a-user-report?language=fr) pour signaler le compte pour usurpation d'identité et demander sa suppression. Choisissez "Usurpation d'identité" parmi les catégories possibles incluses dans le formulaire.
>
> Veuillez noter que la réponse à votre demande peut prendre un certain temps. Enregistrez cette page dans vos signets et revenez à cette procédure dans quelques jours.

Cela a-t-il fonctionné ?

- [Oui](#resolved_end)
- [Non](#account_end)


### fake-website

> Vérifiez si ce site Web est connu comme étant malveillant en recherchant son URL dans les services en ligne suivants :
>
> - [circl.lu/urlabuse](https://circl.lu/urlabuse/)
> - [Virus Total.com](https://www.virustotal.com/)
> - [sitecheck.sucuri.net](https://sitecheck.sucuri.net/)
> - [urlscan.io](https://urlscan.io/)

Le domaine est-il connu pour être malveillant ?

 - [Oui](#malicious-website)
 - [Non](#non-malicious-website)

### malicious-website

> Signalez l'URL à Google Safe Browsing en remplissant le [formulaire "Signaler un logiciel malveillant"](https://safebrowsing.google.com/safebrowsing/report_badware/).
>
> Veuillez noter qu'il se peut qu'il faille un certain temps pour s'assurer que votre signalement soit pris en compte. En attendant, vous pouvez passer à l'étape suivante pour envoyer une demande de fermeture du site au fournisseur d'hébergement et au bureau d'enregistrement du nom de domaine (registrar), ou enregistrer cette page dans vos signets et revenir à ce déroulé dans quelques jours.

Cela a-t-il fonctionné ?

- [Oui](#resolved_end)
- [Non](#non-malicious-website)


### non-malicious-website

> Vous pouvez essayer de signaler le site Web au fournisseur d'hébergement ou au bureau d'enregistrement de domaine, en demandant la fermeture du site.
>
> ***Note:*** *Toujours [documenter](/../../documentation) avant de prendre des mesures telles que la suppression de messages ou de journaux de conversation ou le blocage de profils. Si vous envisagez une action en justice, vous devriez consulter les informations sur la [documentation juridique](/../../documentation#legal).*
>
> Si le site Web que vous voulez signaler utilise votre contenu, vous devrez peut-être prouver que vous êtes le propriétaire légitime du contenu original. Vous pouvez le prouver en présentant votre contrat original avec le bureau d'enregistrement du domaine et/ou le fournisseur d'hébergement, mais vous pouvez également effectuer une recherche sur la [Wayback Machine](https://archive.org/web/), en recherchant à la fois l'URL de votre site Web et le faux site. Si les sites y ont été indexés, vous y trouverez un historique qui peut permettre de montrer que votre site existait avant la publication du faux site.
>
> Pour envoyer une demande de fermeture, vous devrez également recueillir des informations sur le faux site Web :
>
> - Allez à [Network Tools' NSLookup service](https://network-tools.com/nslookup/) et trouvez l’adresse IP (ou les adresses) du faux site internet en entrant son URL dans le formulaire de recherche.
> - Allez à [Domain Tools' Whois Lookup service](https://whois.domaintools.com/) et cherchez le domaine et l’adresse/les adresses IP du faux site internet
> - Enregistrer le nom et l'adresse e-mail abusive du fournisseur d'hébergement et du service d'enregistrement du domaine. S'il est inclus dans les résultats de votre recherche, inscrivez également le nom du propriétaire du site Web.
> - Ecrivez au fournisseur d'hébergement et au bureau d'enregistrement du faux site web pour demander sa fermeture. Dans votre message, indiquez des informations sur l'adresse IP, l'URL et le propriétaire du site Web usurpateur de votre identité, ainsi que les raisons pour lesquelles il est abusif.
> - Vous pouvez utiliser [le modèle de Access Now Helpline's pour dénoncer un site internet cloné à un hébergeur](https://accessnowhelpline.gitlab.io/community-documentation/352-Report_Fake_Domain_Hosting_Provider.html) pour écrire à l’hébergeur du site.
> - Vous pouvez utiliser [le modèle de Access Now Helpline's pour dénoncer une usurpation ou un clonage à un fournisseur de domaine](https://accessnowhelpline.gitlab.io/community-documentation/343-Report_Domain_Impersonation_Cloning.html) pour écrire à un fournisseur de domaine.
>
> Veuillez noter qu'il peut prendre un certain temps avant de recevoir une réponse à vos demandes. Sauvegardez cette page dans vos signets et revenez à ce déroulé dans quelques jours.

Est-ce que cela a marché ?

- [Oui](#resolved_end)
- [Non](#web-protection_end)


### spoofed-email1

> Pour des raisons techniques sous-jacentes, il est assez difficile d'authentifier les e-mails. C'est aussi la raison pour laquelle il est très facile de créer de fausses adresses d'expéditeurs et des e-mails falsifiés.

Vous faites-vous usurper votre identité par le biais de votre adresse e-mail ou d'une adresse similaire, par exemple avec le même nom d'utilisateur, mais un domaine différent ?

- [Je me fais usurper mon identité par mon adresse e-mail](#spoofed-email2)
- [Je me fais usurper mon identité par le biais d'une adresse e-mail similaire](#similar-email)


### spoofed-email2

> La personne qui se fait passer pour vous a peut-être piraté votre compte de messagerie. Pour écarter cette possibilité, essayez de changer votre mot de passe.

Pouvez-vous changer votre mot de passe ?

- [Oui](#spoofed-email3)
- [Non](#hacked-account)

### hacked-account

Si vous ne pouvez pas changer votre mot de passe, votre compte de courriel est probablement compromis. Vous pouvez suivre le questionnaire ["Je ne peux pas accéder à mon compte"](../../../account-access-issues) pour résoudre ce problème.

Cela vous a-t-il aidé à résoudre votre problème ?

- [Oui](#resolved_end)
- [Non](#account_end)

### spoofed-email3

> L'usurpation d'identité consiste à envoyer des messages électroniques avec une adresse d'expéditeur falsifiée. Le message semble provenir d'une personne ou d'un endroit autre que la source réelle.
>
> L'usurpation d'adresse électronique est courante dans les campagnes d'hameçonnage et de pourriels (spams) parce que les gens sont plus susceptibles d'ouvrir un courriel lorsqu'ils pensent qu'il provient d'une source légitime.
>
> Si quelqu'un usurpe votre courriel, vous devriez informer vos contacts pour les avertir du danger de l'hameçonnage (faites-le à partir d'un compte de courriel, d'un profil ou d'un site Web qui est entièrement sous votre contrôle).
>
> Si vous pensez que l'usurpation d'identité visait l'hameçonnage ou d'autres intentions malveillantes, vous pouvez également lire la section [J'ai reçu des messages suspects](../../../suspicious-messages).

Est-ce que les courriels se sont arrêtés après que vous ayez changé le mot de passe de votre compte de courriel ?

- [Oui](#compromised-account)
- [Non](#secure-comms_end)


### compromised-account

> Votre compte a probablement été piraté par quelqu'un qui l'a utilisé pour envoyer des courriels pour se faire passer pour vous. Comme votre compte a été compromis, vous pouvez également lire la section [J'ai perdu l'accès à mes comptes](../../../account-access-issues/).

Cela vous a-t-il aidé à résoudre votre problème ?

- [Oui](#resolved_end)
- [Non](#account_end)


### similar-email

> Si l'usurpateur utilise une adresse e-mail similaire à la vôtre mais avec un nom de domaine ou d'utilisateur différent, il est conseillé d'avertir vos contacts de cette tentative d'usurpation d'identité (à partir d'un compte mail, profil ou site web qui est entièrement sous votre contrôle).
>
> Vous pouvez également lire la section [J'ai reçu des messages suspects](../../../suspicious-messages), car cette usurpation d'identité pourrait viser l'hameçonnage.

Cela vous a-t-il aidé à résoudre votre problème ?

- [Oui](#resolved_end)
- [Non](#secure-comms_end)


### PGP

Pensez-vous que votre clé PGP privée a pu être compromise, par exemple parce que vous avez perdu le contrôle de l'appareil où elle était stockée ?

- [Oui](#PGP-compromised)
- [Non](#PGP-spoofed)

### PGP-compromised

> Créez une nouvelle paire de clés et faites-la signer par des personnes de confiance.
> Informez vos contacts, par le biais d'un canal de confiance que vous contrôlez (tel que Signal ou un autre [outil de chiffrement de bout en bout](https://www.frontlinedefenders.org/fr/resource-publication/guide-secure-group-chat-and-conferencing-tools), qu'ils doivent utiliser votre nouvelle clé et cesser d'utiliser l'ancienne. Dites-leur qu'ils peuvent reconnaître votre nouvelle clé en se basant sur l'empreinte digitale de votre clé réelle. Vous pouvez également leur envoyer directement votre nouvelle clé publique par le même canal de confiance que celui que vous utilisez pour les informer.

Avez-vous besoin de plus d'aide pour résoudre votre problème ?

- [Oui](#secure-comms_end)
- [Non](#resolved_end)

### PGP-spoofed

> Informez vos contacts par le biais d'un canal de confiance que vous contrôlez, tel que Signal ou un autre [outil de chiffrement de bout en bout](https://www.frontlinedefenders.org/fr/resource-publication/guide-secure-group-chat-and-conferencing-tools), que quelqu'un essaie de se faire passer pour vous. Dites-leur qu'ils peuvent reconnaître votre véritable clé en se basant sur l'empreinte digitale de votre véritable clé. Vous pouvez également lui envoyer directement votre clé publique par le même canal de confiance que celui que vous utilisez pour l'informer.

Avez-vous besoin de plus d'aide pour résoudre votre problème ?

- [Oui](#secure-comms_end)
- [Non](#resolved_end)

### doxing

> Si quelqu'un partage vos informations personnelles ou des vidéos ou images privées, nous vous recommandons de suivre le questionnaire de la trousse de premiers soins numériques sur la [Doxing et partage non consensuel de médias privés](../../../doxing).

Que voulez-vous faire ?

- [Me conduire à la section du kit de premiers secours numériques consacrée à la divulgation de données personnelles et au partage non consensuel de médias privés](../../../doxing)
- [J'ai besoin d'aide pour résoudre mon problème](#harassment_end)

### app1

> Si quelqu'un répand une copie malveillante de votre application ou d'un autre logiciel, c'est une bonne idée de faire une communication publique pour avertir les utilisateurs de ne télécharger que la version légitime.
>
> Vous devez également signaler l'application malveillante et demander son retrait.

Où la copie malveillante de votre application est-elle distribuée ?

- [Sur Github](#github)
- [Sur Gitlab.com](#gitlab)
- [Sur Google Play Store](#playstore)
- [Sur l'Apple App Store](#apple-store)
- [Sur un autre site web](#fake-website)

### github

> Si le logiciel malveillant est hébergé sur Github, lisez le [Guide pour la notification d'un Digital Millennium Copyright Act (DMCA) Notification de suppression](https://help.github.com/en/articles/guide-to-submitting-a-dmca-takedown-notice) pour retirer les contenus qui violent les droits d'auteur.
>
> Il se peut qu'il faille attendre un certain temps avant d'obtenir une réponse à votre demande. Enregistrez cette page dans vos favoris et revenez à ce processus de travail dans quelques jours.

Cela vous a-t-il aidé à résoudre votre problème ?

- [Oui](#resolved_end)
- [Non](#app_end)

### gitlab

> Si le logiciel malveillant est hébergé sur Gitlab.com, lisez les conditions requise par Gitlab [Digital Millennium Copyright Act (DMCA) Conditions requises pour notification de suppression](https://about.gitlab.com/handbook/dmca/) pour le retrait des contenus qui violent les droits d'auteur.
>
> Il se peut qu'il faille attendre un certain temps avant d'obtenir une réponse à votre demande. Enregistrez cette page dans vos favoris et revenez à ce processus de travail dans quelques jours.

Cela vous a-t-il aidé à résoudre votre problème ?

- [Oui](#resolved_end)
- [Non](#app_end)


### playstore

> Si l'application malveillante est hébergée sur la boutique Google Play, suivez les étapes de la section ["Supprimer du contenu dans Google"](https://support.google.com/legal/troubleshooter/1114905) pour retirer le contenu qui viole les droits d'auteur.
>
> Il se peut qu'il faille attendre un certain temps avant d'obtenir une réponse à votre demande. Enregistrez cette page dans vos favoris et revenez à ce processus de travail dans quelques jours.

Cela vous a-t-il aidé à résoudre votre problème ?

- [Oui](#resolved_end)
- [Non](#app_end)


### apple-store

> Si l'application malveillante est hébergée sur l'App Store, remplissez le ["Apple App Store Formulaire de litige sur les contenus"](https://www.apple.com/legal/internet-services/itunes/appstorenotices/#/contacts?lang=fr) pour retirer le contenu qui viole les droits d'auteur.
>
> Il se peut qu'il faille attendre un certain temps avant d'obtenir une réponse à votre demande. Enregistrez cette page dans vos favoris et revenez à ce processus de travail dans quelques jours.

Cela vous a-t-il aidé à résoudre votre problème ?

- [Oui](#resolved_end)
- [Non](#app_end)

### physical-sec_end

> Si vous craignez pour votre bien-être physique, veuillez contacter les organisations ci-dessous qui peuvent vous soutenir.

:[](organisations?services=physical_sec)

### harassment_end

> Si vos informations privées ou vos médias ont été publiés sans votre consentement et que vous avez besoin d'aide pour résoudre votre problème, vous pouvez contacter les organisations ci-dessous qui peuvent vous aider.
>
> Avant de contacter une organisation, nous vous recommandons vivement de suivre le questionnaire de la trousse de premiers soins numériques sur le [Doxing et partage non consensuel de médias privés](../../../doxing).), car il vous aidera à comprendre ce qui vous arrive exactement.

:[](organisations?services=harassment)

### account_end

> Si vous êtes toujours victime d'usurpation d'identité ou si votre compte est toujours compromis, veuillez contacter les organisations ci-dessous qui peuvent vous aider.

:[](organisations?services=account&services=legal)


### app_end

> Si la fausse application n'a pas été retirée, veuillez contacter les organisations ci-dessous qui peuvent vous aider.

:[](organisations?services=account&services=legal)

### web-protection_end

> Si vos demandes de retrait n'ont pas abouti, vous pouvez essayer de contacter les organisations ci-dessous pour obtenir de l'aide supplémentaire.

:[](organisations?services=web_protection)

### secure-comms_end

> Si vous avez besoin d'aide ou de recommandations sur l'hameçonnage, la sécurité et le chiffrement des courriels et la sécurité des communications en général, vous pouvez communiquer avec ces organisations :

:[](organisations?services=secure_comms)


### resolved_end

Nous espérons que ce guide des premiers soins numériques (DFAK) a été utile. Veuillez nous faire part de vos commentaires [par email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

Pour éviter toute autre tentative d'usurpation d'identité, lisez les conseils ci-dessous.

### final_tips

- Créez des mots de passe solides, complexes et uniques pour tous vos comptes.
- Pensez à utiliser un gestionnaire de mots de passe pour créer et stocker les mots de passe afin de pouvoir utiliser de nombreux mots de passe différents sur différents sites et services sans avoir à les mémoriser.
- Activez l'authentification à deux facteurs (2FA) pour vos comptes les plus importants. 2FA offre une plus grande sécurité de compte en exigeant l'utilisation de plus d'une méthode pour se connecter à vos comptes. Cela signifie que même si quelqu'un s'emparait de votre mot de passe principal, il ne pourrait pas accéder à votre compte à moins qu'il n'ait également votre téléphone portable ou un autre moyen secondaire d'authentification.
- Vérifiez vos profils sur les plateformes de réseautage social. Certaines plateformes offrent une fonction permettant de vérifier votre identité et de la relier à votre compte.
- Configurez des alertes Google. Vous pouvez recevoir des e-mails lorsque de nouveaux résultats pour un sujet apparaissent dans Google Search. Par exemple, vous pouvez obtenir des informations sur les mentions de votre nom ou de celui de votre organisation/collectif.
- Prenez une capture d’écran de votre page web telle qu'elle apparaît maintenant pour l'utiliser comme preuve à l'avenir. Si votre site web autorise les robots d’exploration, vous pouvez utiliser la Wayback Machine, proposée par archive.org. Visitez l’[Internet Archive Wayback Machine](https://web.archive.org/), entrez le nom de votre site web dans le champ situé sous l'en-tête "Save Page Now", et cliquez sur le bouton "Save Page".


#### resources

- [Security Self-Defense: Créer des mots de passe forts et uniques](https://ssd.eff.org/fr/module/cr%C3%A9er-des-mots-de-passe-robustes)
- [Security Self-Defense: Aperçu animé en utilisant des gestionnaires de mots de passe](https://ssd.eff.org/en/module/animated-overview-using-password-managers-stay-safe-online)
- [Guide pratique : activer l’authentification à deux facteurs](https://ssd.eff.org/module/how-enable-two-factor-authenticationhttps://ssd.eff.org/fr/module/guide-pratique-activer-l%E2%80%99authentification-%C3%A0-deux-facteurs)
- [Archive.org: Archiver votre site internet](https://archive.org/web/)
- [Security Self-Defense: Comment se servir de KeePassXC- un gestionnaire de mots de passe libre](https://ssd.eff.org/fr/module/guide-pratique-utiliser-keepassxc)
- [Access Now Helpline Community Documentation: Choosing a Password Manager](https://communitydocs.accessnow.org/295-Password_managers.html)
