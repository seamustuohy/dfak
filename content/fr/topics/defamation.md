﻿---
layout: page
title: "Je suis la cible d'une campagne de diffamation"
author: Inés Binder, Florencia Goldsman, Erika Smith, Gus Andrews
language: fr
summary: "Que faire lorsque quelqu'un tente de nuire à votre réputation en ligne ?"
date: 2023-04
permalink: /fr/topics/defamation
parent: /fr/
---

# I'm being targeted by a defamation campaign

Lorsque quelqu'un crée et diffuse délibérément des informations fausses, manipulées ou trompeuses dans le but de nuire à la réputation d'une personne, il s'agit d'une campagne de diffamation. Ces campagnes peuvent viser des personnes ou des organisations ayant une certaine exposition publique. Elles peuvent être orchestrées par n'importe quel acteur, gouvernemental ou privé.

Bien que ce phénomène ne soit pas nouveau, l'environnement numérique diffuse rapidement ces messages, ce qui accroît l'ampleur et la rapidité de l'attaque et renforce son impact.

Les campagnes de diffamation ont une dimension liée au genre lorsqu'elles visent à exclure les femmes et les personnes LGBTQIA+ de la vie publique, en diffusant des informations qui "exploitent les inégalités entre les sexes, promeuvent l'hétéronormativité et accentuent les clivages sociaux" [[1]](#note-1).

Cette section de la trousse de premiers soins numériques vous guidera à travers quelques étapes de base pour planifier la manière de répondre à une campagne de diffamation. Suivez ce questionnaire pour identifier la nature de votre problème et trouver des solutions possibles.

<a name="note-1"></a>
[1] [Comprendre les dimensions de genre de la désinformation](https://counteringdisinformation.org/fr/topics/gender/0-apercu-genre-et-desinformation)

## Workflow

### physical-wellbeing

Craignez-vous pour votre sécurité physique ou votre bien-être ?

- [Oui](#physical-risk_end)
- [Non](#no-physical-risk)

### no-physical-risk

> Une campagne de diffamation vise à attaquer votre réputation en la remettant en question, en jetant le doute, en cernant la cible, en alléguant des mensonges ou en exposant des contradictions afin d'éroder la confiance du public.
>
> Ces attaques touchent particulièrement les personnes exposées au public et dont le travail dépend de leur prestige et de leur confiance : les militants et les défenseurs des droits de l'homme, les hommes politiques, les journalistes et les personnes qui mettent des données à la disposition du public, les artistes, etc.

Cette attaque vise-t-elle à saper votre réputation ?

 - [Oui](#perpetrators)
 - [Non](#no-reputational-damage)

### no-reputational-damage

> Si vous êtes confronté à une attaque qui ne vise pas à porter atteinte à votre réputation, il s'agit peut-être d'une urgence d'une autre nature qu'une campagne de diffamation.

Voulez-vous passer en revue les questions permettant de diagnostiquer le harcèlement en ligne ?

 - [Oui, je suis peut-être victime de harcèlement en ligne](../../../harassed-online)
 - [Non, je pense toujours qu'il s'agit d'une campagne de diffamation](#perpetrators)
 - [J'ai besoin de soutien pour comprendre le problème auquel je suis confronté](/../../support)


### perpetrators

Savez-vous qui se cache derrière cette campagne de diffamation ?

 - [Oui](#respond-defamation)
 - [Non](#analyze-messages)

### respond-defamation

> Il existe de nombreuses façons de réagir à une campagne de diffamation, en fonction de l'impact et de la diffusion des messages, des acteurs impliqués et de leurs motivations. Si la campagne a un impact et une diffusion faibles, nous recommandons de l'ignorer. Si, au contraire, elle a un impact et une diffusion importants, vous pouvez envisager de signaler et de supprimer le contenu, de rétablir la vérité et de la réduire au silence.
>
> Veillez à ne pas amplifier les messages diffamatoires lorsque vous tentez de répondre à la campagne de diffamation. Le fait de citer un message, ne serait-ce que pour exposer les personnes ou les motivations qui en sont à l'origine, peut accroître sa diffusion. Prenez en considération la ["technique du sandwich de vérité"](https://en.wikipedia.org/wiki/Truth_sandwich) afin de couvrir les fausses informations sans en favoriser involontairement la diffusion. Cette technique "consiste à présenter la vérité sur un sujet afin de couvrir ensuite la désinformation, puis de terminer l'article en présentant à nouveau la vérité".
>
> Choisissez les stratégies qui vous semblent les plus appropriées pour contenir l'attaque, restaurer la confiance et rétablir la crédibilité de votre communauté. N'oubliez pas que, quelle que soit la stratégie choisie, le fait de [prendre soin de vous](/../../self-care/) doit être la priorité absolue. Pensez également à [documenter](/../../documentation) le contenu ou les profils qui vous attaquent avant de répondre.

Comment voulez-vous répondre à la campagne de diffamation ?

 - [Je veux avertir les plateformes de médias sociaux et retirer le contenu diffamatoire](#notify-take-down)
 - [Je veux l'ignorer et faire taire les notifications](#ignore-silence)
 - [Je veux rétablir la vérité](#set-record-straight)
 - [Je veux déposer une plainte en justice](#plainte-en-loi)
 - [J'ai besoin d'analyser plus en détail les messages de diffamation pour pouvoir prendre une décision](#analyse-messages)

### analyze-messages

> Lorsque vous ou votre organisation êtes la cible d'une campagne de diffamation, la compréhension des méthodes standard utilisées pour diffuser la désinformation peut vous aider à évaluer les risques et à orienter vos prochaines actions. L'analyse des messages que vous recevez lors de ce type d'attaque peut vous fournir des informations supplémentaires sur les auteurs, les motivations et les ressources utilisées pour saper votre réputation. Les techniques d'attaque font appel à toute une série d'outils et à de la coordination en ligne.
>
> Vous pouvez commencer par évaluer le niveau de risque auquel vous êtes confronté. Dans le [Interaction Disinformation Toolkit](https://www.interaction.org/documents/disinformation-toolkit/), vous trouverez un outil d'évaluation des risques qui vous aidera à évaluer la vulnérabilité de votre environnement médiatique.
>
> Une campagne de diffamation sur les médias sociaux utilisera fréquemment des hashtags pour susciter un plus grand intérêt, mais cela est également utile pour surveiller l'attaque, car vous pouvez effectuer une recherche par hashtag et évaluer les auteurs et les messages. Pour mesurer l'impact des hashtags sur les médias sociaux, vous pouvez essayer les outils suivants :
>
> - [Suivre mon hashtag](https://https://www.trackmyhashtag.com/)
> - [Brand mentions](https://brandmentions.com/hashtag-tracker) regroupe les mentions sur les hashtags de Twitter, Instagram et Facebook en un seul flux de données.
> - [InVid Project](https://www.invid-project.eu/) est une plateforme de vérification des connaissances qui permet de détecter les histoires émergentes et d'évaluer la fiabilité des fichiers vidéo et des contenus dignes d'intérêt diffusés via les médias sociaux.
> - [Metadata2go](https://www.metadata2go.com/view-metadata) découvre les métadonnées qui se cachent derrière les fichiers que vous analysez.
> - [YouTube DataViewer](https://citizenevidence.amnestyusa.org/) peut être utilisé pour effectuer une recherche inversée des vidéos d'Amnesty International afin de voir si la vidéo est plus ancienne et si elle a été modifiée.
>
> **Manual vs Automated Attacks**
>
> Les acteurs étatiques et d'autres adversaires utilisent fréquemment des réseaux de robots informatiques coordonnés qui ne sont ni coûteux ni techniquement difficiles à mettre en place. Reconnaître qu'une vague de messages d'attaque ne provient pas de dizaines ou de centaines de personnes mais de robots automatisés peut réduire l'anxiété et faciliter la documentation, et vous aidera à décider de la manière avec laquelle vous voulez vous défendre. Utilisez [Botsentinel](https://botsentinel.com/) pour vérifier la nature des comptes qui vous attaquent. Certains messages de robots peuvent également être repris et diffusés par des individus, vous trouverez peut être que le score intègre certains comptes qui transmettent des messages de désinformation, c'est-à-dire qu'ils ont des abonnés, une image de profil et un contenu uniques, ainsi que d'autres indicateurs qui réduisent la probabilité qu'il s'agisse d'un compte de robot. Un autre outil que vous pouvez utiliser est [Pegabot](https://es.pegabot.com.br/).
>
> **Internal Information vs Public Information**
>
> Les désinformateurs habiles peuvent fonder leur contenu sur un noyau de vérité ou le faire paraître véridique. Une réflexion sur le type d'informations partagées et leurs sources peut déterminer différentes lignes de conduite.
>
> Les informations sont-elles privées ou proviennent-elles de sources internes ? Envisagez de changer vos mots de passe et d'installer un système d'authentification à deux facteurs pour vous assurer que personne d'autre que vous n'accède à vos comptes. Si ces informations sont mentionnées dans des chats ou dans des conversations ou des fichiers partagés, encouragez vos amis et collègues à revoir leurs paramètres de sécurité. Envisagez de nettoyer et de fermer les anciens salons de discussion afin que les informations du passé ne soient pas facilement accessibles sur votre appareil ou celui de vos collègues et amis.
>
> Des adversaires peuvent détourner les comptes d'une personne ou d'une organisation pour diffuser des [fausses informations](https://fr.wikipedia.org/wiki/Infox) et des [désinformations](https://fr.wikipedia.org/wiki/D%C3%A9sinformation) à partir de ce compte, ce qui confère aux informations partagées une plus grande légitimité. Pour reprendre le contrôle de vos comptes, consultez la section de la trousse de premiers soins numériques [Je ne peux pas accéder à mon compte](../../../account-access-issues/questions/what-type-of-account-or-service/).
>
> Les informations diffusées proviennent-elles d'informations publiques disponibles en ligne ? Il peut être utile de suivre la section de la trousse de premiers soins numériques [J'ai été victime de doxxing ou quelqu'un partage des images non consensuelles de moi] (../../../doxing) pour rechercher et circonscrire l'endroit où ces informations ont été rendues disponibles et ce qui peut être fait.
>
> **Création de matériel plus sophistiqué** (vidéos YouTube, deepfakes, etc.)
>
> Parfois, les attaques sont plus ciblées et les auteurs ont investi du temps et des efforts dans la création de fausses images et de fausses informations dans des vidéos YouTube ou des [deepfakes](https://fr.wikipedia.org/wiki/Deepfake) afin d'étayer leur fausse histoire. La diffusion de ce matériel peut révéler les auteurs et donner plus de matière à la dénonciation légale et publique de l'attaque, ainsi que des motifs clairs pour les demandes de retrait. Cela peut également signifier que les attaques sont hautement personnalisées et augmentent le sentiment de vulnérabilité et de risque. Comme pour toute attaque, les [soins personnels](/../../self-care/) et la sécurité personnelle sont des priorités absolues.
>
> Un autre exemple est l'utilisation de faux domaines dans lesquels un adversaire crée un site web ou un profil de réseau social qui ressemble au compte authentique. Si vous êtes confronté à un tel problème, consultez la section de la trousse de premiers soins numériques consacrée à l'[usurpation d'identité](../../../impersonated/)..
>
> Ce type d'attaque peut également indiquer que l'attaquant dispose d'une organisation, d'une logistique, d'un financement et d'un temps plus importants, ce qui permet de mieux comprendre les motivations de l'attaque et ses auteurs éventuels.

Avez-vous maintenant une meilleure idée de qui vous attaque et de son mode opératoire ?

 - [Oui, j'aimerais maintenant envisager une stratégie de riposte](#respond-defamation)
 - [Non, j'ai besoin d'aide pour déterminer comment réagir](#defamation_end)

### notify-take-down

> ***Note:*** *Pensez à [documenter les attaques](/../../documentation) avant de demander à une plateforme de retirer un contenu diffamatoire. Si vous envisagez une action en justice, vous devriez consulter les informations sur la [documentation juridique](/../../documentation#legal).*
>
> Les conditions de service des différentes plateformes expliquent quand les demandes de retrait sont considérées comme légitimes, par exemple dans le cas d'un contenu en violation des droits d'auteur, dont vous devez être le propriétaire. Le droit d'auteur s'applique par défaut à partir du moment où vous créez une œuvre originale : vous prenez une photo, écrivez un texte, composez une chanson, filmez une vidéo, etc. Il ne suffit pas que vous apparaissiez dans l'œuvre, vous devez l'avoir créée.

Détenez-vous les droits sur le contenu utilisé dans la campagne de diffamation ?

 - [Oui](#copyright)
 - [Non](#report-defamatory-content)

### set-record-straight

> Votre version des faits est toujours importante. Il peut être plus urgent de rétablir la vérité si la campagne de diffamation dont vous faites l'objet est généralisée et très préjudiciable à votre réputation. Toutefois, dans ce cas, vous devez évaluer l'impact de la campagne sur vous afin de déterminer si une réponse publique est la meilleure stratégie à adopter.
>
> Outre le signalement des contenus et des personnes mal intentionnées aux plateformes, vous pouvez souhaiter élaborer un contre-récit, démystifier la désinformation, dénoncer publiquement les auteurs ou simplement rétablir la vérité. La manière dont vous réagirez dépendra également de la communauté de soutien dont vous disposez pour vous aider et de votre propre évaluation du risque auquel vous pourriez être confronté en prenant une position publique.

Disposez-vous d'une équipe pour vous soutenir ?

 - [Oui](#campaign-team)
 - [Non](#self-campaign)

### legal-complaint

> Lorsque vous êtes confronté·e à une procédure judiciaire, que vous rassemblez, organisez et présentez des preuves au tribunal, vous devez suivre un protocole spécifique afin qu'elles puissent être acceptées par le juge en tant que pièces à conviction. Lorsqu'il s'agit de présenter une preuve en ligne d'une attaque, il ne suffit pas de faire une capture d'écran. Consultez la section de la trousse de premiers soins numériques [comment documenter une attaque pour la présenter dans le cadre d'une procédure judiciaire](/../../documentation#legal) pour en savoir plus sur ce processus avant de prendre contact avec l'avocat de votre choix.

Que souhaitez-vous faire ?

 - [Ces recommandations ont été utiles - je sais ce qu'il faut faire maintenant](#resolved_end)
 - [Ces recommandations ont été utiles, mais j'aimerais envisager une autre stratégie](#respond-defamation)
 - [J'ai besoin d'aide pour poursuivre ma plainte en justice](#legal_end)

### ignore-silence

> Il arrive, surtout lorsque la campagne de diffamation n'a que peu d'impact ou de diffusion, que vous préfériez ignorer les auteurs et simplement faire taire les messages. Il peut également s'agir d'une étape importante pour prendre soin de soi, et des amis ou des alliés peuvent être sollicités pour surveiller le contenu et vous alerter si d'autres actions sont nécessaires. Cela n'élimine pas le contenu, au cas où vous souhaiteriez plus tard documenter ou examiner les informations diffusées pour vous nuire.

Où sont diffusés les contenus diffamatoires que vous souhaitez faire taire ?

 - [Email](#silence-email)
 - [Facebook](#silence-facebook)
 - [Instagram](#silence-instagram)
 - [Reddit](#silence-reddit)
 - [TikTok](#silence-tiktok)
 - [Twitter](#silence-twitter)
 - [Whatsapp](#silence-whatsapp)
 - [YouTube](#silence-youtube)

### copyright

> Vous pouvez recourir à la réglementation sur les droits d'auteur, telle que le [Digital Millennium Copyright Act (DMCA)](https://fr.wikipedia.org/wiki/Digital_Millennium_Copyright_Act), pour retirer un contenu. La plupart des plateformes de médias sociaux proposent des formulaires pour signaler les violations de droits d'auteur et peuvent être plus réceptives à ce type de demande qu'à d'autres considérations de retrait, en raison des implications juridiques pour elles.
>
> ***Note:*** *Toujours [documenter](/../../documentation) avant de demander le retrait d'un contenu. Si vous envisagez une action en justice, vous devriez consulter les informations sur la [documentation juridique](/../../documentation#legal).*
>
> Vous pouvez utiliser ces liens pour envoyer une demande de retrait pour violation des droits d'auteur aux principales plateformes de réseaux sociaux :
>
> - [Archive.org](https://help.archive.org/help/how-do-i-request-to-remove-something-from-archive-org/)
> - [Facebook](https://www.facebook.com/help/1020633957973118/?helpref=hc_fnav)
> - [Instagram](https://help.instagram.com/contact/552695131608132)
> - [TikTok](https://www.tiktok.com/legal/report/Copyright)
> - [Twitter](https://help.twitter.com/fr/forms/ipi)
> - [YouTube](https://support.google.com/youtube/answer/2807622)
>
> Veuillez noter que la réponse à vos demandes peut prendre un certain temps. Enregistrez cette page dans vos signets et revenez à ce proessus de travail dans quelques jours.

Le contenu a-t-il été supprimé ?

 - [Oui](#resolved_end)
 - [Non, j'ai besoin d'une assistance juridique](#legal_end)
 - [Non, j'ai besoin d'aide pour contacter la plateforme](#harassment_end)

### report-defamatory-content

> Toutes les plateformes ne disposent pas de procédures spécialisées pour signaler les contenus diffamatoires, et certaines d'entre elles exigent une procédure juridique préalable. Vous pouvez consulter ci-dessous la marche à suivre établie par chaque plateforme pour signaler un contenu diffamatoire.
>
> - [Archive.org](https://help.archive.org/help/how-do-i-request-to-remove-something-from-archive-org/)
> - [Facebook](https://www.facebook.com/help/contact/430253071144967)
> - [Instagram](https://help.instagram.com/contact/653100351788502)
> - [TikTok](https://www.tiktok.com/legal/report/feedback)
> - [Twitch](https://help.twitch.tv/s/article/how-to-file-a-user-report?language=fr)
> - [Twitter](https://help.twitter.com/fr/rules-and-policies/twitter-report-violation#directly)
> - [Whatsapp](https://faq.whatsapp.com/1142481766359885?helpref=search&cms_platform=android)
> - [YouTube](https://support.google.com/youtube/answer/6154230)
>
> Veuillez noter que la réponse à vos demandes peut prendre un certain temps. Enregistrez cette page dans vos signets et revenez à ce processus de travail dans quelques jours.

Le contenu a-t-il été signalé et analysé par le fournisseur en vue de sa suppression ?

 - [Yes](#resolved_end)
 - [No, I need legal support](#legal_end)
 - [No, I need support contacting the platform](#harassment_end)

### silence-email

> La plupart des clients de messagerie offrent la possibilité de filtrer les messages et de les marquer automatiquement, de les archiver ou de les supprimer afin qu'ils n'apparaissent pas dans votre boîte de réception. En général, vous pouvez créer des règles pour filtrer les messages en fonction de l'expéditeur, du destinataire, de la date, de la taille, de la pièce jointe, de l'objet ou de mots-clés.
>
> Apprenez à créer des filtres dans les webmails :
>
> - [Gmail](https://support.google.com/mail/answer/6579?hl=fr#zippy=%2Ccreate-a-filter)
> - [Protonmail](https://proton.me/support/email-inbox-filters)
> - [Yahoo](https://help.yahoo.com/kb/SLN28071.html)
>
> Apprenez à créer des filtres dans les clients de messagerie :
>
> - [MacOS Mail](https://support.apple.com/fr-fr/guide/mail/mlhl1f6cf15a/mac)
> - [Microsoft Outlook](https://support.microsoft.com/fr-fr/office/configurer-des-r%C3%A8gles-dans-outlook-75ab719a-2ce8-49a7-a214-6d62b67cbd41)
> - [Thunderbird](https://support.mozilla.org/fr/kb/classer-vos-messages-en-utilisant-des-filtres#)

Voulez-vous faire taire les contenus diffamatoires sur une autre plateforme ?

 - [Oui](#ignore-silence)
 - [Non](#damage-control)

### silence-twitter

> Si vous ne souhaitez pas être exposé·e à des messages spécifiques, vous pouvez bloquer des utilisateurs ou réduire au silence des utilisateurs et des messages. N'oubliez pas que si vous choisissez de bloquer quelqu'un ou un contenu, vous n'aurez pas accès au contenu diffamatoire pour [documenter l'attaque](/../../documentation). Dans ce cas, vous préférerez peut-être y avoir accès par l'intermédiaire d'un autre compte ou simplement le réduire au silence.
>
> - [Masquer des comptes pour qu'ils n'apparaissent pas sur votre ligne temporelle](https://help.twitter.com/fr/using-twitter/twitter-mute)
> - [Masquer des mots, des phrases, des noms d'utilisateur, des emojis, des hashtags et des notifications pour une conversation](https://help.twitter.com/fr/using-twitter/advanced-twitter-mute-options)
> - [Mettre en sommeil pour toujours les notifications de messages directs](https://help.twitter.com/fr/using-twitter/direct-messages#snooze)

Souhaitez-vous faire taire les contenus diffamatoires sur une autre plateforme ?

 - [Oui](#ignore-silence)
 - [Non](#damage-control)

### silence-facebook

> Bien que Facebook ne permette pas de mettre en sourdine des utilisateurs ou du contenu, il permet de bloquer des profils et des pages d'utilisateurs.
>
> - [Bloquer un profil Facebook](https://www.facebook.com/help/)
> - [Bloquer les messages d'un profil sur Facebook](https://www.facebook.com/help/1682395428676916)
> - [Bloquer une page Facebook](https://www.facebook.com/help/395837230605798)
> - [Interdire ou bloquer des profils de votre page Facebook](https://www.facebook.com/help/185897171460026/)

Voulez-vous faire taire un contenu diffamatoire sur une autre plateforme ?

 - [Oui](#ignore-silence)
 - [Non](#damage-control)

### silence-instagram

> Voici une liste de conseils et d'outils pour réduire au silence les utilisateurs et les conversations sur Instagram :
>
> - [Mettre en sourdine ou réactiver un·e internaute sur Instagram](https://help.instagram.com/469042960409432)
> - [Mettre en sourdine ou réactiver la story Instagram de quelqu’un](https://help.instagram.com/290238234687437)
> - [restreindre ou restaurer l’accès d’un utilisateur sur Instagram](https://help.instagram.com/2638385956221960)
> - [Désactiver les suggestions de compte pour votre profil Instagram](https://help.instagram.com/530450580417848)
> - [Masquer des publications suggérées dans votre fil Instagram](https://help.instagram.com/423267105807548)

Voulez-vous faire taire les contenus diffamatoires sur une autre plateforme ?

 - [Oui](#ignore-silence)
 - [Non](#damage-control)

### silence-youtube

> Si vous accédez à YouTube à partir de votre navigateur de bureau, vous pouvez supprimer des vidéos, des chaînes, des sections et des listes de lecture de votre page d'accueil.
>
> - [Ajuster vos recommandations YouTube](https://support.google.com/youtube/answer/6342839)
>
> Vous pouvez également créer une liste de blocage dans votre profil YouTube pour bloquer d'autres spectateurs sur votre chat en direct sur YouTube.
>
> - YouTube : [myaccount.google.com/blocklist](https://myaccount.google.com/blocklist)
> - [Bloquer d'autres spectateurs sur le chat en direct sur YouTube](https://support.google.com/youtube/answer/7663906)

Souhaitez-vous faire taire les contenus diffamatoires sur une autre plateforme ?

 - [Oui](#ignore-silence)
 - [Non](#damage-control)

### silence-whatsapp

> WhatsApp est une plateforme fréquemment utilisée  pour les campagnes de diffamation. Les messages circulent rapidement en raison des relations de confiance entre les expéditeurs et les destinataires.
>
> - [Archiver ou désarchiver une discussion ou un groupe](https://faq.whatsapp.com/1426887324388733)
> - [Quitter et supprimer des groupes](https://faq.whatsapp.com/498814665492149)
> - [Bloquer et signaler un contact](https://faq.whatsapp.com/1142481766359885)
> - [Mettre en sourdine ou rétablir le son des notifications de groupe](https://faq.whatsapp.com/694350718331007)

Souhaitez-vous faire taire un contenu diffamatoire sur une autre plateforme ?

 - [Oui](#ignore-silence)
 - [Non](#damage-control)

### silence-tiktok

> TikTok vous permet de bloquer des utilisateurs, d'empêcher les utilisateurs de commenter vos vidéos, de créer des filtres pour les commentaires sur vos vidéos et de supprimer, masquer et filtrer les messages directs.
>
> - [Bloquez des utilisateurs sur TikTok](https://support.tiktok.com/fr/using-tiktok/followers-and-following/blocking-the-users)
> - [Choisissez qui peut commenter vos vidéos dans vos paramètres](https://support.tiktok.com/fr/using-tiktok/messaging-and-notifications/comments)
> - [Activez les filtres de commentaires pour vos vidéos TikTok](https://support.tiktok.com/fr/using-tiktok/messaging-and-notifications/comments#3)
> - [Gérez les paramètres de confidentialité des commentaires pour toutes vos vidéos TikTok](https://support.tiktok.com/fr/using-tiktok/messaging-and-notifications/comments#4)
> - [Gérez les paramètres de confidentialité des commentaires pour l'une de vos vidéos TikTok](https://support.tiktok.com/fr/using-tiktok/messaging-and-notifications/comments#5)
> - [Comment supprimer, mettre en sourdine et filtrer les messages directs](https://support.tiktok.com/fr/account-and-privacy/account-privacy-settings/direct-message#6)
> - [Comment gérer les personnes autorisées à vous envoyer des messages directs](https://support.tiktok.com/fr/account-and-privacy/account-privacy-settings/direct-message#7)

Souhaitez-vous faire taire un contenu diffamatoire sur une autre plateforme ?

 - [Oui](#ignore-silence)
 - [Non](#damage-control)

### silence-reddit

> Si des propos diffamatoires circulent sur Reddit, vous pouvez choisir d'exclure les communautés où ces propos apparaissent dans vos notifications, votre fil d'actualité (y compris les recommandations) et le fil d'actualité populaire sur votre espace Reddit ou sur reddit.com. Si vous êtes modérateur, vous pouvez également bannir ou mettre en sourdine les utilisateurs qui enfreignent de manière répétée les règles de votre communauté.
>
> - [Reddit Community Muting](https://support.reddithelp.com/hc/fr/articles/9810475384084-What-is-community-muting-)
> - [Bannissement et mise en sourdine des membres de la communauté](https://mods.reddithelp.com/hc/fr-fr/articles/360009161872-User-Management-banning-and-muting)

Souhaitez-vous faire taire les contenus diffamatoires sur une autre plateforme ?

 - [Oui](#ignore-silence)
 - [Non](#damage-control)


### campaign-team

> Si la campagne menée contre vous implique du temps et des ressources, cela indique que les auteurs ont une motivation profonde pour vous attaquer. Il se peut que vous deviez utiliser toutes les stratégies mentionnées dans ce flux de travail pour atténuer les dommages et mettre en place une campagne publique pour vous défendre. Les amis et les alliés peuvent être utiles et vous aider à prendre soin de vous s'ils vous soutiennent dans l'analyse des messages, la documentation des attaques et la rédaction de rapports sur le contenu et les profils.
>
> Une équipe de campagne peut travailler ensemble pour décider de la meilleure façon de contester le discours utilisé contre vous. Les messages de désinformation sont insidieux et on nous dit souvent de ne pas relayer les messages des attaquants. Il est important de trouver un équilibre entre la mise en valeur de votre histoire et la réfutation des fausses affirmations.
>
> Il est essentiel de procéder à une [analyse d'évaluation des risques](https://www.interaction.org/documents/disinformation-toolkit/) pour vous-même et toutes les personnes et organisations qui vous soutiennent avant de vous lancer dans une campagne publique.
>
> Outre l'analyse des messages d'attaque, vous et votre équipe pouvez commencer par [recenser les organisations et les alliés](https://holistic-security.tacticaltech.org/exercises/explore/visual-actor-mapping-part-1.html) qui reconnaissent votre parcours et qui contribueront à une description positive de votre travail, ou qui feront suffisamment de bruit pour détourner ou étouffer une campagne de diffamation.
>
> Vous et votre organisation n'êtes peut-être pas les seul·es à être attaqué·es. Envisagez d'instaurer un climat de confiance avec d'autres organisations et communautés qui ont été victimes de discrimination ou ciblées par la campagne de désinformation. Il peut s'agir de préparer les membres de la communauté à répondre à des messages controversés ou à partager des ressources afin que d'autres puissent se défendre.
>
> Prenez le temps, au sein de votre équipe, d'identifier ce que vous souhaitez provoquer via les messages et les contenus diffusés, selon que vous réfutez la campagne ou que vous diffusez des messages positifs sans faire référence aux tactiques de diffamation. Vous souhaiterez peut-être que les gens soient plus aptes à différencier les rumeurs des faits. Un message simple basé sur des valeurs est particulièrement efficace dans toute campagne publique.
>
> Outre les déclarations publiques ou de presse, vous pouvez choisir et préparer différents porte-parole pour votre campagne, ce qui permet également d'éviter les attaques personnalisées courantes dans les campagnes de diffamation, en particulier dans les cas de désinformation sexiste.
>
> La mise en place d'un système de suivi des médias pour suivre votre campagne de messages et celle de vos attaquants vous permettra de voir quels messages ont le plus d'impact et d'affiner votre campagne.
>
> Si le contenu diffamatoire est également publié dans un journal ou un magazine, votre équipe peut souhaiter exiger un [droit de réponse](#self-strategies) ou activer votre dossier auprès de [sites de vérification des faits](#self-strategies).

Pensez-vous avoir été en mesure de contrôler les dommages causés à votre réputation ?

 - [Oui](#resolved_end)
 - [Non](#defamation_end)

### self-campaign

> Si vous ne bénéficiez pas du soutien d'une équipe, vous devez tenir compte des limites de vos capacités et de vos ressources lorsque vous planifiez vos prochaines étapes, en donnant la priorité au fait de [prendre soin de vous](/../../self-care). Vous pouvez publier une déclaration sur votre profil de médias sociaux ou sur votre site web. Assurez-vous d'avoir le [contrôle total](../../../account-access-issues) de ces comptes avant de le faire. Dans votre déclaration, vous pouvez renvoyer les gens à votre parcours reconnu et aux sources dignes de confiance qui reflètent ce parcours. Votre [analyse de la campagne de diffamation](#analyze-messages) vous donnera des indications sur le type de déclaration que vous souhaitez faire. Par exemple, vous pouvez vérifier si des sources d'information sont citées dans l'attaque.

Le contenu diffamatoire est-il également publié dans un journal ou un magazine ?

 - [Oui](#self-strategies)
 - [Non](#damage-control)

### self-strategies

> Si les informations diffamatoires sont publiées, la politique éditoriale ou même la loi d'un pays donné peut vous accorder un [droit de réponse ou droit de correction](https://fr.wikipedia.org/wiki/Droit_de_r%C3%A9ponse). Cela vous donne non seulement une autre plateforme pour publier votre déclaration, au-delà de vos propres médias, mais cela vous permet également d'établir un lien avec d'autres déclarations que vous faites circuler.
>
> Si le média contenant des informations diffamatoires n'est pas une source d'information crédible ou s'il est fréquemment à l'origine d'informations infondées, il peut être utile de le signaler dans toute déclaration.
>
> Il existe de nombreux vérificateurs de faits locaux, régionaux et mondiaux qui peuvent vous aider à démystifier les informations qui circulent à votre sujet.
>
> - [DW Fact Check](https://www.dw.com/en/fact-check/t-56584214)
> - [France24 LesObservateurs](https://observers.france24.com/fr/)
> - [AFP FactCheck](https://factcheck.afp.com/)
> - [EUvsDisinfo](https://euvsdisinfo.eu/)
> - [Latam Chequea](https://chequeado.com/latamchequea)
> - [Europa fact check](https://eufactcheck.eu/)

Pensez-vous avoir besoin d'apporter une réponse plus conséquente à la campagne de diffamation ?

 - [Oui](#campaign-team)
 - [Non](#damage-control)

### damage-control

> Votre analyse de la campagne de diffamation vous donnera des indications sur le type d'actions que vous souhaitez entreprendre. Par exemple, si des messages robots sont utilisés contre vous, vous voudrez peut-être le signaler dans une déclaration publique ou suggérer les arrière-pensées des auteurs de la campagne si vous en avez connaissance. Toutefois, vous préférez peut-être établir votre propre trajectoire et vous faire aider par des amis et des alliés pour promouvoir et élever votre position, plutôt que d'interagir et de réagir à des contenus diffamatoires.

Pensez-vous avoir été en mesure de contrôler les dommages causés à votre réputation ?

 - [Oui](#resolved_end)
 - [Non](#defamation_end)

### physical-risk_end

> Si vous êtes en danger physique, veuillez contacter les organisations ci-dessous qui peuvent vous aider.

:[](organisations?services=physical_security)

### defamation_end

> Si votre réputation est toujours entachée par une campagne de diffamation, veuillez contacter les organisations ci-dessous qui peuvent vous aider.

:[](organisations?services=advocacy&services=individual_care&services=legal)

### harassment_end

> Si vous avez besoin d'aide pour retirer du contenu, vous pouvez contacter les organisations ci-dessous qui peuvent vous aider.

:[](organisations?services=harassment)

### legal_end

> Si votre réputation est encore entachée par une campagne de diffamation et que vous avez besoin d'un soutien juridique, veuillez contacter les organisations ci-dessous.

:[](organisations?services=legal)

### resolved_end

Nous espérons que ce guide de dépannage vous a été utile. N'hésitez pas à nous faire part de vos commentaires [via email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Pour limiter les conséquences d'une campagne de diffamation, il est important d'adopter de bonnes pratiques de sécurité afin de protéger vos comptes contre les acteurs malveillants et de prendre le pouls des informations qui circulent à votre sujet ou à celui de votre organisation. Vous pouvez envisager certains des conseils préventifs ci-dessous.

- Cartographiez votre présence en ligne. L'autodoxage consiste à explorer les sources ouvertes de renseignements sur soi-même afin d'empêcher les acteurs malveillants de trouver et d'utiliser ces informations pour usurper votre identité.
- Mettez en place des alertes Google. Vous pouvez recevoir des courriels lorsque de nouveaux résultats pour un sujet donné apparaissent dans la recherche Google. Par exemple, vous pouvez obtenir des informations sur les mentions de votre nom ou du nom de votre organisation/collectivité.
- Activez l'authentification à deux facteurs (2FA) pour vos comptes les plus importants. L'authentification à deux facteurs offre une plus grande sécurité en vous obligeant à utiliser plusieurs méthodes pour vous connecter à vos comptes. Cela signifie que même si quelqu'un s'emparait de votre mot de passe principal, il ne pourrait pas accéder à votre compte s'il ne disposait pas également de votre téléphone portable ou d'un autre moyen d'authentification secondaire.
- Vérifiez vos profils sur les plateformes de réseaux sociaux. Certaines plateformes proposent une fonction permettant de vérifier votre identité et de la relier à votre compte.
- Capturez votre page web telle qu'elle apparaît aujourd'hui afin de pouvoir l'utiliser comme preuve à l'avenir. Si votre site web autorise les robots d'indexation, vous pouvez utiliser la machine à remonter le temps proposée par archive.org. Pour cela visitez Internet Archive Wayback Machine, saisissez le nom de votre site web dans le champ situé sous l'en-tête "Save Page Now" et cliquez sur le bouton "Save Page Now".

#### resources

- [Réseaux sociaux et lutte contre le harcèlement: des ressources regroupées par Nothing2Hide](https://wiki.nothing2hide.org/doku.php?id=protectionnumerique:reseauxsociaux)
[Union of Concerned Scientists: How to Counter Disinformation: Communication Strategies, Best Practices, and Pitfalls to Avoid](https://www.ucsusa.org/resources/how-counter-disinformation)
- [Addressing Online Misogyny and Gendered Disinformation: A How-To Guide, by ND](https://www.ndi.org/sites/default/files/Addressing%20Gender%20%26%20Disinformation%202%20%281%29.pdf)
- [Verification Handbook For Disinformation And Media Manipulation](https://datajournalism.com/read/handbook/verification-3)
- [A Guide to Prebunking: A promising way to inoculate against misinformation](https://firstdraftnews.org/articles/a-guide-to-prebunking-a-promising-way-to-inoculate-against-misinformation/)
- [OverZero: Communicating During Contentious Times: Dos and Don’ts to Rise Above the Noise](https://overzero.ghost.io/communicating-during-contentious-times-dos-and-donts-to-rise-above-the-noise/)
- [Visual actor mapping](https://holistic-security.tacticaltech.org/exercises/explore/visual-actor-mapping-part-1.html)
- [Disinformation toolkit](https://www.interaction.org/documents/disinformation-toolkit/)
- [Defusing Hate Workbook](https://www.ushmm.org/m/pdfs/20160229-Defusing-Hate-Workbook-3.pdf)
