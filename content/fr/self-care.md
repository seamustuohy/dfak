---
layout: sidebar.pug
title: "Se sentir surmené ?"
author: FP
language: fr
summary: "Le harcèlement en ligne, les menaces et autres types d'attaques numériques peuvent créer des sentiments d'accablement, de surmenage et des états émotionnels très délicats : vous pouvez ressentir de la culpabilité, honte, anxiété, colère, confusion, impuissance ou même avoir peur pour votre bien-être psychologique ou physique."
date: 2023-04
permalink: /fr/self-care/
parent: Home
sidebar: >
  <h3>Pour en savoir plus sur la façon de vous protéger vous et votre équipe contre le sentiment de surmenage :</h3>

  <ul>
    <li><a href="https://www.newtactics.org/conversation/self-care-activists-sustaining-your-most-valuable-resource">Prendre soin de soi pour les activistes : Maintenir votre ressource la plus précieuse</a></li>
    <li><a href="https://www.amnesty.org.au/activism-self-care/">Prendre soin de vous pour que vous puissiez continuer à défendre les droits de l'homme</a></li>
    <li><a href="https://www.frontlinedefenders.org/fr/resources-wellbeing-stress-management">Ressources pour le bien-être et la gestion du stress</a></li>
    <li><a href="https://iheartmob.org/resources/self_care">Prendre soin de soi pour les personnes victimes de harcèlement</a></li>
    <li><a href="https://cyber-women.com/en/self-care/">Module de formation à l'autogestion de la santé pour les femmes en ligne</a></li>
    <li><a href="https://onlineharassmentfieldmanual.pen.org/fr/prendre-soin-de-soi-en-cas-de-cyberharcelement/">Bien-être et communauté</a></li>
    <li><a href="https://www.patreon.com/posts/12240673">Vingt façons d'aider quelqu'un qui est victime d'intimidation en ligne</a></li>
    <li><a href="https://www.hrresilience.org/">Projet de résilience pour les défenseurs des droits humains</a></li>
    <li><a href="https://apps.learn.totem-project.org/learning/course/course-v1:IWPR+IWPR_AH_EN+001/home">Cours d'apprentissage en ligne du projet Totem : Prendre soin de sa santé mentale (inscription obligatoire)</a></li>
    <li><a href="https://apps.learn.totem-project.org/learning/course/course-v1:IWPR+IWPR_PAP_EN+001/home">Cours d'apprentissage en ligne du projet Totem : Premiers secours psychologiques (inscription obligatoire)</a></li>
    </ul>
    <li><a href="https://holistic-security.org/chapters/prepare/1-2-individual-responses-to-threat.html">Réponses individuelles à la menace</a></li>
    <li><a href="https://holistic-security.org/chapters/prepare/1-4-team-and-peer-responses-to-threat.html">Réponses de l'équipe et des partenaires à la menace</a></li>
    <li><a href="https://holistic-security.org/chapters/prepare/1-5-communicating-about-threats-in-teams-and-organisations.html">Communiquer à propos de menaces au sein des équipes et des organisations</a></li>
  </ul>
---

# Vous vous sentez surmené(e) ?

Le harcèlement en ligne, les menaces et autres types d'attaques numériques peuvent créer des sentiments d'accablement, de surmenage et des états émotionnels très délicats pour vous et votre équipe : vous pouvez ressentir de la culpabilité, honte, anxiété, colère, confusion, impuissance ou même avoir peur pour votre bien-être psychologique ou physique, et ces sentiments peuvent être très différents au sein d'une équipe.

Il n'y a pas de "bonne" façon de se sentir, car votre état de vulnérabilité et ce que vos informations personnelles signifient pour vous est différent d'une personne à l'autre. Toute émotion est justifiée, et vous ne devriez pas vous inquiéter de savoir si votre réaction est la bonne ou non.

La première chose à retenir est que se blâmer soi-même ou blâmer quelqu'un d'autre au sein d'une équipe n'aide pas à réagir aux situations d'urgence. Vous pouvez vous adresser à une personne de confiance qui peut vous aider, vous ou votre équipe, à faire face à une situation d'urgence d'un point de vue technique et émotionnel.

Si vous avez une personne en qui vous avez confiance, vous pouvez lui demander de vous aider à suivre les instructions de ce site web, ou lui donner accès à vos appareils ou à vos comptes pour qu'elle recueille des informations pour vous. Vous pouvez également demander un soutien émotionnel ou technique au cours de ce processus.

Si vous ou votre équipe avez besoin d'un soutien émotionnel pour faire face à une urgence numérique (ou également pour un accompagnement), le [Programme communautaire de santé mentale de Team CommUNITY](https://www.communityhealth.team/) offre des services psychosociaux sous différentes formes pour les cas aigus et à long terme et peut fournir un soutien dans différentes langues et contextes.






