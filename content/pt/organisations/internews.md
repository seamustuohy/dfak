---
name: Internews
website: https://www.internews.org
logo: internews.png
languages: English, Español, Русский, العربية, Tagalog
services: in_person_training, org_security, ddos, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, forensic, censorship
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: 24/7, global
response_time: 12 horas
contact_methods: email, pgp
email: help@openinternetproject.org
pgp_key_fingerprint: 4439 FA33 F79C 2D4A 4CC8 9A4A 2FF2 08B9 BE64 58D0
initial_intake: yes
---

Complementar à sua atuação principal, a Internews também trabalha com pessoas, organizações e comunidades ao redor do mundo para aumentar a conscientização sobre segurança digital, proteção do acesso livre e sem censuras à internet, e, aprimoramento de práticas de segurança digital. A Internews ajudou a treinar jornalistas e pessoas defensoras de direitos humanos em mais de 80 países, possui uma forte rede de facilitação em segurança digital e auditoria com experiência no método [SAFETAG](https://safetag.org), o qual teve seu desenvolvimento liderado pela Internews. A Internews constrói parcerias potentes e responsivas tanto com a sociedade civil como com empresas de inteligência e análise de ameaças do setor privado, apoiando diretamente para que elas mantenham uma presença online segura e sem censura. A Internews oferece intervenções técnicas e não-técnicas utilizando desde os recursos de segurança básicos obtidos com o método SAFETAG, até recursos de política organizacional e estratégias personalizadas de mitigação de danos baseadas em pesquisa de ameaças. Presta suporte direto para a análise de malware e phising voltada para grupos de mídia e de defesa de direitos humanos que vivenciam ataques digitais direcionados.
