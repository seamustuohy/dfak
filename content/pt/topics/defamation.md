﻿﻿---
layout: page
title: "Estou sendo alvo de campanha de difamação"
author: Inés Binder, Florencia Goldsman, Erika Smith, Gus Andrews
language: pt
summary: "O que fazer quando alguém está tentando prejudicar a sua reputação online."
date: 2023-04
permalink: /pt/topics/defamation
parent: Home
---

# Estou sendo alvo de campanha de difamação

Quando alguém cria e dissemina informações falsas, manipuladas ou mal interpretadas para violentar a reputação de outra pessoa, chamamos de campanha de difamação. Essas campanhas podem ser direcionadas à pessoas específicas ou à organizações. Essas campanhas de difamação podem ser orquestradas por qualquer agente, sejam governamentais ou privados.

Esse não é um fenômeno novo. O ambiente digital dissemina essas mensagens de maneira muito rápida, ampliando o alcance e a velocidade dos ataques, causando impactos ainda mais profundos.

As campanhas de difamação têm uma dimensão de gênero quando o objetivo é excluir as mulheres e as pessoas LGBTQIAP+ da esfera pública, divulgando informações que "exploram as desigualdades de gênero, promovem a heteronormatividade e aprofundam os abismos sociais" [[1]](#note-1).

Esta seção do Kit de Primeiros Socorros Digitais te guiará por alguns passos básicos sobre como lidar com campanhas de difamação. Responda o questionário abaixo para identificar a natureza do problema e encontrar possíveis soluções:

<a name="note-1"></a>
[[1] 10 maneiras de enfrentar a desinformação](https://intervozes.org.br/publicacoes/10-maneiras-de-enfrentar-a-desinformacao/) (em português) e
[Combatendo a Desinformação](https://counteringdisinformation.org/node/13/) (em inglês)

## Workflow

### physical-wellbeing

Está temendo pelo seu bem-estar ou segurança física?

- [Sim](#physical-risk_end)
- [Não](#no-physical-risk)

### no-physical-risk

> Uma campanha de difamação tem como objetivo atacar a sua reputação ao questionar, gerar dúvidas, incriminar o alvo, alegar falsidades ou expor contradições para minar a confiança do público.
>
> Esses ataques afetam especialmente pessoas que possuem uma vida pública, cujo trabalho depende do prestígio e da confiabilidade: ativistas e defensores de direitos, agentes da política, jornalistas e pessoas que fornecem dados de interesse público, bem como artistas, entre outros.

Esse ataque está tentando comprometer sua reputação?

 - [Sim](#perpetrators)
 - [Não](#no-reputational-damage)

### no-reputational-damage

> Se estiver enfrentando um ataque que não está direcionado necessariamente à sua reputação, você pode estar diante de uma situação diferente de uma campanha de difamação.

 Deseja percorrer algumas perguntas para podermos diagnosticar se está enfrentando assédio online?

 - [Sim, eu posso estar enfrentando assédio online](../../pt/harassed-online)
 - [Não, ainda acho que pode ser uma campanha de difamação](#perpetrators)
 - [Preciso de ajuda para entender a situação pela qual estou passando](/../pt/support)


### perpetrators

Você deseja saber quem está por trás da campanha de difamação?

 - [Sim](#respond-defamation)
 - [Não](#analyze-messages)

### respond-defamation

> Há diversas maneiras de reagir a uma campanha de difamação, dependendo do impacto e alcance das mensagens, das pessoas envolvidas e das motivações por trás delas. Se a campanha tiver um impacto pequeno e alcance limitado, é recomendável ignorá-la. No entanto, se o impacto for significativo, é importante considerar denunciar e remover o conteúdo, silenciando o discurso difamatório e explicando ao público o que realmente aconteceu.
>
> É crucial ter cuidado para não amplificar as mensagens difamatórias ao tentar combatê-las. Citar uma mensagem, mesmo que seja para expôr as pessoas ou as motivações por trás dela, pode aumentar seu alcance. Considere utilizar a técnica do ["sanduíche da verdade"](https://portalimprensa.com.br/noticias/ultimas_noticias/84402/sanduiche+da+verdade+tecnica+que+ajuda+a+combater+noticias+falsas+ganha+relevancia+no+jornalismo), para esconder informações falsas sem o risco potencializá-las. Essa técnica consiste em apresentar a verdade sobre um assunto antes de revelar as informações falsas e, em seguida, encerrar a história reforçando novamente a verdade.
>
> Escolha as estratégias que parecem mais apropriadas para conter o ataque, reconstruir a confiança e a credibilidade na sua comunidade. Lembre-se! Independente da estratégia escolhida, o [autocuidado](/../pt/self-care/) deve ser uma prioridade. É importante também [documentar](/../pt/documentation/) o conteúdo ou os perfis responsáveis pelos ataques antes de responder.

Como você deseja agir em resposta à campanha de difamação?

 - [Quero notificar a plataforma de rede social e derrubar os conteúdos difamatórios](#notify-take-down)
 - [Quero ignorar e silenciar as notificações.](#ignore-silence)
 - [Quero expor e explicar toda a história.](#set-record-straight)
 - [Quero fazer uma denúncia na justiça.](#legal-complaint)
 - [Preciso analisar as mensagens antes, para só assim tomar uma decisão](#analyze-messages)

### analyze-messages

> Quando você ou sua organização estão sendo alvos de uma campanha de difamação, compreender os métodos básicos de disseminação de desinformação pode te ajudar a avaliar os riscos e orientar os próximos passos. Analisar as mensagens pode fornecer informações adicionais sobre os responsáveis, suas motivações e os recursos que estão utilizando. Técnicas de ataque envolvem diversas ferramentas e coordenação online.
>
> Comece avaliando o nível do risco que está enfrentando. No [Interaction Disinformation Toolkit](https://www.interaction.org/documents/disinformation-toolkit/) (em inglês), você encontrará uma ferramenta de avaliação de risco para auxiliar na estimativa da vulnerabilidade do seu ambiente midiático. [Nesse guia da Artigo 19](https://artigo19.org/wp-content/uploads/2014/11/guia_de_protecao_e_seguranca_para_comunicadores_e_defensores_de_direitos_humanos.pdf) você também encontra orientações sobre como realizar um avaliação de risco.
>
> Uma campanha de desinformação nas redes sociais frequentemente inclui o uso de hashtags para gerar maior engajamento. Mas essas hastags também podem ser útil para monitorar do ataque, permitindo a pesquisa e identificação dos agressores. Para medir o impacto das hashtags nas redes sociais, experimente essas ferramentas:
>
> - [Track my hashtag](https://www.trackmyhashtag.com/) (em inglês)
> - [Brand mentions](https://brandmentions.com/hashtag-tracker) reúne menções no Twitter, Instagram e Facebook em uma mesma pesquisa. (em inglês)
> - [InVid Project](https://www.invid-project.eu/) é uma plataforma de verificação de conhecimentos que detecta histórias que estão crescendo na rede, avaliando inclusive a confiabilidade de vídeos-notícia difundidos nas redes sociais. (em inglês)
> - [Metadata2go](https://www.metadata2go.com/view-metadata ) descobre metadados por trás dos arquivos que você está analisando. (em inglês)
> - [YouTube DataViewer](https://citizenevidence.amnestyusa.org/) pode ser usado para fazer uma pesquisa nos bancos de vídeos da Anistia Internacional para saber se eles são antigos e se já foram adulterados. (em ingleŝ)
>
>**Ataques manuais vs automáticos**
>
> Atores estatais e outres adversáries frequentemente utilizam redes coordenadas de bots, pois são mais acessíveis financeiramente e fáceis de configurar. Reconhecer que mensagens em massa não são originadas por dezenas ou centenas de pessoas, mas sim de bots automatizados, pode ajudar a reduzir a ansiedade e facilitar a documentação, auxiliando na tomada de decisão sobre a melhor forma de reagir e se defender. Utilize o [Botsentinel](https://botsentinel.com/) (em inglês) para verificar a natureza das contas que estão realizando os ataques [(clique aqui para entender o que é um bot sentinel ou bot sentinela](https://www.trackmyhashtag.com/). Algumas mensagens de bots também podem ser coletadas e disseminadas por indivídues, então o serviço oferece pontuações para as contas, verificando se eles possuem seguidores, um avatar personalizado e conteúdo recente, além de outros indicadores que acusam se podem ser bots ou não. Outra ferramenta que você pode usar é a [Pegabot](https://pegabot.com.br/).
>
> **Informações internas vs públicas**
>
> Campanhas de desinformação habilidosas podem basear o conteúdo em aspectos da verdade ou fazer com que pareça verdadeiro. Refletir sobre o tipo de informação que está sendo compartilhada e suas fontes pode determinar diferentes linhas de ação.
>
> As informações são privadas ou de fontes internas? Considere alterar suas senhas e instalar a autenticação de dois fatores, garantindo que só você tenha acesso às suas contas. Se houver indícios de que as informações foram descobertas em grupos ou conversas compartilhadas, incentive amigos e colegas a também atualizar suas definições de segurança. Limpe e encerre conversas antigas para que informações do passado não estejam facilmente acessíveis no dispositivo.
>
> Antagonistas podem sequestrar contas individuais ou de organizações para espalhar [desinformação](https://pt.wikipedia.org/wiki/Desinforma%C3%A7%C3%A3o) e [notícias falsa (fake news)](https://pt.wikipedia.org/wiki/Not%C3%ADcia_falsa), atribuindo uma falsa legitimidade aos discursos. Para retomar o controle sobre as contas, confira no Kit de Primeiros Socorros Digitais, o tópico ["Não consigo acessar minha conta"](../../pt/account-access-issues/questions/what-type-of-account-or-service/).
>
> As informações que estão circulando parecem ter sido retiradas de locais públicos da internet? Veja também o tópico ["Fui vítima de doxxing ou alguém está compartilhando imagens minhas sem permissão](../../pt/doxing), para procurar e identificar onde essas informações foram disponibilizadas, e como reagir.
> <br />
>
> **Criação de Materiais Mais Sofisticados ** (vídeos no YouTube, deepfakes, etc.)
>
> Algumas vezes os ataques podem ser ainda mais direcionados e, para isso, são investidos tempo e esforço para a criação de conteúdo imagético e noticioso em formato de vídeos no YouTube ou [deepfakes](https://pt.wikipedia.org/wiki/Deepfake), para sustentar falsas narrativas. A divulgação de material desse tipo pode revelar seus autores e dar mais motivos para denúncias legais, bem como motivos explícitos para pedidos de remoção de conteúdo. Isso também pode significar que os ataques são altamente direcionados, o que pode aumentar nossos sentimentos de vulnerabilidade. E lembre-se, não só nesse tipo de ataque, mas em todos, [autocuidado](/../pt/self-care/) e segurança pessoal devem ser prioridades.
>
> Outro exemplo é a utilização de domínios falsos em que adversários criam sites ou perfis de redes sociais semelhantes aos originais, o que conhecemos como *fakes*. Se estiver passando por esse tipo de situação, no Kit de Primeiros Socorros Digitais, visite a seção sobre [falsificação de identidade](../../pt/impersonated/)
>
> Esse tipo de ataque pode indicar também que existe uma maior organização, logística, financiamento e investimento de tempo por parte de quem está por trás dele, o que permite uma melhor compreensão das motivações envolvidas.

Tem agora uma melhor ideia de quem está te atacando e como?

 - [Sim, agora posso traçar uma estratégia de resposta](#respond-defamation)
 - [Não, preciso de ajuda para descobrir como responder](#defamation_end)

### notify-take-down

> ***Nota:*** *Considere [documentar os ataques](/../pt/documentation) antes de solicitar que uma plataforma retire o conteúdo difamatório. Se está pensando em entrar com uma ação judicial, consulte informações sobre como reunir [documentação jurídica](/../pt/documentation#legal).*
>
> Os termos de serviço das diferentes plataformas explicam quando é que os pedidos de retirada são considerados legítimos, por exemplo, conteúdos que violem direitos autorais, dos quais o utilizador deve ser o proprietário. Os direitos autorais são concedidos por padrão quando você cria uma obra original: tira uma foto, escreve um texto, compõe uma música, filma um vídeo, etc. Não basta que você apareça na obra, você deve ter criado.

Você tem a propriedade dos conteúdos utilizados na campanha de difamação?

 - [Sim](#copyright)
 - [Não](#report-defamatory-content)

### set-record-straight

> Sua versão dos fatos é sempre importante. Explicitar toda a situação pode ser ainda mais urgente quando a campanha de difamação já está amplamente difundida, prejudicando sua reputação. No entanto, nesses casos você deve avaliar o impacto da campanha para decidir se uma resposta pública é a melhor estratégia.
>
> Para além de denunciar os conteúdos e responsáveis para as plataformas, você pode querer construir uma contra-narrativa, desmistificar a desinformação, denunciar publicamente autores ou simplesmente defender a verdade. A sua reação dependerá também da comunidade de apoio de que dispõe para te ajudar, e da sua própria avaliação do risco que poderá correr ao tomar uma posição pública.

Você possui uma equipe de pessoas que podem te dar apoio?

 - [Sim](#campaign-team)
 - [Não](#self-campaign)

### legal-complaint

> Ao enfrentar um processo judicial e recolher evidências para apresentá-la em um tribunal, é crucial seguir um protocolo específico para que este material seja aceito pela justiça. Quando se trata de apresentar provas de um ataque online, não basta fazer uma captura de tela. Para saber mais sobre esse processo, antes de acionar advogades, consulte a seção sobre [como documentar um ataque para utilizar em um processo judicial](/../../documentation#legal), no Kit de Primeiros Socorros Digitais para aprender mais sobre como proceder nesse tipo de situação.


O que você gostaria de fazer?

 - [As recomendações foram úteis. Já sei o que fazer agora.](#resolved_end)
 - [As recomendações foram úteis, mas gostaria de considerar outras estratégias.](#respond-defamation)
 - [Preciso de apoio para seguir com minha denúncia.](#legal_end)

### ignore-silence

> Em algumas situações, especialmente em casos de uma campanha de difamação de baixo alcance e impacto, pode ser preferível apenas ignorá-la ou silenciar as mensagens. Essa abordagem é importante para o autocuidado. Nesse caso, você pode solicitar a pessoas de confiança que monitorem as mensagens em seu lugar e te alertem, caso necessário. É importante ressaltar que ssa medida não elimina as mensagens, então você pode documentar e analisar o conteúdo posteriormente.

Onde o conteúdo difamatório que você deseja silenciar está sendo difundido?

 - [Email](#silence-email)
 - [Facebook](#silence-facebook)
 - [Instagram](#silence-instagram)
 - [Reddit](#silence-reddit)
 - [TikTok](#silence-tiktok)
 - [Twitter](#silence-twitter)
 - [Whatsapp](#silence-whatsapp)
 - [YouTube](#silence-youtube)

### copyright

> Você pode utilizar mecanismos de direitos autorais para solicitar a remoção de conteúdos difamatórios. ***Busque compreender as políticas de direitos autorais do seu país*** ou busque o apoio de especialistas. A maioria das redes sociais oferece formulários específicos para denúncias de violação de direitos autorais ,e a resposta costuma ser mais rápida devido às implicações legais envolvidas.
>
> ***Note:*** *Sempre [documente](/../pt/documentation) antes de solicitar a retirada do conteúdo. Se você estiver cosiderando entrar com uma ação na justiça, é importante consultar antes as orientações sobre como reunir [documentação legal](/../../documentation#legal).*
>
> Você pode também utilizar esses links para enviar solicitações de remoção de conteúdo por violação de direitos autorais para as principais plataformas de redes sociais:
>
> - [Archive.org](https://help.archive.org/help/how-do-i-request-to-remove-something-from-archive-org/) (em inglês)
> - [Facebook](https://www.facebook.com/help/1020633957973118/?helpref=hc_fnav)
> - [Instagram](https://help.instagram.com/contact/552695131608132)
> - [TikTok](https://www.tiktok.com/legal/report/Copyright)
> - [Twitter](https://help.twitter.com/pt/forms/ipi)
> - [YouTube](https://support.google.com/youtube/answer/2807622?sjid=1561215955637134274-SA)
>
> Lembre-se de que a resposta para esses pedidos pode demorar. Salve essa página nos "favoritos" do seu navegador para retornar com facilidade sempre que precisar.

O conteúdo foi derrubado?

 - [Sim](#resolved_end)
 - [Não, preciso de apoio legal](#legal_end)
 - [No, preciso de apoio para entrar em contato com a plataforma](#harassment_end)

### report-defamatory-content

> Nem todas as plataformas têm serviços específicos para receber denúncias de conteúdo difamatório, e algumas exigem que já exista um processo legal em andamento. Você pode revisar o caminho que cada plataforma estabelece para esse tipo de denúncia.
>
> - [Archive.org](https://help.archive.org/help/how-do-i-request-to-remove-something-from-archive-org/) (em inglês)
> - [Facebook](https://www.facebook.com/help/contact/430253071144967)
> - [Instagram](https://help.instagram.com/contact/653100351788502)
> - [TikTok](https://www.tiktok.com/legal/report/feedback)
> - [Twitch](https://help.twitch.tv/s/article/how-to-file-a-user-report?language=pt_BR)
> - [Twitter](https://help.twitter.com/pt/rules-and-policies/twitter-report-violation#directly)
> - [Whatsapp](https://faq.whatsapp.com/1142481766359885?helpref=search&cms_platform=android)
> - [YouTube](https://support.google.com/youtube/answer/6154230)
>
> Lembre-se de que a resposta para esses pedidos pode demorar. Salve essa página nos seus favoritos para retornar sempre que precisar.

O conteúdo foi denunciado com sucesso e analisado para ser retirado?

 - [Sim](#resolved_end)
 - [Não, preciso de apoio legal.](#legal_end)
 - [Não, preciso de apoio para entrar em contato com a plataforma.](#harassment_end)

### silence-email

> A maioria dos serviços de e-mail oferece a opção de filtrar mensagens e marcá-las automaticamente como spam, arquivá-las ou deletá-las, para que não apareçam na sua caixa de entrada. Normalmente é possível criar regras para essa filtragem por remetente, destinatário, data, tamanho, anexo, assunto e palavras-chave.
>
> Aprenda a criar filtros no seu webmail:
>
> - [Gmail](https://support.google.com/mail/answer/6579?hl=pt-BR&sjid=14833816887872351285-SA#zippy=%2Ccreate-a-filter)
> - [Protonmail](https://proton.me/pt-br/support/email-inbox-filters)
> - [Yahoo](https://help.yahoo.com/kb/SLN28071.html) (em inglês)
>
> Aprenda a criar filtros nos clientes de e-mail:
>
> - [MacOS Mail](https://support.apple.com/guide/mail/filter-emails-mlhl1f6cf15a/mac)
> - [Microsoft Outlook](https://support.microsoft.com/pt-br/office/configurar-regras-no-outlook-75ab719a-2ce8-49a7-a214-6d62b67cbd41)
> - [Thunderbird](https://support.mozilla.org/pt-BR/kb/organize-suas-mensagens-usando-filtros)

Deseja silenciar conteúdos difamatórios em outra plataforma?

 - [Sim](#ignore-silence)
 - [Não](#damage-control)

### silence-twitter

> Se não quiser se expor a mensagens específicas, bloqueie e/ou silencie usuáries e mensagens. Lembre-se que se optar por bloquear alguém ou algum tipo de conteúdo, você não terá acesso ao conteúdo para [documentar o ataque](/../pt/documentation). Nesse caso, você pode acessar o conteúdo através de outra conta ou apenas silenciá-lo.
>
> - [Silencie contas para que não apareçam na sua linha do tempo](https://help.twitter.com/pt/using-twitter/twitter-mute)
> - [Silencie palavras, frases, nomes de usuário, emojis, hashtags e notificações](https://help.twitter.com/pt/using-twitter/advanced-twitter-mute-options)
> - [Suspenda definitivamente notificações de conversas](https://help.twitter.com/pt/using-twitter/direct-messages)

Deseja silenciar conteúdos difamatório em outra plataforma?

 - [Sim](#ignore-silence)
 - [Não](#damage-control)

### silence-facebook

> Mesmo que o Facebook não permita silenciar usuáries ou conteúdos, permite bloquear perfis e páginas.
>
> - [Bloqueie um perfil no Facebook ](https://www.facebook.com/help/)
> - [Bloqueie mensagens de um perfil no Facebook ](https://www.facebook.com/help/1682395428676916)
> - [Bloqueie uma página no Facebook](https://www.facebook.com/help/395837230605798)
> - [Banir ou bloquear perfis em sua página no seu Facebook](https://www.facebook.com/help/185897171460026/)

Deseja silenciar conteúdo difamatório em outra plataforma?

 - [Sim](#ignore-silence)
 - [Não](#damage-control)

### silence-instagram

> Veja uma lista de dicas e ferramentas para silenciar usuáries e conversas no Instagram:
>
> - [Silenciar alguém no Instagram](https://help.instagram.com/469042960409432)
> - [Silenciar os stories de alguém no Instagram ](https://help.instagram.com/290238234687437)
> - [Restringir o conteúdo de alguém no Instagram ](https://help.instagram.com/2638385956221960)
> - [Desligar as sugestões para o seu perfil no Instagram ](https://help.instagram.com/530450580417848)
> - [Esconder postagens sugeridas na sua timeline](https://help.instagram.com/423267105807548)
> - [Desligar os pedidos para seguir seu perfil no Instagram](https://help.instagram.com/530450580417848)

Deseja silenciar conteúdo difamatório em outra plataforma?

 - [Sim](#ignore-silence)
 - [Não](#damage-control)

### silence-youtube

> Se estiver acessando o YouTube a partir do navegador do seu computador, você pode deletar vídeos, canais, seções e listas de reprodução da sua página inicial.
>
> - [Gerencie suas recomendações e resultados de pesquisa](https://support.google.com/youtube/answer/6342839)
>
> Você pode também criar uma lista de bloqueio de espectadores para o chat de suas transmissões ao vivo.
>
> - YouTube: [myaccount.google.com/blocklist](https://myaccount.google.com/blocklist)
> - [Bloqueie espectadores no chat ao-vivo do YouTube.](https://support.google.com/youtube/answer/7663906)
>
Deseja silenciar conteúdo difamatório em outra plataforma?

 - [Sim](#ignore-silence)
 - [Não](#damage-control)

### silence-whatsapp

> O WhatsApp é uma plataforma muito utilizada para campanhas de difamação. As mensagens se espalham rapidamente devido às relações de confiança entre quem envia e quem recebe as mensagens.
>
> - [Arquive uma conversa ou grupo](https://faq.whatsapp.com/1426887324388733)
> - [Sair e excluir grupos](https://faq.whatsapp.com/498814665492149)
> - [Bloquear um contato](https://faq.whatsapp.com/1142481766359885)
> - [Silenciar notificações de grupos](https://faq.whatsapp.com/694350718331007)

Deseja silenciar conteúdo difamatório em outra plataforma?

 - [Sim](#ignore-silence)
 - [Não](#damage-control)

### silence-tiktok

> O TikTok permite bloquear usuáries, impedir que outros usuáries comentem em seus vídeos, criar filtros para os comentários e excluir, silenciar e filtrar mensagens diretas.
>
> - [Bloqueando usuáries no TikTok](https://support.tiktok.com/pt_BR/using-tiktok/followers-and-following/blocking-the-users)
> - [Escolher quem pode comentar em seus vídeos no TikTok](https://support.tiktok.com/pt_BR/using-tiktok/messaging-and-notifications/comments)
> - [Ativar filtros de comentários no TikTok](https://support.tiktok.com/pt_BR/using-tiktok/messaging-and-notifications/comments#3)
> - [Administrar definições de privacidade de comentários para todos vídeos no TikTok](https://support.tiktok.com/pt_BR/using-tiktok/messaging-and-notifications/comments#4)
> - [Administrar definições de privacidade de comentários para um único vídeo no TikTok](https://support.tiktok.com/pt_BR/using-tiktok/messaging-and-notifications/comments#5)
> - [Como excluir, silenciar e filtrar mensagens diretas no TikTok](https://support.tiktok.com/pt_BR/account-and-privacy/account-privacy-settings/direct-message#6)
> - [Como definir quem pode te enviar mensagens diretas](https://support.tiktok.com/pt_BR/account-and-privacy/account-privacy-settings/direct-message#6)

Deseja silenciar conteúdo difamatório em outra plataforma?

 - [Sim](#ignore-silence)
 - [Não](#damage-control)

### silence-reddit

> Se a campanha de difamação está circulando no Reddit, você pode fazer com que as comunidades em que isso está acontecendo deixem de aparecer no seu feed da página inicial e no feed de populares. Se você for moderadore, também pode banir ou silenciar usuáries que violem repetidamente as regras da sua comunidade.
>
> - [Silenciando comunidades no Reddit](https://support.reddithelp.com/hc/pt-pt/articles/9810475384084-O-que-%C3%A9-o-silenciamento-de-comunidades-)
> - [Banindo e silenciando membres da sua comunidade](https://mods.reddithelp.com/hc/pt-br/articles/360009161872-Gest%C3%A3o-de-usu%C3%A1rios-como-banir-e-silenciar)
>

Deseja silenciar conteúdo difamatório em outra plataforma?

 - [Sim](#ignore-silence)
 - [Não](#damage-control)


### campaign-team

> Se a campanha contra você envolve investimento de tempo e recursos, isso sugere que responsáveis têm motivações mais profundas. Nesse caso, você pode precisar utilizar todas as estratégias aqui mencionadas, para mitigar os danos e construir uma campanha pública em sua defesa. Amigues e aliades podem apoiar seu autocuidado, auxiliando nas análises das mensagens, na documentação dos ataques e nas denúncias de conteúdo e perfis.
>
> Uma equipe de campanha pode trabalhar em conjunto para determinar as melhores maneiras de contestar as narrativas difamatórias.
As mensagens de desinformação são traiçoeiras, e é importante ter cuidado ao lidar com mensagens de desinformação, evitando replicar seu conteúdo. É recomendado encontrar um equilíbrio entre promover sua própria história e refutar alegações falsas, sem impulsionar a narrativa difamatória.
>
> É fundamental que seja feita uma [análise de risco](https://www.interaction.org/documents/disinformation-toolkit/) (em inglês) para você e para qualquer pessoa e organização que esteja te apoiando antes de embarcar em uma campanha pública. [Aqui](https://escoladeativismo.org.br/project/caderno-de-planejamento-e-estrategia/) você também encontra orientações para realizar uma análise de risco.
>
> Além de analisar as mensagens que te atacam, você e sua equipe podem começar [mapeando organizações e aliades](https://escoladeativismo.org.br/project/caderno-de-planejamento-e-estrategia/) que reconheçam seu passado e que podem contribuir para uma narrativa positiva sobre seu trabalho, ou que façam barulho suficiente para desviar ou abafar a campanha de difamação.
>
> Você e sua organização podem não ser os únicos que estão sendo atacades. Pense em criar confiança com outras organizações e comunidades que tenham sido discriminadas ou visadas pela campanha de desinformação. Isso significa preparar as pessoas da comunidade para lidar com mensagens controversas ou compartilhar recursos para que possam se defender.
>
> Sua equipe deve dedicar tempo para identificar que tipo de sentimento o conteúdo que estão criando vai provocar, pensando que para refutar a difamação, o discurso deve ser positivo e não se referir propriamente às táticas de difamação. Seu intuito deve ser que as pessoas se tornem mais hábeis ao diferenciar boatos de fatos. Mensagens simples, baseadas em valores, são bastante eficazes em qualquer campanha pública.
>
> Mais do que emitir declarações públicas ou à imprensa, selecionar e preparar diferentes porta-vozes para sua campanha é um diferencial. Isso também ajuda a desviar os ataques personalizados comuns nas campanhas de difamação, especialmente em casos de desinformação de gênero.
>
> Estabelecer um sistema de monitoramento de mídia, para acompanhar tanto as mensagens da sua campanha quanto da campanha adversária, vai te ajudar a comparar os resultados de cada uma para que possa refinar o trabalho.
>
> Se o conteúdo difamatório também for publicado em jornais ou revistas, sua equipe pode reivindicar o [direito de resposta](#self-strategies) ou ativar o seu caso através de sites de [checagem de fatos](#self-strategies).

Acha que conseguiu controlar os danos a sua reputação?

 - [Sim](#resolved_end)
 - [Não](#defamation_end)

### self-campaign

> Se não tiver o apoio de uma equipe, você deve levar em consideração qualquer limitação de capacidade e recursos ao planejar seus próximos passos, sempre priorizando seu [autocuidado](/../pt/self-care). Você poderá publicar uma declaração em seus perfis nas redes sociais ou no seu site. Antes disso, certifique-se de que tem o [controle total dessas contas](../../pt/account-access-issues). Em sua declaração, você pode mencionar pessoas e fontes confiáveis para corroborar sua narrativa, estabelecendo sua trajetória. A sua [análise da campanha de difamação](#analyze-messages) fornecerá informações sobre o tipo de declaração que pretende fazer. Por exemplo, pode verificar se alguma fonte de notícias está sendo referida no ataque.

O conteúdo difamatório também foi publicado em jornais ou revistas?

 - [Sim](#self-strategies)
 - [Não](#damage-control)

### self-strategies

> Se a informação difamatória está publicada, a política editorial ou mesmo a legislação do seu país pode lhe conceder o [direito de resposta, ou o direito de correção](https://www.jusbrasil.com.br/artigos/direito-de-resposta-como-funciona/366454662). Isto garante a você mais uma plataforma onde publicar a sua declaração, para além dos seus próprios meios de comunicação, e ainda te proporciona um local para linkar outras declarações que você estiver fazendo.
>
> Se os meios de comunicação que contém as informações difamatórias não forem fontes de notícias de credibilidade ou forem frequentemente uma fonte de notícias infundadas, pode ser útil assinalar esse fato em qualquer declaração.
>
> Existem muitas ferramentas e organizações que trabalham com checagem de fatos em nível local, regional e mundial que podem te ajudar a desmentir as informações que circulam sobre você.
>
> - [Aos Fatos](https://www.aosfatos.org/)
>  - [Lupa ](https://lupa.uol.com.br/jornalismo/)
>  - [Checazap](https://enoisconteudo.com.br/checazap)
> - [FakeCheck](http://nilc-fakenews.herokuapp.com/)
>  - [Uol Confere](https://noticias.uol.com.br/confere/)
> - [Boatos.org](https://www.boatos.org/)
> - [Fato ou Fake](https://g1.globo.com/fato-ou-fake/)
> - [Latam Chequea](https://chequeado.com/latamchequea)

Sente que precisa de uma resposta maior à campanha de difamação?

 - [Sim](#campaign-team)
 - [Não](#damage-control)

### damage-control

> A análise da campanha de difamação fornecerá informações valiosas para determinar as ações adequadas a serem tomadas. Por exemplo, se for identificado o uso de robôs na campanha difamatória, pode ser relevante destacar esse fato em uma declaração pública, ou sugerir possíveis segundas intenções por parte dos responsáveis, se houver informações a respeito.
No entanto, estabelecer a sua própria trajetória e obter ajuda de amigues e aliades para promover sua verdade pode ser uma prioridade, em vez de interagir e reagir a conteúdos difamatórios.

Acha que conseguiu controlar os danos causados à sua reputação?

 - [Sim](#resolved_end)
 - [Não](#defamation_end)

### physical-risk_end

> Se estiver sob risco físico, acione as organizações abaixo indicadas, elas podem te apoiar.

:[](organisations?services=physical_security)

### defamation_end

> Se ainda estiver sofrendo danos em sua reputação devido a uma campanha de difamação, você pode contactar as seguintes organizações.

:[](organisations?services=advocacy&services=individual_care&services=legal)

### harassment_end

> Se precisar de apoio para remover conteúdos, pode contactar as organizações abaixo indicadas que podem te ajudar.

:[](organisations?services=harassment)

### legal_end

> Se ainda estiver sofrendo danos a sua reputação devido a uma campanha de difamação e precisar de apoio jurídico, contacte as organizações abaixo.

:[](organisations?services=legal)

### resolved_end

Esperamos que este guia de resolução de problemas tenha sido útil. Nos envie seus comentários por e-mail. [via email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Uma forma importante de controlar os danos de uma campanha de difamação é ter boas práticas de segurança para manter as suas contas a salvo de agentes maliciosos e tomar as rédeas das informações que circulam sobre si ou sobre a sua organização. Veja só as dicas abaixo:

- Mapeie sua presença online. O self-doxing consiste em explorar informações públicas sobre você para evitar que agentes maliciosos descubram e utilizem essas informações se passando você.
- Ative os Alertas do Google. Você pode receber e-mails quando aparecerem novos resultados para um tópico na Pesquisa Google. Por exemplo, pode obter informações sobre as menções ao seu nome ou ao nome da sua organização/coletiva.
- Ative a autenticação de dois factores (2FA) para as suas contas mais importantes. A 2FA oferece uma maior segurança para as contas, exigindo a utilização de mais do que um método para logins.
- Isto significa que, mesmo se alguém conseguir descobrir sua senha, não conseguiria acessar sua conta, a menos que tenha em mãos também o seu dispositivo ou outro meio secundário de autenticação.
- Verifique seus perfis nas plataformas de redes sociais. Algumas plataformas oferecem uma funcionalidade para verificar sua identidade e associá-la à sua conta.
- Capture a sua página web tal como aparece agora para utilizar como prova no futuro. Se o seu site permitir rastreadores, pode utilizar a Wayback Machine, oferecida pelo site [archive.org](https://archive.org/) (em inglês).
- Visite o [Internet Archive Wayback Machine](https://pt.wikipedia.org/wiki/Wayback_Machine), introduza o nome do seu sítio Web no campo por baixo do cabeçalho "Guardar página agora" e clique no botão "Guardar página agora"...

#### resources

- [Desinformação e direitos humanos – Contribuição do Intervozes para a ONU](https://intervozes.org.br/publicacoes/desinformacao-direitos-humanos-contribuicao-onu/)
- [Pesquisa do Intervozes analisa medidas de combate à desinformação adotadas por plataformas digitais](https://intervozes.org.br/nova-pesquisa-do-intervozes-analisa-medidas-de-combate-a-desinformacao-adotadas-por-plataformas-digitais/)
- [Union of Concerned Scientists: Como conbater Desinformação: Estratégias de comunicação, boas práticas práticas e armadilhas para evitar](https://www.ucsusa.org/resources/how-counter-disinformation) (em inglês)
- [Abordando Misoginia e Desinformação de Gênero: um guia, by ND](https://www.ndi.org/sites/default/files/Addressing%20Gender%20%26%20Disinformation%202%20%281%29.pdf) (em inglês)
- [Manual de Verificação de Desinformação e Manipulação nas Mídias](https://datajournalism.com/read/handbook/verification-3) (em inglês)
- [Um guia para o Prebunking: Uma forma promissora de se proteger contra a desinformação](https://firstdraftnews.org/articles/a-guide-to-prebunking-a-promising-way-to-inoculate-against-misinformation/) (em inglês)
- [OverZero: Comunicar em tempos contenciosos: O que fazer e o que não fazer para superar o barulho](https://overzero.ghost.io/communicating-during-contentious-times-dos-and-donts-to-rise-above-the-noise/) (em inglês)
- [Caderno de planejamento e estratégia](https://escoladeativismo.org.br/project/caderno-de-planejamento-e-estrategia/)
- [Mapa de atores](https://holistic-security.tacticaltech.org/exercises/explore/visual-actor-mapping-part-1.html) (em inglês)
- [Kit de Ferramentas contra Desinformação](https://www.interaction.org/documents/disinformation-toolkit/) (em inglês)
- [Manual de trabalho para desarmar o ódio](https://www.ushmm.org/m/pdfs/20160229-Defusing-Hate-Workbook-3.pdf) (em inglês)
