---
name: Qurium Media Foundation
website: https://www.qurium.org
logo: qm_logo.png
languages: English, Español, Français, Русский
services: web_hosting, web_protection, assessment, vulnerabilities_malware, ddos, triage, censorship, forensic
beneficiaries: journalists, media, hrds, activists, lgbti, cso
hours: ۸ صبح تا ۱۸ بعد از ظهر دوشنبه تا یکشنبه CET
response_time: ۴ ساعت
contact_methods: email, pgp, web_form
web_form: https://www.qurium.org/contact/
email: info@virtualroad.org
pgp_key: https://www.virtualroad.org/keys/info.asc
pgp_key_fingerprint: 02BF 7460 09F9 40C5 D10E B471 ED14 B4D7 CBC3 9CF3
initial_intake: yes
---

بنیاد رسانه‌ای کوریوم یک ارائه‌دهنده راه‌حل‌های امنیتی برای رسانه‌های مستقل، سازمان‌های حقوق بشر، روزنامه‌نگاران تحقیقی و فعالان است. کوریوم مجموعه ای از راه حل های حرفه ای، سفارشی و ایمن را با پشتیبانی شخصی برای سازمان ها و افراد در معرض خطر ارائه می دهد که شامل:

- میزبانی امن با کاهش DDoS وب سایت های در معرض خطر
- پشتیبانی واکنش سریع به سازمان ها و افراد تحت تهدید فوری
- ممیزی امنیتی خدمات وب و برنامه های تلفن همراه
- دور زدن وب سایت های مسدود شده اینترنتی
- تحقیقات پزشکی قانونی در مورد حملات دیجیتال، برنامه های تقلبی، بدافزارهای هدفمند و اطلاعات نادرست

