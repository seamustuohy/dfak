﻿---
name: Nothing2Hide
website: https://nothing2hide.org
logo: nothing2hide.png
languages: Français, English
services: in_person_training, org_security, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, forensic, individual_care, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso, land
hours: دوشنبه تا جمعه، ۹ صبح تا ۶ بعد از ظهر CET
response_time: یک روز
contact_methods: email, pgp, signal, web_form, wire
web_form: https://vault.tech4press.org/#/ (online secured form - based on Globaleaks)
email: help@tech4press.org
pgp_key:  http://tech4press.org/fr/contact-mail/
pgp_key_fingerprint: 2DEB 9957 8F91 AE85 9915 DE94 1B9E 0F13 4D46 224B
signal: +33 7 81 37 80 08 <http://tech4press.org/fr/#signal>
wire: tech4press
---

Nothing2Hide (N2H) انجمنی است که با ارائه راه‌حل‌های فنی و آموزش متناسب با هر زمینه‌ای، به خبرنگاران، وکلا، فعالان حقوق بشر و شهروندان «عادی» ابزاری برای محافظت از داده‌ها و ارتباطات خود ارائه می‌کند. چشم انداز ما این است که فناوری را در خدمت انتشار و حفاظت اطلاعات قرار دهیم تا دموکراسی‌ها را در سراسر جهان تقویت کنیم.


