﻿---
name: Digital Defenders Partnership
website: https://www.digitaldefenders.org
logo: DDP_logo_zwart_RGB.png
languages: Español, English, Français, Português, Deutsch, Nederlands, Русский
services: grants_funding, in_person_training, org_security, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: دوشنبه تا پنجشنبه ۹ صبح تا ۵ بعد از ظهر CET
response_time: چهار روز
contact_methods: email, phone, mail
email: team@digitaldefenders.org
mail: Raamweg 16, 2596 HL Den Haag
phone: +31 070 376 5500
---

مشارکت Digital Defenders از مدافعان حقوق بشر تحت تهدید دیجیتال پشتیبانی می‌کند و برای تقویت شبکه‌های واکنش سریع محلی کار می‌کند. DDP پشتیبانی اضطراری را برای افراد و سازمان‌هایی مانند مدافعان حقوق بشر، روزنامه نگاران، فعالان جامعه مدنی و وبلاگ نویسان هماهنگ می‌کند.

DDP دارای پنج نوع مختلف بودجه است که به موقعیت‌های اضطراری فوری می‌پردازد، و همچنین کمک های مالی بلندمدت متمرکز بر ایجاد ظرفیت در یک سازمان. علاوه بر این، آنها یک کمک هزینه یکپارچگی دیجیتال را هماهنگ می‌کنند که در آن سازمان‌ها آموزش‌های شخصی‌شده امنیت دیجیتال و حریم خصوصی و یک برنامه شبکه واکنش سریع را دریافت می‌کنند.



