﻿---
name: Media Diversity Institute - Armenia
website: https://mdi.am/
logo: MDI_Armenia_Logo.png
languages: հայերեն, Русский, English
services: in_person_training, org_security, web_hosting, web_protection, digital_support, triage, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, censorship, physical_sec
beneficiaries: journalists, hrds, activists, lgbti, women, cso
hours: بیست و چهار ساعته, GMT+4
response_time: سه ساعت
contact_methods: email, pgp, phone, whatsapp, signal, telegram
email: help@mdi.am
pgp_key_fingerprint: D63C EC9C D3AD 51BE EFCB  683F F2B8 6042 F9D9 23A8
phone: "+37494938363"
whatsapp: "+37494938363"
signal: "+37494938363"
telegram: "+37494938363"
initial_intake: yes
---

موسسه تنوع رسانه‌ها - ارمنستان یک سازمان غیر انتفاعی و غیر دولتی است که به دنبال استفاده از قدرت رسانه‌های سنتی، رسانه‌های اجتماعی و فناوری‌های جدید برای محافظت از حقوق بشر، کمک به ایجاد یک جامعه دموکراتیک و مدنی، رساندن صدای افراد بی‌صدا و تعمیق درک جمعی از انواع مختلف تنوع اجتماعی.

MDI ارمنستان وابسته به موسسه تنوع رسانه ای مستقر در لندن است، اما یک نهاد مستقل است.


