---
layout: page
title: پست
author: mfc
language: fa
summary: روش‌های تماس
date: 2018-09
permalink: /fa/contact-methods/postal-mail.md
parent: /fa/
published: true
---

ارسال نامه یک روش ارتباطی کند است اگر با یک موقعیت فوری روبرو هستید. بسته به حوزه‌های قضایی که نامه در آن سفر می‌کند، مقامات ممکن است نامه را باز کنند و اغلب فرستنده، مکان ارسال، گیرنده و مکان مقصد را ردیابی می‌کنند.
