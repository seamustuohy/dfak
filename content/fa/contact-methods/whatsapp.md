---
layout: page
title: واتس‌اپ
author: mfc
language: fa
summary: روش‌های تماس
permalink: /fa/contact-methods/whatsapp.md
parent: /fa/
published: true
---

استفاده از واتس‌اپ تضمین می‌کند که مکالمه شما با گیرنده محافظت می‌شود به طوری که فقط شما و گیرنده می‌توانید ارتباطات را بخوانید، با این حال این واقعیت که شما با گیرنده ارتباط برقرار کرده‌اید ممکن است توسط دولت‌ها یا سازمان‌های مجری قانون قابل دسترسی باشد.

منابع: [نحوه: استفاده از WhatsApp در Android](https://ssd.eff.org/en/module/how-use-whatsapp-android)[نحوه: استفاده از WhatsApp در iOS](https://ssd. eff.org/en/module/how-use-whatsapp-ios).
