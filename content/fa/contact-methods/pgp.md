---
layout: page
title: PGP
author: mfc
language: fa
summary: روش‌های تماس
date: 2020-11
permalink: /fa/contact-methods/pgp.md
parent: /fa/
published: true
---

PGP (یا Pretty Good Privacy) و معادل متن باز آن، GPG (Gnu Privacy Guard)، به شما امکان می‌دهد محتوای ایمیل‌ها را رمزگذاری کنید تا پیام خود را از مشاهده توسط ارائه دهنده ایمیل خود یا هر طرف دیگری که ممکن است به ایمیل دسترسی داشته باشد محافظت کنید. اما این واقعیت که شما پیامی را به سازمان گیرنده ارسال کرده‌اید ممکن است توسط دولت‌ها یا سازمان‌های مجری قانون قابل دسترسی باشد. برای جلوگیری از این امر، می‌توانید یک آدرس ایمیل جایگزین ایجاد کنید که با هویت شما مرتبط نیست.

منابع: [Access Now Helpline Community Documentation: Secure Email](https://communitydocs.accessnow.org/253-Secure_Email_Recommendations.html)

[Freedom of the Press Foundation: Encrypting Email with Mailvelope: A Beginner's Guide](https://freedom.press/training/encrypting-email-mailvelope-guide/)

[Privacy Tools: Private Email Providers](https://www.privacytools.io/providers/email/)
