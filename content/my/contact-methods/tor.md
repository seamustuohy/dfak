---
layout: page
title: Tor
author: mfc
language: my
summary: ဆက်သွယ်ရန် နည်းလမ်းများ
date: 2021-03
permalink: /my/contact-methods/tor.md
parent: /my/
published: true
---

Tor ဘရောင်ဇာသည် သင့်အမည်နှင့် တည်နေရာကို (သင့် IP လိပ်စာမှ တစ်ဆင့်) မထုတ်ဖေါ်ပဲ ဝဘ်ဆိုဒ်များကို အသုံးပြုနိုင်သော ပုဂ္ဂိုလ်ရေးအလေးထားသော ဝဘ်ဘရောင်ဇာ တစ်ခုဖြစ်ပါသည်။

လေ့လာရန် - [Tor အကြောင်းအရာ](https://www.torproject.org/about/overview.html.en).
