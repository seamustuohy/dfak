---
name: Access Now
website: https://www.accessnow.org/help
logo: accessnow.png
languages: English, Español, Français, Deutsch, Português, Русский, العربية, Tagalog, Italiano, Українська, тоҷикӣ
services: grants_funding, in_person_training, org_security, web_protection, digital_support, assessment, secure_comms, device_security, vulnerabilities_malware, account, harassment, forensic, advocacy
beneficiaries: journalists, hrds, activists, lgbti, women, youth, cso
hours: "၂၄/၇, ကမ္ဘာတစ်ဝှမ်း"
response_time: "၂ နာရီ"
contact_methods: email, pgp
email: help@accessnow.org
pgp_key: https://keys.accessnow.org/help.asc
pgp_key_fingerprint: 6CE6 221C 98EC F399 A04C 41B8 C46B ED33 32E8 A2BC
initial_intake: yes
---

Access Now ရဲ့ ဒစ်ဂျစ်တယ်ဘေးကင်းလုံခြုံရေး helpline သည် ကမ္ဘာတစ်ဝှမ်းရှိ လူပုဂ္ဂိုလ်များနှင့် အဖွဲ့အစည်းများ အင်တာနက်ပေါ်တွင်လုံခြုံစေရန် ကြိုးပမ်းလျက်ရှိပါသည်။ လိုင်းပေါ်တွင် သင်အန္တရာယ်များကြုံတွေ့နေပါက သင်၏ဒစ်ဂျစ်တယ်ဘေးကင်းလုံခြုံရေးဆိုင်ရာ အလေ့အကျင့်တွေပိုမိုခိုင်မာတိုးတက်ရန်အတွက် ကျွန်ုပ်တို့ဘက်က ကူညီပေးနိုင်ပါသည်။ သို့မဟုတ် သင်သည်မျက်မှောက်တွင် ဒစ်ဂျစ်တယ်တိုက်ခိုက်ရေးများခံစားနေရပါက ကျွန်ုပ်တို့ဘက်က အမြန်ဆုံးအရေးပေါ်အထောက်အကူပေးနိုင်ပါသည်။
