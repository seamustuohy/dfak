---
name: Digital Defenders Partnership
website: https://www.digitaldefenders.org
logo: DDP_logo_zwart_RGB.png
languages: Español, English, Français, Português, Deutsch, Nederlands, Русский
services: grants_funding, in_person_training, org_security, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso
hours: понеділок-четвер, 9-17 CET
response_time: 4 дні
contact_methods: email, phone, mail
email: team@digitaldefenders.org
mail: Raamweg 16, 2596 HL Den Haag
phone: +31 070 376 5500
---

Digital Defenders Partnership пропонує правозахисникам підтримку в ситуаціях, коли виникають цифрові загрози, і працює над зміцненням місцевих мереж швидкого реагування. DDP координує оперативну підтримку для окремих осіб та організацій, зокрема, правозахисників, журналістів, громадських активістів та блогерів.

DDP має п'ять різних видів фінансування для вирішення нагальних ситуацій, а також довгострокові гранти, призначені для підтримки внутрішніх ресурсів організації. Крім того, DDP координує програму Digital Integrity Fellowship, за якою організації отримують персоналізовані тренінги з питань цифрової безпеки та приватності, а також програму Мережі швидкого реагування (Rapid Response Network).
