---
name: Digital Rights Foundation (DRF)
website: https://digitalrightsfoundation.pk/
logo: DRF.png
languages: English, اردو 
services: grants_funding, in_person_training, org_security, web_hosting, web_protection, digital_support, relocation, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: activists, journalists, hrds, lgbti, women, youth, cso, land
hours: 7 วันต่อสัปดาห์ 9:00 – 17:00 น. โซนเวลา UTC+5 (เวลามาตรฐานแปซิฟิค)
response_time: 56 ชั่วโมง
contact_methods: email, pgp, mail, phone, whatsapp
email: helpdesk@digitalrightsfoundation.pk
pgp_key_fingerprint: 4D02 8B68 866F E2AD F26F  D7CC 3283 B8DA 6782 7590
mail: 180A, Garden Block, Garden Town, Lahore, Pakistan
phone: +9280039393
whatsapp: +923323939312
---

DRF เป็นหน่วยงานด้านรณรงค์เคลื่อนไหวผ่านงานวิจัย โดยจดทะเบียนและตั้งอยู่ในประเทศปากีสถาน มูลนิธิฯ นี้ได้รับการก่อตั้งขึ้นเมื่อปี 2555 และมุ่งเน้นให้ความช่วยเหลือด้านสิทธิมนุษยชน การมีส่วนร่วมของคนทุกกลุ่ม กระบวนการประชาธิปไตย และธรรมาภิบาลทางดิจิทัล นอกจากนี้ DRF ทำงานเกี่ยวกับประเด็นเสรีภาพในการพูดแสดงความคิดเห็นในโลกออนไลน์ ความเป็นส่วนตัว การคุ้มครองข้อมูล และความรุนแรงต่อผู้หญิงในโลกออนไลน์
