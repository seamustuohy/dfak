---
layout: page
title: "Peranti saya tampak mencurigakan"
author: Donncha Ó Cearbhaill, Claudio Guarnieri, Rarenet
language: id
summary: "Jika komputer atau ponsel Anda tampak mencurigakan, mungkin ada perangkat lunak yang tidak diinginkan atau berbahaya di peranti Anda."
date: 2023-04
permalink: /id/topics/device-acting-suspiciously
parent: Home
---

# Peranti saya tampak mencurigakan

Serangan perangkat lunak berbahaya (*malware*) telah berevolusi dan menjadi sangat canggih seiring berjalannya waktu. Serangan-serangan tersebut menimbulkan berbagai macam ancaman serta dapat berdampak serius pada infrastruktur dan data pribadi maupun organisasi Anda.

Serangan *malware* hadir dalam berbagai bentuk, misalnya *virus*, *phishing*, *ransomware*, *trojan* dan *rootkit*. Beberapa contoh ancaman tersebut yaitu komputer mengalami *crash*, pencurian data (berupa kredensial akun sensitif, info keuangan, *login* rekening bank), penyerang memeras Anda untuk membayar uang tebusan dengan mengambil alih peranti Anda, atau mengambil kendali peranti Anda dan menggunakannya untuk untuk memata-matai semua yang Anda lakukan secara daring atau meluncurkan serangan *DDoS*.

Beberapa metode yang umum digunakan oleh penyerang untuk mengganggu Anda dan peranti Anda tampak seperti aktivitas biasa, misalnya:

- Surel atau postingan di media sosial yang akan menggoda Anda untuk membuka lampiran atau mengeklik tautan.

- Mendorong orang untuk mengunduh atau memasang perangkat lunak dari sumber yang tidak terpercaya.

- Mendorong orang untuk memasukkan nama pengguna dan kata sandi mereka ke situs web yang dibuat sedemikian rupa agar terlihat tepercaya, padahal tidak.

- Memasang aplikasi perangkat pengintai (*spyware*) komersial di perannti Anda saat Anda membiarkannya tanpa pengawasan dan tidak terkunci.

Bagian Pertolongan Pertama pada Darurat Digital ini akan memandu Anda melalui beberapa langkah dasar untuk mengetahui apakah perangkat Anda terinfeksi *malware* atau tidak.

Jika Anda merasa bahwa komputer atau peranti seluler Anda mulai tampak mencurigakan, pertama-tama pikirkan apa saja gejalanya.

Gejala yang umumnya dapat diartikan sebagai aktivitas perangkat yang mencurigakan, tapi seringkali tak cukup menjadi alasan untuk khawatir, antara lain:

- Bunyi klik saat panggilan telepon
- Baterai terkuras secara tak terduga
- Suhu terlalu panas saat peranti tidak digunakan
- Peranti beroperasi dengan lambat

Gejala-gejala tersebut seringkali disalahpahami sebagai indikator pasti dari aktivitas peranti yang mengkhawatirkan. Akan tetapi, jika gejala tersebut berdiri sendiri maka itu tidak cukup untuk menjadi alasan kekhawatiran.

Gejala pasti dari peranti yang disalahgunakan biasanya adalah:

- Peranti kerap kali memulai ulang (*restart*) dengan sendirinya
- Aplikasi mengalami *crash*, terutama setelah melakukan suatu tindakan
- Pembaruan sistem operasi dan/atau *patch* keamanan gagal berulang kali
- Lampu indikator aktivitas *webcam* menyala saat sedang tidak digunakan
- Kemunculan berulang kali ["Blue Screens of Death"](https://id.wikipedia.org/wiki/Blue_Screen_of_Death) atau *kernel panic*
- *Window* yang berkedip
- Peringatan antivirus

## Workflow

### start

Dengan adanya informasi yang disediakan di pendahuluan, jika Anda masih merasa bahwa peranti Anda mungkin disalahgunakan, panduan berikut ini dapat membantu Anda mengidentifikasi masalahnya.

 - [Saya yakin peranti seluler saya tampak mencurigakan](#phone-intro)
 - [Saya yakin komputer saya tampak mencurigakan](#computer-intro)
 - [Saya sudah tidak merasa peranti saya mungkin disalahgunakan](#device-clean)


### device-clean

> Bagus! Namun, ingatlah bahwa panduan ini hanya untuk membantu Anda melakukan analisis cepat. Meskipun seharusnya cukup untuk mengidentifikasi anomali yang terlihat, *spyware* yang lebih canggih mampu bersembunyi dengan lebih efektif.

Jika Anda masih curiga kalau peranti Anda disalahgunakan, Anda sebaiknya:

- [mencari bantuan tambahan](#malware_end)
- [langsung reset peranti tersebut.](#reset).


### phone-intro

> Penting untuk mempertimbangkan bagaimana peranti Anda bisa disalahgunakan.
>
> - Berapa lama sampai Anda mulai mencurigai peranti Anda tampak mencurigakan?
> - Apakah Anda ingat pernah mengeklik tautan apa pun dari sumber yang tak diketahui?
> - Pernahkah Anda menerima pesan dari pihak yang tidak Anda kenal?
> - Apakah Anda memasang (*install*) perangkat lunak apa pun yang diunduh dari sumber yang tak tepercaya?
> - Pernahkah peranti tersebut tidak berada di tangan Anda?

Pikirkan pertanyaan-pertanyaan tersebut untuk mencoba mengidentifikasi keadaan yang mengarah ke penyalahgunaan peranti Anda.

 - [Saya memiliki peranti Android](#android-intro)
 - [Saya memiliki peranti iOS](#ios-intro)


### android-intro

> Periksalah terlebih dahulu apakah ada aplikasi asing yang terpasang di peranti Android Anda.
>
> Temukan daftar aplikasi di bagian “Aplikasi” pada menu pengaturan. Identifikasi aplikasi apa pun yang tidak terpasang seperti bawaan sebelumnya di peranti Anda dan Anda tidak ingat pernah mengunduhnya.
>
> Jika Anda mencurigai aplikasi manapun yang ada di daftar tersebut, lakukan pencarian di web dan carilah sumber informasi untuk melihat apakah ada laporan tepercaya yang mengidentifikasi aplikasi tersebut sebagai berbahaya.

Apakah Anda menemukan aplikasi yang mencurigakan?

 - [Tidak](#android-unsafe-settings)
 - [Ya, saya menemukan aplikasi yang berpotensi berbahaya](#android-badend)


### android-unsafe-settings

> Android memberi penggunanya pilihan untuk mengaktifkan akses tingkat-rendah ke peranti mereka. Hal ini dapat bermanfaat bagi pengembang perangkat lunak, tapi juga dapat membuat peranti rentan terkena serangan tambahan. Anda harus meninjau pengaturan keamanan ini dan memastikannya diatur ke opsi yang lebih aman. Produsen bisa jadi menjual peranti dengan pengaturan bawaan yang tidak aman. Pengaturan tersebut harus ditinjau meskipun Anda sendiri belum mengubah apa pun.
>
> #### Aplikasi dari Sumber Tak Tepercaya
>
> Android biasanya memblokir pemasangan aplikasi yang tidak berasal dari Google Play Store. Google memiliki serangkaian proses untuk meninjau dan mengidentifikasi aplikasi berbahaya di Play Store. Penyerang seringkali mencoba menghindari pemeriksaan ini dengan mengirimkan aplikasi berbahaya secara langsung kepada pengguna melalui pesan berisi berkas atau tautan untuk pemasangan. Penting halnya untuk memastikan peranti Anda tidak mengizinkan pemasangan aplikasi dari sumber yang tidak tepercaya.
>
> Pastikan pemasangan aplikasi dari sumber yang tidak tepercaya dinonaktifkan di peranti Anda. Di banyak versi Android, Anda bisa menemukan opsi ini di pengaturan keamanan, tapi bisa juga ada di bagian lain, tergantung dari versi Android mana yang Anda jalankan, dan pada tipe ponsel terbaru opsi ini bisa jadi berada di pengaturan perizinan di tiap aplikasi.
>
> #### Mode Pengembang
>
> Android mengizinkan para pengembang untuk secara langsung menjalankan perintah pada sistem operasi yang mendasarinya ketika dalam “Mode Pengembang”. Saat diaktifkan, mode ini dapat membuat peranti rentan terhadap serangan fisik. Seseorang dengan akses fisik ke peranti tersebut dapat menggunakan Mode Pengembang untuk mengunduh salinan data pribadi dari peranti tersebut atau untuk memasang aplikasi berbahaya.
>
> Jika Anda melihat menu Mode Pengembang di pengaturan perangkat Anda, sebaiknya Anda pastikan fitur tersebut dinonaktifkan.
>
> #### Google Play Protect
>
> Layanan Google Play Protect tersedia di semua peranti Android terkini. Layanan ini melakukan pemindaian rutin pada semua aplikasi yang terpasang di peranti Anda. Play Protect juga dapat secara otomatis menghapus aplikasi berbahaya yang diketahui berada di peranti Anda. Mengaktifkan layanan ini akan mengirimkan informasi mengenai peranti Anda (misalnya, aplikasi-aplikasi yang terpasang) pada Google.
>
> Google Play Protect dapat diaktifkan melalui pengaturan keamanan peranti Anda. Informasi lebih lanjut tersedia di situs [Play Protect](https://support.google.com/googleplay/answer/2812853?hl=id&sjid=14470226472030044995-AP).

Apakah Anda menemukan pengaturan yang tidak aman?

- [Tidak](#android-bootloader)
- [Ya, saya menemukan pengaturan yang berpotensi tidak aman](#android-badend)


### android-bootloader

> *Bootloader* Android adalah perangkat lunak utama yang berjalan begitu Anda menyalakan peranti Anda. *Bootloader* memungkinkan sistem operasi untuk memulai mengakses perangkat keras. *Bootloader* yang disalahgunakan dapat memberi penyerang akses penuh ke perangkat keras Anda. Mayoritas produsen menjual peranti mereka dengan *bootloader* yang terkunci. Cara umum untuk mengetahui apakah *bootloader* resmi dari produsen telah diubah adalah dengan memulai ulang (*restart*) peranti Anda dan carilah logo *boot*-nya. Jika muncul sebuah segitiga kuning dengan tanda seru, maka *bootloader* aslinya telah diganti. Peranti Anda mungkin juga telah disalahgunakan jika ia menunjukkan tampilan peringatan *bootloader* yang tidak terkunci dan Anda sendiri belum pernah membuka kuncinya untuk memasang ROM Android khusus seperti CyanogenMod. Anda harus melakukan reset ke setelan pabrik pada peranti Anda jika ia menunjukkan tampilan peringatan *bootloader* tidak terkunci yang tidak Anda duga.

Apakah *bootloader*-nya disalahgunakan atau peranti Anda menggunakan *bootloader* asli?

- [*Bootloader* di peranti saya telah disalahgunakan](#android-badend)
- [Peranti saya menggunakan *bootloader* asli](#android-goodend)


### android-goodend

> Tampaknya peranti Anda tidak disalahgunakan.

Apakah Anda masih khawatir peranti Anda disalahgunakan?

- [Ya, saya ingin mencari bantuan profesional](#malware_end)
- [Tidak, saya sudah mengatasi permasalahan saya](#resolved_end)


### android-badend

> Peranti Anda mungkin telah disalahgunakan. Melakukan reset ke setelan pabrik dapat menghilangkan ancaman apa pun yang ada di peranti Anda. Namun, tindakan ini bukanlah solusi terbaik. Sebagai tambahan, Anda sebaiknya menyelidiki masalah ini lebih jauh untuk mengidentifikasi tingkat kerentanan Anda dan bagaimana tepatnya sifat serangan yang Anda alami.
>
> Anda sebaiknya menggunakan alat untuk diagnosis mandiri bernama [Emergency VPN](https://www.civilsphereproject.org/emergency-vpn) atau [PiRogue](https://pts-project.org/), atau mencari bantuan dari organisasi yang bisa membantu.

Apa yang ingin Anda lakukan?

- [Saya ingin mereset peranti saya ke setelan pabrik](#reset).
- [Saya ingin mencari bantuan profesional](#malware_end)
- [Saya punya jaringan bantuan lokal yang bisa dihubungi untuk mendapatkan bantuan profesional](#resolved_end)


### ios-intro

> Ikuti beberapa langkah tersebut untuk [melihat siapa yang memiliki akses ke iPhone atau iPad Anda](https://support.apple.com/id-id/guide/personal-safety/ipsb8deced49/web). Periksa pengaturan iOS untuk melihat apakah ada yang tidak biasa.
>
> Di app Pengaturan, periksa apakah peranti Anda terkait dengan ID Apple Anda. Butir menu pertama di sisi kiri harus berupa nama Anda atau nama yang Anda gunakan untuk ID Apple Anda. Klik butir tersebut dan periksa apakah alamat surel yang ditampilkan sudah benar. Di bagian bawah halaman ini Anda bisa melihat daftar berisi nama dan model peranti iOS yang terkait dengan dengan ID Apple ini.
>
> Pastikan peranti Anda tidak terkait dengan sistem Manajemen Perangkat Seluler (*MDM*) yang tidak Anda kenali. Buka Pengaturan > Umum > VPN & Manajemen Peranti dan periksa apakah Anda memiliki bagian untuk Profil. Anda bisa [menghapus semua profil konfigurasi yang tidak diketahui](https://support.apple.com/id-id/guide/personal-safety/ips41ef0e8c3/1.0/web/1.0#ips68379dd2e). Jika Anda tidak memiliki Profil yang terdaftar, Anda tidak terdaftar di MDM.

 - [Semua informasi yang ditampilkan benar dan saya masih memegang kendali atas ID Apple saya](#ios-goodend)
 - [Nama atau detail lainnya salah, atau saya menemukan peranti atau Profil dalam daftar tersebut yang bukan milik saya](#ios-badend)


### ios-goodend

> Tampaknya peranti Anda tidak disalahgunakan.

Apakah Anda masih khawatir peranti Anda disalahgunakan?

- [Ya, saya ingin mencari bantuan profesional](#malware_end)
- [Tidak, saya sudah mengatasi permasalahan saya](#resolved_end)


### ios-badend

> Peranti Anda mungkin telah disalahgunakan. Melakukan reset ke setelan pabrik dapat menghilangkan ancaman apa pun yang ada di peranti Anda. Namun, tindakan ini bukanlah solusi terbaik. Sebagai tambahan, Anda sebaiknya menyelidiki masalah ini lebih jauh untuk mengidentifikasi tingkat kerentanan Anda dan bagaimana tepatnya sifat serangan yang Anda alami.
>
> Anda sebaiknya menggunakan alat untuk diagnosis mandiri bernama [Emergency VPN](https://www.civilsphereproject.org/emergency-vpn) atau [PiRogue](https://pts-project.org/), atau mencari bantuan dari organisasi yang bisa membantu.

Apa yang ingin Anda lakukan?

- [Saya ingin mereset peranti saya ke setelan pabrik](#reset).
- [Saya ingin mencari bantuan profesional](#malware_end)
- [Saya punya jaringan bantuan lokal yang bisa dihubungi untuk mendapatkan bantuan profesional](#resolved_end)


### computer-intro

> **Catatan: Jika Anda mengalami serangan *ransomware*, segera menuju ke [No More Ransom!](https://www.nomoreransom.org/).**
>
> Alur kerja ini akan membantu Anda untuk menyelidiki aktivitas mencurigakan pada peranti komputer Anda. Jika Anda membantu seseorang dari jarak jauh, Anda bisa coba mengikuti beberapa langkah yang dijelaskan pada tautan di bawah ini dengan menggunakan perangkat lunak desktop jarak jauh seperti [TeamViewer](https://www.teamviewer.com), atau Anda dapat melihat kerangka kerja forensik jarak jauh seperti [Google Rapid Response (GRR)](https://github.com/google/grr). Ingatlah bahwa latensi dan keandalan jaringan memegang peranan penting untuk bisa melakukan hal tersebut dengan benar.

Silakan pilih sistem operasi yang ingin Anda periksa:

 - [Windows](#windows-intro)
 - [macOS](#mac-intro)


### windows-intro

> Anda bisa mengikuti panduan pengantar ini untuk menyelidiki aktivitas mencurigakan pada peranti Windows:
>
> - [Cara melakukan Live Forensic pada Windows dengan menggunakan Tek](https://github.com/Te-k/how-to-quick-forensic/blob/master/Windows.md)

Apakah panduan ini membantu Anda mengidentifikasi aktivitas berbahaya?

 - [Ya, saya rasa komputer saya telah terinfeksi](#device-infected)
 - [Tidak, tidak ditemukan aktivitas berbahaya apa pun](#device-clean)


### mac-intro

> Untuk mengidentifikasi potensi infeksi pada komputer macOS, ikuti langkah-langkah berikut:
>
> 1. Periksa apakah ada program mencurigakan yang memulai secara otomatis
> 2. Periksa apakah ada proses mencurigakan yang berjalan
> 3. Periksa apakah ada ekstensi kernel yang mencurigakan
>
> Situs web [Objective-See](https://objective-see.com) menyediakan sejumlah perangkat lunak gratis untuk berbagai keperluan guna memfasilitasi proses ini:
>
> - [KnockKnock](https://objective-see.com/products/knockknock.html) dapat digunakan untuk mengidentifikasi daftar program yang memulai secara otomatis.
> - [TaskExplorer](https://objective-see.com/products/taskexplorer.html) dapat digunakan untuk memeriksa proses yang berjalan dan mengidentifikasi proses yang tampak mencurigakan (misalnya proses yang tidak memiliki tanda resmi, atau proses yang diberi tanda bahaya oleh VirusTotal).
> - [KextViewr](https://objective-see.com/products/kextviewr.html) dapat digunakan untuk mengidentifikasi ekstensi kernel mencurigakan yang termuat di komputer macOS.
>
> Jika beberapa program tersebut tidak langsung mengungkap hal apa pun yang mencurigakan dan Anda ingin melakukan asesmen lebih lanjut, Anda bisa menggunakan [pcQF](https://github.com/botherder/pcqf). pcQF adalah perangkat berguna yang menyederhanakan proses mengumpulkan seluruh informasi pada sistem dan menangkap gambaran keseluruhan memori.
>
> Alat tambahan yang dapat bermanfaat untuk mengumpulkan detail lebih lanjut (tapi membutuhkan pemahaman tentang perintah terminal) adalah [AutoMacTC](https://www.crowdstrike.com/blog/automating-mac-forensic-triage/) buatan perusahaan keamanan siber Amerika, CrowdStrike.

Apakah petunjuk ini membantu Anda mengidentifikasi aktivitas berbahaya?

 - [Ya, saya rasa komputer saya telah terinfeksi](#device-infected)
 - [Tidak, tidak ditemukan aktivitas berbahaya apa pun](#device-clean)

### device-infected

Ya ampun! Peranti Anda tampaknya terinfeksi. Untuk menghilangkan infeksinya, Anda perlu:

- [mencari bantuan tambahan](#malware_end)
- [langsung reset peranti tersebut](#reset).

### reset

> Sebaiknya Anda pertimbangkan untuk mereset peranti Anda sebagai tindakan keamanan ekstra. Panduan di bawah ini akan menyediakan petunjuk yang sesuai untuk tipe peranti Anda:
>
> - [Android](https://support.google.com/android/answer/6088915?hl=id)
> - [iOS](https://support.apple.com/id-id/HT201252)
> - [Windows](https://support.microsoft.com/id/help/4000735/windows-10-reinstall)
> - [Mac](https://support.apple.com/id-id/HT201314)

Apakah Anda merasa perlu bantuan tambahan?

- [Ya](#malware_end)
- [Tidak](#resolved_end)


### malware_end

Jika Anda memerlukan bantuan tambahan dalam menangani peranti yang terinfeksi, Anda dapat menghubungi organisasi yang terdaftar di bawah ini.

:[](organisations?services=vulnerabilities_malware)

### resolved_end

Kami harap alur kerja Pertolongan Pertama Darurat Digital (P2D2) ini bermanfaat. Silakan beri kami masukan [melalui email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

Berikut beberapa tip untuk mencegah diri Anda menjadi korban dari penyerang yang berusaha menyalahgunakan peranti dan data Anda:

- Selalu periksa kembali keabsahan surel apa pun yang Anda terima, berkas yang Anda unduh, atau tautan yang meminta detail *login* akun Anda.
- Baca lebih lanjut mengenai cara melindungi peranti Anda dari infeksi *malware* di panduan-panduan yang tertaut di sumber informasi berikut.

#### Resources

- Security in a Box: [Lindungi perangkat Anda dari serangan *malware* dan *phishing* (dalam bahasa Inggris)](https://securityinabox.org/en/phones-and-computers/malware)
  - Tip lebih lanjut (dalam bahasa Inggris) untuk melindungi peranti [Android](https://securityinabox.org/en/phones-and-computers/android), [iOS](https://securityinabox.org/en/phones-and-computers/ios), [Windows](https://securityinabox.org/en/phones-and-computers/windows), [MacOS](https://securityinabox.org/en/phones-and-computers/mac), dan [Linux](https://securityinabox.org/en/phones-and-computers/linux)
- [Surveillance Self-Defense: Cara menghindari Serangan *Phishing* (dalam bahasa Inggris)](https://ssd.eff.org/en/module/how-avoid-phishing-attacks)
- [Keamanan tanpa batas: Panduan tentang *Phishing* (dalam bahasa Inggris)](https://github.com/securitywithoutborders/guide-to-phishing/blob/master/SUMMARY.md)
