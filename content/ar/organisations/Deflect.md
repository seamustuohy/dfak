---
name: Deflect
website: https://www.deflect.ca
logo: deflect.png
languages: English, Français, Русский, Español, Bahasa Indonesia, Filipino
services: ddos, web_hosting, web_protection
beneficiaries: hrds, cso
hours: Monday-Friday, 24/5, UTC-4
response_time: 6 ساعات
contact_methods: email
email: support@equalit.ie
initial_intake: no
---

دِفلِكت خدمة مجانيّة لتأمين مواقع الوِب مَنْ الهجمات، مُقدَّمة إلى المجتمع المدني و&nbsp;مجموعات المدافعين عن حقوق الإنسان.

