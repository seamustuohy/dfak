---
layout: page
title: "Pienso que me están vigilando"
author: Carlos Guerra, Peter Steudtner, Ahmad Gharbeia
language: es
summary: "Si crees que puedes ser un objetivo de vigilancia digital, este flujo de trabajo te guiará a través de preguntas para identificar posibles indicadores de vigilancia."
date: 2023-05
permalink: /es/topics/surveilled
parent: Home
---

# Pienso que me están vigilando

Recientemente, hemos visto muchos casos emergentes de vigilancia de personas activistas del espacio cívico, desde el uso de stalkerware por parte de socios abusivos y vigilancia corporativa de sus personas trabajadoras, hasta vigilancia estatal masiva y campañas de spyware dirigidas contra activistas y personas en el periodismo. Dado el amplio espectro de técnicas y precedentes observados, es imposible cubrir todos los aspectos de la vigilancia, especialmente para el apoyo de emergencia.

Dicho esto, este flujo de trabajo se centrará en casos comunes vistos en la esfera digital y relacionados con dispositivos de personas usuarias finales y cuentas en línea que representan emergencias con respuesta accionable. Cuando no haya una solución factible, entonces no la consideraremos una emergencia y no se cubrirá extensamente (en su lugar, se compartirán las referencias).

Algunos de los casos que este flujo de trabajo no cubre son:

- Vigilancia física. Si temes ser objeto de vigilancia física y deseas ayuda, puedes comunicarte con las organizaciones que se centran en la seguridad física en el Kit de Primeros Auxilios Digitales [página de ayuda](../../support).
- Vigilancia masiva con hardware como cámaras de CCTV en espacios públicos, corporativos y privados.
- Errores de hardware que no dependen de una conexión a Internet

Un segundo descargo de responsabilidad relevante es que en la actualidad todo el mundo está siendo vigilado de forma predeterminada en algún grado:

- La infraestructura de telefonía móvil está diseñada para recopilar una gran cantidad de datos sobre las personas que la usa, que pueden ser utilizados por varios actores para conocer detalles como nuestra ubicación, red de contactos, etc.
- Los servicios en línea como las redes sociales y los proveedores de correo electrónico también recopilan una gran cantidad de datos sobre  las personas que los usan, que pueden vincularse, por ejemplo, a campañas publicitarias para mostrarnos anuncios relacionados con los temas de nuestras publicaciones o incluso mensajes uno a uno.
- Cada vez más países cuentan con muchos sistemas interconectados que utilizan nuestra identidad y recopilan información sobre nuestras interacciones con diversas instituciones públicas, lo que permite la creación de un "perfil" basado en solicitudes de documentos de información fiscal o de salud.

Estos ejemplos de vigilancia masiva, además de los muchos precedentes de operaciones dirigidas, crean la sensación de que todas las personas (especialmente en el espacio activista) están siendo vigilados, lo que puede generar paranoia y afectar nuestra capacidad emocional para enfrentar las amenazas reales que podemos afrontar. Dicho esto, existen casos concretos en los que -dependiendo de nuestro perfil de riesgo, del trabajo que realizamos y de las capacidades de nuestros potenciales oponentes- podríamos tener sospechas fundadas de que nos vigilan. Si ese es tu caso, sigue leyendo.

Como tercer descargo de responsabilidad, muchos esquemas de vigilancia (especialmente los más específicos), generalmente dejan pocos o ningún indicador accesible a las víctimas, lo que hace que la detección sea difícil y, a veces, imposible. Ten esto en cuenta mientras navegas por el flujo de trabajo, y si aún crees que tu caso no está cubierto o es más complejo, comunícate con las organizaciones enumeradas al final del flujo de trabajo.

Como consideración final, ten en cuenta que los esfuerzos de vigilancia pueden ser masivos o dirigidos. Las operaciones de vigilancia masiva suelen cubrir poblaciones enteras o gran parte de ellas, como todas las personas usuarias de una plataforma, todas las de una región específica, etc. Éstas suelen depender más de la tecnología y suelen ser menos peligrosas que la vigilancia dirigida, que se dirige a una persona o un pequeño número de ellas, requiere recursos dedicados (dinero, tiempo y/o capacidad) y suele ser más peligroso que la vigilancia masiva dadas las motivaciones detrás de poner recursos para monitorear la actividad y las comunicaciones de personas específicas.

Si encontraste un indicador de que estás bajo vigilancia, haz clic en iniciar para comenzar con el flujo de trabajo.

## Workflow

### start

> Si está aquí, probablemente tenga motivos para pensar que sus comunicaciones, su ubicación u otras actividades en línea podrían ser vigiladas. A continuación encontrará una serie de indicadores comunes de actividad sospechosa relacionada con la vigilancia. Haga clic en su caso para continuar.

Dada la información provista en la introducción, ¿qué tipo de problema está experimentando?

 - [Encontré un dispositivo de rastreo cerca de mí](#tracking-device-intro)
 - [Mi dispositivo está actuando de manera sospechosa](#device-behaviour-intro)
 - [Me están redirigiendo a sitios web http desprotegidos para instalar actualizaciones o aplicaciones](#suspicious-redirections-intro)
 - [Recibo mensajes de seguridad y alertas de servicios de confianza](#security-alerts-intro)
 - [Encontré un dispositivo sospechoso vinculado a mi ordenador o red](#suspicious-device-intro)
 - [La información que compartí de forma privada a través de correo electrónico, messenger o similar es conocida por rival](#leaked-internet-info-intro)
 - [La información confidencial compartida a través de llamadas telefónicas o SMS parece haberse filtrado a rival (especialmente durante eventos delicados)](#leaked-phone-info-intro)
 - [Las personas que te rodean han sido objeto de medidas de vigilancia con éxito](#peer-compromised-intro)
 - [Algo más / No sé](#other-intro)

### tracking-device-intro

> Si encuentras un dispositivo inesperado cerca de ti y crees que es un dispositivo de vigilancia, el primer paso sería confirmar que efectivamente el dispositivo está allí para rastrearte y no cumple otras funciones relevantes. Con la introducción de los dispositivos de Internet de las cosas (IoT), es más común encontrar dispositivos que realizan una gran variedad de acciones que podemos olvidar después de usarlos por un tiempo. Uno de los primeros pasos para asegurarse de que se trata de un dispositivo de vigilancia es inspeccionar el dispositivo para obtener información sobre la marca, el modelo, etc. Algunos dispositivos de rastreo comunes que están disponibles a precios bajos en el mercado incluyen Apple Airtags, Samsung Galaxy SmartTag o Tile. Estos dispositivos se venden con fines legítimos, como ayudar a encontrar artículos personales. Sin embargo, las personas actoras maliciosas pueden abusar de ellos para rastrear a otras personas sin su consentimiento.
>
> Para comprobar si nos está siguiendo un rastreador inesperado, además de localizarlo físicamente, podría ser útil considerar algunas soluciones específicas:
>
> - Para Apple Airtags, en iOS [debería recibir una notificación automática después de que el dispositivo desconocido haya sido detectado por algún tiempo](https://support.apple.com/es-es/HT212227).
> -  Para Android, existe una [aplicación para buscar Airtags cercanos (en inglés)](https://play.google.com/store/apps/details?id=com.apple.trackerdetect).
> - Para dispositivos Tile, la [aplicación Tile (en inglés)](https://www.tile.com/download) tiene una función para buscar rastreadores desconocidos.
> - Para Samsung Galaxy SmartTags, puedes usar la [aplicación SmartThings](https://www.samsung.com/es/smartthings/all-smartthings/) para una función similar.
>
> Otro tipo de dispositivo que puedes encontrar son los rastreadores GPS. Estos registran la ubicación del objetivo con frecuencia y, en algunos casos, pueden transmitir la información a través de una línea telefónica, o pueden almacenar las ubicaciones en algún tipo de almacenamiento interno que luego se puede recopilar físicamente para descargar y analizar. Si encuentras un dispositivo como éste, comprender la forma en como funciona es clave para tener una mejor idea de quién plantó el dispositivo y cómo obtienen la información.
>
> Para cualquier tipo de dispositivo de vigilancia que encuentres, debes documentar todo sobre él: marca, modelo, si se conecta a la red inalámbrica, dónde exactamente lo encontraste, los nombres del dispositivo en la aplicación correspondiente, etc.

Después de identificar el dispositivo, hay algunas estrategias que quizás desees seguir. ¿Qué quieres hacer con el dispositivo?

 - [Quiero desactivarlo](#tracking-disable-device)
 - [Me gustaría usarlo para engañar a quien lo maneja](#tracking-use-device)

### tracking-disable-device

> Según el tipo y la marca del dispositivo que hayas encontrado, es posible que puedas apagarlo o deshabilitarlo por completo. En algunos casos, esto se puede hacer a través de aplicaciones de rastreo como las descritas en el paso anterior, pero también a través de interruptores de alimentación físicos, o incluso dañando el dispositivo. Ten en cuenta que si deseas realizar una investigación más profunda sobre este incidente, deshabilitar el dispositivo podría alterar la evidencia relevante almacenada en el dispositivo.

¿Deseas tomar más medidas para identificar a quien posee el dispositivo? (*ten en cuenta que tendrás que mantener el dispositivo de rastreo en línea para hacerlo*)

 - [Sí](#identify-device-owner)
 - [No](#pre-closure)

### tracking-use-device

> Una estrategia común para lidiar con los dispositivos de rastreo es hacer que rastreen algo diferente de lo que se pretendía. Por ejemplo, puedes dejarlo en una posición estática segura mientras vas a otros lugares, o incluso puedes hacer que rastree un objetivo diferente en movimiento que no esté relacionado contigo. Ten en cuenta que en algunos de estos escenarios puedes perder el acceso al dispositivo y hacer que quien rastrea siga a otra persona o rastree una ubicación específica puede tener implicaciones legales y de seguridad.

¿Deseas tomar más medidas para identificar la propiedad del dispositivo? (*ten en cuenta que tendrás que mantener el dispositivo de rastreo en línea para hacerlo*)

 - [Sí](#identify-device-owner)
 - [No](#pre-closure)

### identify-device-owner

> Identificar e quien tiene la propiedad del dispositivo de rastreo puede ser difícil en algunos escenarios, y esto dependerá en gran medida del tipo de dispositivo utilizado, por lo que recomendamos investigar específicamente sobre el dispositivo de rastreo que has encontrado. Algunos ejemplos son:
>
> - Para rastreadores inalámbricos de consumo:
> - ¿Cómo se muestran en las aplicaciones de revisión?
> - ¿Cuándo recibiste una notificación?
> - ¿Hay alguna forma de extraer más datos del dispositivo que pueda contener información relevante?
> - Para rastreadores más dedicados, como rastreadores GPS:
> - ¿Tienen almacenamiento interno? Algunos modelos tienen una tarjeta SD que se puede extraer para ver cuándo comenzó a capturar datos o cualquier otra información relevante.
> - ¿Tienen línea telefónica? ¿Puedes extraer una tarjeta SIM del dispositivo y ver si puedes probarla en otro teléfono para obtener el número? Con esta información, ¿puedes saber quién posee esa línea telefónica?
> - En general
> - Busca señales físicas, como nombres escritos en el dispositivo, números de inventario, etc.
> - Si el dispositivo se conecta a la red, ¿tiene algún nombre de dispositivo específico que pueda incluir datos de identificación?
> - ¿Quién podría acceder al lugar donde encontraste el dispositivo? Conociendo el lugar, ¿cuándo se podría haber plantado?

¿Quieres emprender acciones legales contra la propiedad del dispositivo de rastreo?

 - [Sí](#legal-action-device-owner)
 - [No](#pre-closure)

### legal-action-device-owner

> En caso de que desees emprender acciones legales contra la propiedad del dispositivo de vigilancia, deberás verificar cómo es el proceso en tu jurisdicción. En algunos casos requerirá de servicios legales, en otros bastará con acercarse a una comisaría para iniciar el proceso. El único consejo común a todos los casos es que cualquier caso será tan sólido como la evidencia que recopile a su alrededor. Un buen comienzo es seguir los consejos proporcionados en los pasos anteriores de este flujo de trabajo sobre qué documentar. Puedes encontrar más información y ejemplos en la guía Kit de primeros auxilios digitales para [documentar emergencias digitales](/../../documentation).

¿Necesitas ayuda para emprender acciones legales contra la propiedad del dispositivo de rastreo?

 - [Sí, necesito apoyo legal](#legal_end)
 - [No, creo que he resuelto mis problemas](#resolved_end)
 - [No, pero me gustaría considerar otros escenarios de vigilancia](#pre-closure)

### device-behaviour-intro

> Una de las técnicas de vigilancia dirigida más comunes hasta la fecha es infectar teléfonos o computadoras con software espia -software malicioso diseñado para monitorear y transmitir actividad a terceras personas. El software espía puede ser implantado por diferentes actores a través de diferentes métodos, como por ejemplo, parejas abusivas que descargan manualmente una aplicación de espionaje (comúnmente conocida como stalkerware o software de cónyuge) en el dispositivo, o por el crimen organizado que envía enlaces de phishing para descargar una aplicación.
>
> Algunos ejemplos de indicadores de vigilancia pueden incluir aplicaciones sospechosas que encontraste en tu dispositivo con permisos para micrófono, cámara, acceso a la red, etc. Ver el indicador de la cámara web sin usar ninguna aplicación que deba operarla, o tener el contenido de los archivos en el dispositivo "filtrado".
>
> Para tales casos, puedes consultar el flujo de trabajo del Kit de Primeros Auxilios Digitales "Mi dispositivo está actuando de manera sospechosa".

¿Qué te gustaría hacer?

 - [Llévame al flujo de trabajo del dispositivo que actúa de manera sospechosa](../../../device-acting-suspiciously)
 - [Llévame al siguiente paso aquí](#pre-closure)

### suspicious-redirections-intro

> Aunque esto es poco común, a veces vemos alertas de seguridad cuando intentamos actualizar aplicaciones o incluso el sistema operativo de nuestros dispositivos. Hay muchas razones para esto, y pueden ser legítimas o maliciosas. Algunas preguntas orientadoras que debemos hacernos son:
>
> - ¿Tiene mi dispositivo una fecha y hora exactas? La forma en que nuestros dispositivos confían en los servidores para operar de manera más segura depende de verificar los certificados de seguridad que son válidos dentro de un período de tiempo específico. Si, por ejemplo, tu ordenador está configurado en un año anterior, estos controles de seguridad fallarán para varios sitios web y servicios. Si tu fecha y hora son correctas, también podría sugerir que el proveedor no actualizó sus certificados o que está sucediendo algo sospechoso. En cualquier caso, es una buena costumbre en general nunca actualizar ni operar aplicaciones en tu dispositivo si ves estas advertencias de seguridad o redirecciones.
> - ¿Apareció el problema después de un evento poco común, como hacer clic en un anuncio, instalar una aplicación o abrir un documento? Muchos ataques se basan en la suplantación de procesos, como instalaciones y actualizaciones de software legítimos, por lo que es posible que tu dispositivo lo descubra y brinde una advertencia o detenga el proceso por completo.
> - ¿El proceso con el que tienes problemas está ocurriendo de una manera inusual, por ejemplo, normalmente el proceso de actualización de esa aplicación sigue un proceso diferente al que estás viendo?
>
> En cualquier caso, no olvides documentarlo todo de la forma más detallada posible, especialmente si deseas recibir ayuda especializada, realizar una investigación más profunda o emprender alguna acción legal.
>
> En casi todos los casos, será suficiente abordar la causa raíz del problema antes de que el dispositivo y la navegación vuelvan a la normalidad. Sin embargo, puede haber más problemas si realmente instalaste o actualizaste una aplicación (o el sistema operativo) en circunstancias sospechosas.

¿Hiciste clic y descargaste o instalaste algún software o actualizaciones en condiciones sospechosas?

 - [Sí](#pre-closure)
 - [No](#suspicious-software-installed)

### suspicious-software-installed

> En la mayoría de los casos, quien ataca y secuestra los procesos de instalación o actualización de aplicaciones o sistemas operativos tiene como objetivo instalar malware en el sistema. Si sospechas que este es tu escenario, te recomendamos que vayas al flujo de trabajo del Kit de Primeros Auxilios Digitales "Mi dispositivo está actuando de manera sospechosa".

¿Qué te gustaría hacer?

 - [Llévame al flujo de trabajo del dispositivo que actúa de manera sospechosa](../../../device-acting-suspiciously)
 - [Llévame al siguiente paso aquí](#pre-closure)

### security-alerts-intro

> Si estás recibiendo mensajes o alertas de notificación de servicios de correo electrónico, plataformas de redes sociales o cualquier otro servicio en línea que realmente uses, y están relacionados con problemas de seguridad, como nuevos inicios de sesión sospechosos desde nuevos dispositivos o ubicaciones, primero puedes verificar si el mensaje es legítimo o si es un mensaje de phishing. Para hacer esto, recomendamos verificar el flujo de trabajo del Kit de Primeros Auxilios Digitales [Recibí un mensaje sospechoso](../../../suspicious-messages) antes de continuar.
>
> Si el mensaje es legítimo, la siguiente pregunta que debes hacerte es si hay alguna razón por la que este mensaje pueda ser legítimo. Por ejemplo, si usas un servicio VPN o Tor y navegas por la versión web de un servicio, pensará que tienes ubicación en el país de su servidor VPN o nodo de salida Tor, lo que activará una alerta de un nuevo inicio de sesión sospechoso, cuando eres tú simplemente usando una solución de cimcurvention.
>
> Otra cosa útil que debemos preguntarnos es qué tipo de alerta de seguridad estamos recibiendo: ¿se trata de que alguien intenta iniciar sesión en tu cuenta o de que lo está logrando? Dependiendo de la respuesta, la solución requerida puede ser muy diferente.
>
> Si recibimos una notificación sobre intentos de inicio de sesión fallidos, y tenemos una buena contraseña (larga, no compartida con otros servicios inseguros, etc.) y usamos autenticación multifactor (también conocida como MFA o 2FA), no debemos preocuparnos demasiado de tener la cuenta comprometida. Independientemente de la gravedad de la amenaza de compromiso de la cuenta, siempre es bueno verificar los registros de actividad reciente, donde generalmente podemos revisar nuestro historial de inicio de sesión, incluida la ubicación y el tipo de dispositivo.
>
> Si ves algo sospechoso en tus registros de actividad, las estrategias más comunes, según la cuenta, incluyen:
>
> - Cambio de contraseñas
> - Habilitación de MFA
> - Intentar desconectar todos los dispositivos de la cuenta
> - Documentar indicadores del intento de piratería (en caso de que queramos buscar ayuda, investigar más o iniciar una acción legal)
>
> Otra pregunta importante para ayudar a evaluar estas notificaciones es si compartimos dispositivos o accedemos a la cuenta con otra persona que podría haber activado la alerta. Si este es el caso, ten cuidado con el nivel de acceso que le das a terceras personas a tu información y repite los pasos anteriores si deseas revocar el acceso para esta tercera persona.
>

Si crees que hay otras amenazas potenciales enumeradas al comienzo del flujo de trabajo que deseas verificar, o si tienes un caso más complejo y deseas buscar ayuda, haz clic en Siguiente.

 - [Siguiente](#pre-closure)


### suspicious-device-intro

> Si hemos encontrado un dispositivo sospechoso, lo primero que debemos preguntarnos es si este dispositivo es malicioso o si está destinado a estar donde está. Esto se debe a que en los últimos años ha crecido considerablemente la cantidad de dispositivos con capacidad de comunicación en nuestros hogares y lugares de trabajo: impresoras, luces inteligentes, lavadoras, termostatos, etc. Dada la gran cantidad de estos dispositivos, es posible que terminemos preocupándonos por un dispositivo que no recordamos haber configurado o que fue configurado por otra persona pero que no es malicioso.
>
> En caso de que no sepas si el dispositivo que encontraste es malicioso, una de las primeras cosas que debes hacer es obtener información sobre él: marca, modelo, si está conectado a la red cableada o inalámbrica, si dice qué hace, si está encendido, etc. Si aún no sabes qué hace el dispositivo, puedes usar esta información para buscar en la web y obtener más información sobre qué es y qué hace el dispositivo.

Después de revisar el dispositivo, ¿crees que es legítimo?

 - [Era un dispositivo legítimo](#pre-closure)
 - [He revisado pero todavía no sé qué hace el dispositivo o si es malicioso](#suspicious-device2)


### suspicious-device2

> En caso de que aún tengas preocupación por el dispositivo después de una revisión inicial, debes considerar la posibilidad remota de que se trate de un dispositivo de vigilancia. Por lo general, estos dispositivos monitorean la actividad en la red a la que están conectados, o incluso pueden grabar y transmitir audio y/o video como lo hace el software espía. Es importante entender que este escenario es muy poco común y suele estar ligado a perfiles de muy alto riesgo. Sin embargo, si esto te preocupa, algunas cosas que puedes hacer incluyen:
>
> - Desconectar el dispositivo de la red/alimentación y comprobar si todos los servicios previstos funcionan sin problemas. Después de eso, puedes hacer que alguien con experiencia analice el dispositivo.
> - Investigarlo un poco más y buscar ayuda (esto generalmente implica pruebas más avanzadas como mapear la red o buscar señales poco comunes).
> - Como medida preventiva para mitigar cualquier vigilancia en la red, si dejas el dispositivo conectado, utiliza VPN, Tor o una herramienta similar que cifre tu navegación web, así como herramientas de comunicación con cifrado de extremo a extremo.
> - Como medida preventiva para mitigar la posible grabación de audio/video si dejas el dispositivo conectado, aísla físicamente el dispositivo para que no capture ningún audio o video valioso.

Si crees que hay otras amenazas potenciales enumeradas al principio que deseas verificar o deseas buscar ayuda, haz clic en "Siguiente".

 - [Siguiente](#pre-closure)
 - [Creo que he resuelto mis problemas](#resolved_end)

### leaked-internet-info-intro

> Esta rama del flujo de trabajo cubrirá los datos filtrados de los servicios y la actividad de Internet. Para obtener información filtrada de las comunicaciones telefónicas, consulta la sección ["La información confidencial compartida a través de llamadas telefónicas o SMS parece haberse filtrado a rival"](#leaked-phone-info-intro).
>
> En general, las filtraciones de servicios basados ​​en Internet pueden ocurrir por tres razones:
>
> 1. La información era pública en primer lugar, lo cual no se tratará en este recurso.
> 2. La información se filtró directamente de la cuenta (más común a rivales "menores" y más cercanos a la víctima).
> 3. La información se filtró desde la plataforma en la que se cargó en primer lugar (más común para "grandes" rivales como gobiernos y grandes empresas).
>
> Cubriremos los dos últimos casos, ya que están más relacionados con emergencias comunes que se ven en la comunidad del espacio cívico. Puedes seguir el flujo de trabajo a partir de la siguiente pregunta:

¿Compartes dispositivos o cuentas con tu rival?

 - [Sí](#shared-devices-or-accounts)
 - [No](#leaked-internet-info2)

### shared-devices-or-accounts

> En algunos casos, compartir el acceso a la cuenta con otras personas puede dar acceso a partes no deseadas de información confidencial, que luego se pueden filtrar fácilmente. Además, generalmente cuando compartimos cuentas, necesitamos hacer que el proceso sea práctico, lo que a menudo afecta la seguridad de los mecanismos de acceso a la cuenta, como hacer que las contraseñas sean más débiles para compartirlas fácilmente y deshabilitar la autenticación multifactor (MFA o 2FA) para permitir que muchas personas inicien sesión. en la misma cuenta.
>
> Un primer consejo sería evaluar quién debe tener acceso a las cuentas o recursos relevantes (como documentos almacenados en la nube) y limitar esto a la menor cantidad de personas posible. Otro principio útil es tener una cuenta para cada persona, lo que permite que configuren sus ajustes de acceso de la manera más segura posible.
>
> También se recomienda consultar la sección ["Recibo mensajes de seguridad y alertas de servicios de confianza"](#security-alerts-intro), donde se pueden encontrar consejos para fortalecer las cuentas en caso de accesos sospechosos.

Haz clic en Siguiente para explorar tu situación con más detalle.

 - [Siguiente](#leaked-internet-info2)

### leaked-internet-info2

¿Puede tu rival controlar o acceder a los servicios en línea que utilizas y donde almacenas la información filtrada? (*Esto suele aplicarse a los gobiernos, las fuerzas del orden o los propios proveedores*)

 - [Sí](#control-of-online-services)
 - [No](#leaked-internet-info3)

### control-of-online-services

> A veces, rivales acceden fácilmente a las plataformas que usamos o incluso las controlan. Algunas preguntas comunes que puedes hacerte son:
>
> - ¿Quién tiene la propiedad del servicio?
> - ¿Es rival?
> - ¿Tiene acceso a los servidores o datos a través de consultas de datos legales?
>
> En ese caso, es recomendable mover la información a una plataforma diferente no controlada por rivales para evitar futuras filtraciones.
>
> Otros indicadores útiles podrían ser:
>
> - Actividad de inicio de sesión (incluidas las ubicaciones geográficas), si está disponible
> - Comportamiento extraño de elementos leídos y no leídos
> - Configuraciones que afectan lo que se hace con los mensajes o la información, como redireccionamientos, reglas para rechazar correos electrónicos, etc., cuando estén disponibles
>
> También puedes verificar si tus mensajes son leídos por alguien sin querer usando tokens Canary, como los que podemos generar en [canarytokens.org (en inglés)](https://canarytokens.org/generate)

Haz clic en Siguiente para explorar tu situación con más detalle.

 - [Siguiente](#leaked-internet-info3)

### leaked-internet-info3

¿Tiene tu rival acceso a equipos de vigilancia y capacidad para desplegarlos cerca de ti?

 - [Sí](#leaked-internet-info4)
 - [No](#pre-closure)

### leaked-internet-info4

> Rivales más poderosos pueden tener la capacidad de desplegar equipos de vigilancia especializados alrededor de sus víctimas, como colocar dispositivos de rastreo o infectar sus propios dispositivos con software espía.

¿Crees que podrías ser rastreado por un dispositivo de rastreo o que tu computadora o móvil puede haber sido infectado con software malicioso para espiarte?

 - [Me gustaría obtener más información sobre los dispositivos de rastreo](#tracking-device-intro)
 - [Me gustaría obtener más información sobre la posibilidad de que mi dispositivo se vea comprometido con software malicioso](#device-behaviour-intro)

### leaked-phone-info-intro

> Como se describe en la introducción de este flujo de trabajo, la vigilancia telefónica masiva es una amenaza omnipresente en la mayoría de las regiones y países (si no en todos). Como referencia, debemos ser conscientes de que es realmente fácil para las compañías telefónicas (y cualquier persona con control o acceso a ellas) verificar información como:
>
> - Ubicación histórica (cada vez que el teléfono está encendido y conectado a la red), los datos de las torres del teléfono móvil al que está conectado el teléfono
> - Registros de llamadas (quién llama a quién, cuándo y por cuánto tiempo)
> - SMS (tanto los registros como el contenido real de los mensajes)
> - Tráfico de Internet (esto es menos común y menos útil para rivales, pero sería posible inferir de esto qué aplicaciones se usan, cuándo, qué sitios web se consultan, etc.)
>
> Otro escenario que todavía se observa en algunos contextos, aunque poco común, es el uso de dispositivos de interceptación física llamados rayas o IMSI-Catchers. Estos dispositivos se hacen pasar por una torre de red celular legítima para redirigir a través de ellos el tráfico telefónico de personas en un área física cercana de alcance. Si bien las vulnerabilidades que habilitan estos dispositivos se resuelven o mitigan con protocolos más nuevos (4G/5G), aún es posible bloquear la señal para hacer que los teléfonos crean que el único protocolo disponible es 2G (el más abusado por estos dispositivos). Para este caso, hay algunos indicadores con cierta confiabilidad, especialmente "degradados" a 2G en áreas donde debería haber 4G o 5G o donde otras personas alrededor están conectadas a los protocolos más nuevos sin problemas. Para obtener más información, puede consultar el [sitio web del Proyecto FADe (en inglés)](https://fadeproject.org/?page_id=36). Además, puedes preguntarte si tu rival tiene acceso comprobado a este tipo de dispositivos, entre otras cosas.
>
> Como recomendación general para estos escenarios, es aconsejable que las personas en riesgo migren las comunicaciones a canales cifrados que dependan de Internet en lugar de llamadas telefónicas y SMS convencionales. Lo mismo ocurre con el uso de VPN o Tor para el tráfico de Internet confidencial (incluidas las aplicaciones). La gran asignatura pendiente sería el seguimiento de la ubicación de la persona usuaria, algo ineludible mientras permanece conectado a la red telefónica.
>
> Con respecto a la recopilación masiva de datos a través de operadores, en algunas jurisdicciones las personas usuarias pueden solicitar cuales de sus datos se compartieron en el pasado. Puedes investigar si esto se te aplica y si esa información será confiable para detectar una posible vigilancia. Ten en cuenta que esto implica generalmente una investigación legal.

Si deseas verificar otras amenazas potenciales o tienes un caso más complejo y deseas buscar ayuda, haz clic en Siguiente.

 - [Siguiente](#pre-closure)


### peer-compromised-intro

> En caso de que alguna persona u organización relacionada contigo haya sido víctima de vigilancia, te recomendamos que intentes comprender las técnicas que se utilizaron en estas personas y selecciones en la lista anterior las más cercanas, para que puedas recibir un contexto más relevante y decidas si tú también podrías ser víctima de técnicas similares.

¿Qué te gustaría hacer?

 - [Llévame al paso anterior](#start)
 - [No lo tengo claro](#pre-closure)

### other-intro

> La lista que viste anteriormente incluía los casos más comunes vistos en el espacio civil por ayudantes, pero es posible que tu situación no esté cubierta allí. Si ese es tu caso, haz clic en "Necesito ayuda"; de lo contrario, puedes volver a la lista de casos más comunes y elegir el escenario más cercano que se te aplique para obtener una guía más específica.

¿Qué te gustaría hacer?

 - [Llévame a la lista de amenazas potenciales](#start)
 - [Necesito ayuda](#help_end)

### pre-closure

> Esperamos que este flujo de trabajo haya sido útil hasta ahora. Puedes optar por volver a la lista inicial de escenarios para comprobar otras amenazas que te puedan preocupar, o ir a la lista de organizaciones con las que podrías ponerse en contacto para que te ayuden con casos más complejos.

¿Qué te gustaría hacer?

 - [Me gustaría aprender sobre otras técnicas de vigilancia](#start)
 - [Creo que mi necesidad no está cubierta en esta sección o es demasiado compleja](#help_end)
 - [Creo que tengo una idea clara de lo que me está pasando](#resolved_end)


### help_end

> Si necesitas ayuda adicional para tratar casos de vigilancia, puedes comunicarse con las organizaciones que se enumeran a continuación.
>
> *Nota: cuando te comuniques con cualquier organización para buscar ayuda, comparte la ruta que seguiste aquí y cualquier indicador que hayas encontrado para facilitar cualquier paso siguiente.*

:[](organisations?services=legal&services=vulnerabilities_malware&services=forensic)

### legal_end

> Si necesitas ayuda para demandar a alguien por espiarte ilegalmente, puedes comunicarte con una de las organizaciones a continuación.

:[](organisations?services=legal)

### resolved_end

Esperemos que esta guía de solución de problemas te haya sido útil. Envía tus comentarios [por correo electrónico](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- Migra cualquier comunicación de canales no seguros, como llamadas telefónicas y SMS, a canales de comunicación cifrados que dependen de Internet, como aplicaciones de mensajería cifradas de extremo a extremo y sitios web protegidos a través de HTTPS.
- Si temes que tu tráfico de Internet esté siendo monitoreado y crees que esto puede ser un riesgo de seguridad, considera conectarse a Internet a través de una herramienta de cifrado como VPN o Tor.
- Realiza un seguimiento de todos los dispositivos presentes en tus espacios, especialmente aquellos conectados a internet, y trata de saber qué hacen.
- Evita compartir información confidencial a través de un servicio controlado por rivales.
- Minimiza compartir tus cuentas o dispositivos tanto como puedas.
- Ten cuidado con los mensajes, sitios web, descargas y aplicaciones sospechosos.

#### resources

- [La aplicación de Android de Apple para escanear AirTags es un paso necesario, pero se necesitan más mitigaciones contra el acecho (en inglés)](https://www.eff.org/es/deeplinks/2021/12/apples-android-app-scan-airtags-necessary-step-forward-more-anti-stalking).
- [Privacidad para estudiantes (en inglés)](https://ssd.eff.org/module/privacy-students): con referencias a técnicas de vigilancia en escuelas que pueden aplicarse a muchos otros escenarios.
- [Security in a Box: Proteje la privacidad de tu comunicación en línea (en inglés)](https://securityinabox.org/en/communication/private-communication/)
- [Security in a Box: Visita sitios web bloqueados y navega de forma anónima (en inglés)](https://securityinabox.org/en/internet-connection/anonymity-and-circumvention/)
- [Security in a Box: Recomendaciones para herramientas de chat cifradas (en inglés)](https://securityinabox.org/en/communication/tools/#more-secure-text-voice-and-video-chat-applications)
