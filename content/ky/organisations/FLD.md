---
name: Front Line Defenders
website: https://www.frontlinedefenders.org/emergency-contact
logo: FrontLineDefenders.jpg
languages: Español, English, Русский, فارسی, Français, Português, Türkçe , العربية, 中文
services: grants_funding, in_person_training, org_security, digital_support, relocation, assessment, secure_comms, device_security, vulnerabilities_malware, browsing, account, harassment, forensic, legal, individual_care, advocacy, censorship
beneficiaries: hrds, hros
hours: кечиктирилгис маселелер боюнча 24/7 (дүйнөнүн булуң бурчунан); кеңсе IST (UTC+1) боюнча иш убактысында дүйшөмбүдөн жумага чейин ачык; кызматкерлер ар кайсы аймактарда жана ар кандай убакыт алкактарында иштешет.
response_time: (шашылыш маселелер боюнча) ошол эле күнү же кийинки күнү
contact_methods: web_form, phone, skype, email
web_form: https://www.frontlinedefenders.org/secure/comment.php
phone: +353-1-210-0489 for emergencies; +353-1-212-3750 office phone
skype: front-line-emergency?call
email: info@frontlinedefenders.org for comments or questions
initial_intake: yes
---

Front Line Defenders — баш кеңсеси Ирландияда жайгашкан эл аралык уюм. Анын милдети – укук коргоочуларды “тобокелдик тобунан” ар тараптуу коргоо болуп саналат. Front Line Defenders аларга коопсуздук гранттары, физикалык жана санариптик коопсуздук боюнча тренингдер, алардын кызыкчылыктарын жана кампанияларын колдоо түрүндө тез, практикалык жардам көрсөтүшөт.

Front Line Defenders эл аралык уюмунун шашылыш маселелер боюнча ыкчам байланыш телефону бар: +353-121-00489. Ал араб, англис, француз, орус жана испан тилдеринде 24/7 жеткиликтүү. Ыкчам байланыш телефонун өтө коркунучтуу кырдаалга туш болгон укук коргоочулар колдоно алышат, андан сырткары мындай активист, укук коргоочуларга Front Line Defenders  убактылуу жай алмаштыруу боюнча жардам берүү менен ишмердүүлүгүн жүргүзүп келет. Front Line Defenders эксперттери физикалык жана санариптик коопсуздук боюнча тренингдерди өткөрүшөт, жана ошондой эле,  уюм коркунучтуу кырдаалга туш болгон укук коргоочулардын окуяларын жарыялап, өнөктөштүктөрдү жүзөгө ашырышат.Ал эми эл аралык деңгээлде,анын ичинде ЕБ, БУУну деңгээлинде активисттерди жактап, аймактар ​​аралык механизмдерди колдонуу менен өз миссиясын аткаруу үчүн өкмөттөр менен өз ара тыгыз иш алып барат.
