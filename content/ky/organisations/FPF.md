---
name: Freedom of the Press Foundation
website: https://freedom.press
logo: Freedom_of_the_Press_Foundation.png
languages: English
services: in_person_training, org_security, assessment, secure_comms, device_security, browsing, account, harassment, advocacy
beneficiaries: journalists, cso
hours: Иш күндөрү убактысында дүйшөмбүдөн жумага чейин, АКШнын чыгыш убакыт алкагы
response_time: 1 күн
contact_methods: web_form, email, pgp, signal, telegram
web_form: https://freedom.press/training/request-training/
email: training@freedom.press
pgp: 0x83F347CEC0095C15EBE7A8A5DC84CA3789C17673
signal: +1 (337) 401-4082
initial_intake: yes
---

 Басма сөз эркиндиги фонду (Freedom of the Press Foundation,FPF) АКШда катталган коммерциялык эмес уюм (501(c)3). FPF 21-кылымдагы журналисттик иликтөөлөрдү колдойт жана коргойт. Журналисттер өздөрүн, уюмдарын жана маалымат булактарын жакшыраак коргой алышы үчүн, FPF чоң медиа борборлордон баштап, жеке журналисттерге чейин коопсуздук жана купуялык темалары боюнча тренинг өткөрүү менен ишмердүүлүгүн жүргүзүп келет. 