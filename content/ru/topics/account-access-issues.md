---
layout: page
title: "Проблема с доступом к аккаунту"
author: RaReNet
language: ru
summary: "Проблемы с доступом к email, аккаунту в соцсети или на каком-то сайте? В аккаунте происходят какие-то действия без вашего ведома? Есть много способов решить эту проблему"
date: 2023-05
permalink: /ru/topics/account-access-issues/
parent: /ru/
---

# У меня проблема с доступом к аккаунту

Гражданские активисты широко используют для связи социальные сети и прочие виды коммуникаций. И не только для связи, но и для обмена знаниями и продвижения собственных интересов. Неудивительно, что эти аккаунты часто становятся мишенями. Злоумышленники вредят аккаунтам, а значит, самим активистам и тем, с кем они общаются.

В этом руководстве мы попытаемся помочь, если вы потеряли доступ к своему аккаунту из-за таких злоумышленников.

Вот вопросы, которые позволят лучше понять суть вашей проблемы и подыскать решения.

## Workflow

### Password-Typo

> Иногда человек не может зайти в аккаунт, потому что неправильно вводит пароль, или забыл переключить раскладку клавиатуры, или у него просто включён CapsLock.
>
> Попробуйте набрать логин и пароль в текстовом редакторе. Скопируйте и вставьте их из редактора в форму на сайте. Убедитесь, что раскладка клавиатуры правильная, а CapsLock выключен.

Это помогло получить доступ к аккаунту?

- [Да](#resolved_end)
- [Нет](#account-disabled)

### account-disabled

> Бывает, что войти в аккаунт не получается, потому что он был заблокирован или отключён самим сервисом за нарушения правил. Такое может случиться, если на аккаунт поступают многочисленные жалобы. Кроме того, процедура подачи жалобы и механизм техподдержки сами по себе могут стать объектами атаки сторонников онлайновой цензуры.
>
> Если вы получили сообщение о том, что ваш аккаунт заблокирован, ограничен или отключён, и вы считаете это ошибкой, используйте процедуру обжалования по ссылке в сообщении. Вот ссылки на то, как обжаловать решения о блокировке:
>
> - [Facebook](https://www.facebook.com/help/185747581553788)
> - [Instagram](https://help.instagram.com/366993040048856?locale=ru_RU)
> - [Twitter](https://help.twitter.com/ru/forms/account-access/appeals/redirect)
> - [Youtube](https://support.google.com/youtube/answer/2802168?hl=ru)

Это помогло войти в аккаунт?

- [Да](#resolved_end)
- [Нет](#what-type-of-account-or-service)

### what-type-of-account-or-service

С каким аккаунтом проблема?

- [Facebook](#Facebook)
- [Страница Facebook](#Facebook-Page)
- [Twitter](#Twitter)
- [Google/Gmail](#Google)
- [Yahoo](#Yahoo)
- [Hotmail/Outlook/Live](#Hotmail)
- [ProtonMail](#ProtonMail)
- [Instagram](#Instagram)
- [TikTok](#Tiktok)
  <!--- - [AddOtherServiceLink](#service-Name) -->

### Facebook-Page

Есть ли у страницы другие администраторы?

- [Да](#Other-admins-exist)
- [Нет](#Facebook-Page-recovery-form)

### Other-admins-exist

У других администраторов та же проблема?

- [Да](#Facebook-Page-recovery-form)
- [Нет](#Other-admin-can-help)

### Other-admin-can-help

> Пожалуйста, попросите других администраторов добавить вас к списку администраторов.

Проблема решена?

- [Да](#Fb-Page_end)
- [Нет](#account_end)

### Facebook-Page-recovery-form

> Пожалуйста, войдите в Facebook и используйте [форму Facebook для восстановления страницы](https://www.facebook.com/help/contact/164405897002583). Если не получается войти в аккаунт Facebook, пожалуйста, [пройдите следующие шаги](#Facebook) для восстановления доступа к Facebook.
>
> Получение ответа может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Процедура восстановления сработала?

- [Да](#resolved_end)
- [Нет](#account_end)

<!---=========================================================
//GoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogleGoogle
=========================================================-->

### Google

У вас есть доступ к связанному с аккаунтом адресу email или номеру телефона?

- [Да](#I-have-access-to-recovery-email-google)
- [Нет](#Recovery-Form-google)

### I-have-access-to-recovery-email-google

> Проверьте ящик почтового адреса для восстановления аккаунта и SMS.
>
> Когда проверяете электронную почту, не забывайте о риске фишинга. Уверены в том, что сообщение не поддельное? Если не уверены, пожалуйста, перейдите к разделу о [подозрительных сообщениях](../../../suspicious-messages/).

 Вы получили электронное письмо или SMS о критической проблеме с безопасностью от Google?

- [Да](#Email-received-google)
- [Нет](#Recovery-Form-google)

### Email-received-google

Сначала убедитесь, что это не мошенники. Если всё в порядке, обратите внимание: в сообщении есть гиперссылка для восстановления доступа к аккаунту?

- [Да](#Recovery-Link-Found-google)
- [Нет](#Recovery-Form-google)

### Recovery-Link-Found-google

> Пожалуйста, используйте ссылку для восстановления доступа к аккаунту. При этом обратите внимание на то, что вы переходите на страницу сайта "google.com", а не какого-то другого.

Получилось восстановить доступ к аккаунту?

- [Да](#resolved_end)
- [Нет](#Recovery-Form-google)

### Recovery-Form-google

> Пожалуйста, попробуйте следовать инструкциям ["Как восстановить доступ к Gmail или аккаунту Google"](https://support.google.com/accounts/answer/7682439?hl=ru).
>
> Пожалуйста, обратите внимание: получение ответа может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Процедура восстановления сработала?

- [Да](#resolved_end)
- [Нет](#account_end)

<!---=========================================================
//YahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahooYahoo
=========================================================-->

### Yahoo

У вас есть доступ к связанному с аккаунтом адресу email или номеру телефона?

- [Да](#I-have-access-to-recovery-email-yahoo)
- [Нет](#Recovery-Form-Yahoo)

### I-have-access-to-recovery-email-yahoo

> Проверьте ящик почтового адреса для восстановления аккаунта, чтобы узнать, пришло ли сообщение от Yahoo о смене пароля.
>
> Когда проверяете электронную почту, не забывайте о риске фишинга. Уверены в том, что сообщение не поддельное? Если не уверены, пожалуйста, перейдите к разделу о [подозрительных сообщениях](../../../suspicious-messages/).

Вы получили письмо от Yahoo о смене пароля для вашего аккаунта?

- [Да](#Email-received-yahoo)
- [Нет](#Recovery-Form-Yahoo)

### Email-received-yahoo

Сначала убедитесь, что это не мошенники. Если всё в порядке, обратите внимание: в сообщении есть гиперссылка для восстановления доступа к аккаунту?

- [Да](#Recovery-Link-Found-Yahoo)
- [Нет](#Recovery-Form-Yahoo)

### Recovery-Link-Found-Yahoo

> Пожалуйста, используйте ссылку для восстановления доступа к аккаунту.

Получилось восстановить доступ к аккаунту?

- [Да](#resolved_end)
- [Нет](#Recovery-Form-Yahoo)

### Recovery-Form-Yahoo

> Чтобы восстановить доступ к аккаунту, пожалуйста, следуйте инструкциям ["Исправьте проблемы с доступом к вашему аккаунту Yahoo"](https://help.yahoo.com/kb/account/fix-problems-signing-yahoo-account-sln2051.html?impressions=true).
>
> Пожалуйста, обратите внимание: получение ответа может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Процедура восстановления сработала?

- [Да](#resolved_end)
- [Нет](#account_end)

<!---=========================================================
TwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitterTwitter
//========================================================= -->

### Twitter

У вас есть доступ к связанному с аккаунтом адресу email или номеру телефона?

- [Да](#I-have-access-to-recovery-email-Twitter)
- [Нет](#Recovery-Form-Twitter)

### I-have-access-to-recovery-email-Twitter

> Проверьте ящик почтового адреса для восстановления аккаунта, чтобы узнать, пришло ли сообщение от Twitter о смене пароля.
>
> Когда проверяете электронную почту, не забывайте о риске фишинга. Уверены в том, что сообщение не поддельное? Если не уверены, пожалуйста, перейдите к разделу о [подозрительных сообщениях](../../../suspicious-messages/).

Вы получили письмо от Twitter о смене пароля для вашего аккаунта?

- [Да](#Email-received-Twitter)
- [Нет](#Recovery-Form-Twitter)

### Email-received-Twitter

Сначала убедитесь, что это не мошенники. Если всё в порядке, обратите внимание: в сообщении есть гиперссылка для восстановления доступа к аккаунту?

- [Да](#Recovery-Link-Found-Twitter)
- [Нет](#Recovery-Form-Twitter)

### Recovery-Link-Found-Twitter

> Пожалуйста, используйте ссылку для восстановления доступа к аккаунту.

Получилось восстановить доступ к аккаунту?

- [Да](#resolved_end)
- [Нет](#Recovery-Form-Twitter)

### Recovery-Form-Twitter

> Если считаете, что ваш аккаунт Twitter скомпрометирован, попробуйте рекомендации ["Помощь при взломе учетной записи"](https://help.twitter.com/ru/safety-and-security/twitter-account-compromised).
>
> Если аккаунт не скомпрометирован (или у вас другие проблемы с доступом к аккаунту), запросите [помощь в восстановлении учетной записи](https://help.twitter.com/forms/restore).
>
> Пожалуйста, обратите внимание: получение ответа может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Процедура восстановления сработала?

- [Да](#resolved_end)
- [Нет](#account_end)

<!---=========================================================
//Protonmail
//========================================================= -->

### ProtonMail

> Чтобы восстановить доступ к аккаунту, пожалуйста, следуйте [инструкциям для сброса пароля](https://protonmail.com/support/knowledge-base/reset-password/).
>
> Если вы сбросили пароль, вы не сможете прочитать существующие сообщения и увидеть контакты. Они зашифрованы с помощью пароля и шифровального ключа. Старые данные можно восстановить, если у вас есть доступ к файлу восстановления или фразе восстановления. Подробнее см. [здесь](https://proton.me/support/recover-encrypted-messages-files).

Эти советы помогли?

- [Да](#resolved_end)
- [Нет](#account_end)

<!---==================================================================
//MicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlookMicorsoftHotmailLiveOutlook
//================================================================== -->

### Hotmail

У вас есть доступ к связанному с аккаунтом адресу email или номеру телефона?

- [Да](#I-have-access-to-recovery-email-Hotmail)
- [Нет](#Recovery-Form-Hotmail)

### I-have-access-to-recovery-email-Hotmail

> Проверьте ящик почтового адреса для восстановления аккаунта, чтобы узнать, пришло ли сообщение от Microsoft о смене пароля.
>
> Когда проверяете электронную почту, не забывайте о риске фишинга. Уверены в том, что сообщение не поддельное? Если не уверены, пожалуйста, перейдите к разделу о [подозрительных сообщениях](../../../suspicious-messages/).

Вы получили письмо от Microsoft о смене пароля для аккаунта Microsoft?

- [Да](#Email-received-Hotmail)
- [Нет](#Recovery-Form-Hotmail)

### Email-received-Hotmail

Сначала убедитесь, что это не мошенники. Если всё в порядке, обратите внимание: в сообщении есть гиперссылка для восстановления доступа к аккаунту?

- [Да](#Recovery-Link-Found-Hotmail)
- [Нет](#Recovery-Form-Hotmail)

### Recovery-Link-Found-Hotmail

> Пожалуйста, используйте ссылку для создания нового пароля и восстановления доступа к аккаунту.

Получилось восстановить доступ к аккаунту?

- [Да](#resolved_end)
- [Нет](#Recovery-Form-Hotmail)

### Recovery-Form-Hotmail

> Пожалуйста, попробуйте [помощника по входу в учетную запись Microsoft](https://go.microsoft.com/fwlink/?linkid=2214157). Следуйте его советам. Вам понадобится, в частности, указать аккаунт, который вы пытаетесь восстановить, и ответить на вопросы о данных, которые можно восстановить.
>
> Пожалуйста, обратите внимание: получение ответа на запросы, полученные через веб-формы, может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Процедура восстановления сработала?

- [Да](#resolved_end)
- [Нет](#account_end)

<!---==================================================================
//FacebookMetaFacebookMetaFacebookMetaFacebookMetaFacebookMetaFacebook
//================================================================== -->


### Facebook

У вас есть доступ к связанному с аккаунтом адресу email или номеру телефона?

- [Да](#I-have-access-to-recovery-email-Facebook)
- [Нет](#Recovery-Form-Facebook)

### I-have-access-to-recovery-email-Facebook

> Проверьте ящик почтового адреса для восстановления аккаунта, чтобы узнать, пришло ли сообщение от Facebook о смене пароля.
>
> Когда проверяете электронную почту, не забывайте о риске фишинга. Уверены в том, что сообщение не поддельное? Если не уверены, пожалуйста, перейдите к разделу о [подозрительных сообщениях](../../../suspicious-messages/).

Вы получали от Facebook сообщение о смене пароля?

- [Да](#Email-received-Facebook)
- [Нет](#Recovery-Form-Facebook)

### Email-received-Facebook

Сначала убедитесь, что это не мошенники. Если всё в порядке, обратите внимание: в сообщении есть строчка "Это не я" с гиперссылкой?

- [Да](#Recovery-Link-Found-Facebook)
- [Нет](#Recovery-Form-Facebook)

### Recovery-Link-Found-Facebook

> Пожалуйста, используйте ссылку "Это не я" для восстановления доступа к аккаунту.

Удалось восстановить доступ к аккаунту по этой ссылке?

- [Да](#resolved_end)
- [Нет](#Recovery-Form-Facebook)

### Recovery-Form-Facebook

> Пожалуйста, попробуйте [форму для восстановления аккаунта](https://www.facebook.com/login/identify).
>
> Пожалуйста, обратите внимание: получение ответа может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Процедура восстановления сработала?

- [Да](#resolved_end)
- [Нет](#account_end)

<!--- ==================================================================
InstagramInstagramInstagramInstagramInstagramInstagramInstagramInstagram
//================================================================== not yet tested-->

### Instagram

У вас есть доступ к связанному с аккаунтом адресу email или номеру телефона?

- [Да](#I-have-access-to-recovery-email-Instagram)
- [Нет](#Recovery-Form-Instagram)

### I-have-access-to-recovery-email-Instagram

> Проверьте ящик почтового адреса для восстановления аккаунта, чтобы узнать, пришло ли сообщение от Instagram о смене пароля.
>
> Когда проверяете электронную почту, не забывайте о риске фишинга. Уверены в том, что сообщение не поддельное? Если не уверены, пожалуйста, перейдите к разделу о [подозрительных сообщениях](../../../suspicious-messages/).

Вы получали от Instagram сообщение о смене пароля?

- [Да](#Email-received-Instagram)
- [Нет](#Recovery-Form-Instagram)

### Email-received-Instagram

Сначала убедитесь, что это не мошенники. Если всё в порядке, обратите внимание: в сообщении есть фраза о защите вашего аккаунта с гиперссылкой?

- [Да](#Recovery-Link-Found-Instagram)
- [Нет](#Recovery-Form-Instagram)

### Recovery-Link-Found-Instagram

> Используйте эту гиперссылку для восстановления доступа к аккаунту.

Удалось восстановить доступ к аккаунту?

- [Да](#resolved_end)
- [Нет](#Recovery-Form-Instagram)

### Recovery-Form-Instagram

> Для восстановления доступа к аккаунту, пожалуйста, попробуйте инструкции ["Что делать, если вам кажется, что ваш аккаунт Instagram взломан?"](https://help.instagram.com/149494825257596?helpref=search&sr=1&query=hacked).
>
> Пожалуйста, обратите внимание: получение ответа на запрос через веб-форму может потребовать времени. Сохраните эту страницу в закладках браузера и зайдите на неё через несколько дней.

Процедура восстановления сработала?

- [Да](#resolved_end)
- [Нет](#account_end)

<!--- ==================================================================
TiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktokTiktok
//================================================================== -->

### Tiktok

У вас есть доступ к связанному с аккаунтом адресу email или номеру телефона?

- [Да](#I-have-access-to-recovery-email-Tiktok)
- [Нет](#Recovery-Form-Tiktok)

### I-have-access-to-recovery-email-Tiktok

> Если у вас есть доступ к email для восстановления пароля, попробуйте [восстановить пароль](https://www.tiktok.com/login/email/forget-password).

Удалось восстановить доступ к аккаунту?

- [Да](#resolved_end)
- [Нет](#Recovery-Form-Tiktok)

### Recovery-Form-Tiktok

> Для восстановления доступа к аккаунту, пожалуйста, следуйте инструкциям ["Мой аккаунт взломали"](https://support.tiktok.com/ru/log-in-troubleshoot/log-in/my-account-has-been-hacked).

Процедура восстановления сработала?

- [Да](#resolved_end)
- [Нет](#account_end)

### Fb-Page_end

Ваша проблема решена? Прекрасно! Пожалуйста, прочтите эти рекомендации. Они помогут минимизировать риск потери доступа к вашей странице в будущем:

- Включите двухфакторную аутентификацию для всех администраторов страницы.
- Предоставляйте права администратора только тем людям, который оперативно выходят на связь.
- Есть кто-то, кому вы доверяете? Возможно, этот человек сможет тоже стать админом? Не забудьте включить двухфакторную аутентификацию и ему (ей).
- Время от времени проверяйте права доступа и разрешения аккаунтов на странице. Давайте кому-либо доступ по принципу необходимого минимума.

### account_end

Если все перечисленные меры не помогли вам восстановить доступ к аккаунту, можете обратиться к организациям, предлагающим помощь в таких вопросах:

:[](organisations?services=account)

### resolved_end

Надеемся, это руководство было вам полезно. Пожалуйста, поделитесь своим мнением [по email](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com).

### final_tips

Пожалуйста, прочтите эти рекомендации, чтобы снизить вероятность потери доступа к аккаунтам в будущем:

- Для всех аккаунтов, где это возможно, есть смысл включать двухфакторную аутентификацию.
- Никогда не используйте один и тот же пароль для двух и более аккаунтов. Если всё-таки использовали, смените пароли как можно скорее.
- Парольный менеджер поможет создавать и запоминать уникальные, надёжные пароли для всех ваших аккаунтов.
- Будьте осторожны, используя открытые сети wifi, которым нет оснований доверять. Возможно, лучше работать в них через VPN или Tor.

#### resources

- [Социальные сети: 22 правила](https://safe.roskomsvoboda.org/socialnetworks/)
- [Двухфакторная аутентификация](https://safe.roskomsvoboda.org/twofactor/)
- [Материалы службы поддержки Access Now по корпоративным парольным менеджерам](https://accessnowhelpline.gitlab.io/community-documentation/295-Password_managers.html)
- [Security Self-Defense, "Самозащита в социальных сетях"](https://ssd.eff.org/ru/module/%D1%81%D0%B0%D0%BC%D0%BE%D0%B7%D0%B0%D1%89%D0%B8%D1%82%D0%B0-%D0%B2-%D1%81%D0%BE%D1%86%D0%B8%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D1%85-%D1%81%D0%B5%D1%82%D1%8F%D1%85)
- [Security Self-Defense, "Создание надёжных паролей""](https://ssd.eff.org/ru/module/%D1%81%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5-%D0%BD%D0%B0%D0%B4%D1%91%D0%B6%D0%BD%D1%8B%D1%85-%D0%BF%D0%B0%D1%80%D0%BE%D0%BB%D0%B5%D0%B9)

<!--- Edit the following to add another service recovery workflow:
#### service-name

Do you have access to the connected recovery email/mobile?

- [Yes](#I-have-access-to-recovery-email-google)
- [No](#Recovery-Form-google)

### I-have-access-to-recovery-email-google

Check if you received a "[Password Change Email Subject]" email from service_name. Did you receive it?

When checking for emails always be wary of phishing attempts. If you are unsure of the legitimacy of a message, please review the [Suspicious Messages Workflow](https://digitalfirstaid.org/en/topics/suspicious-messages/)

- [Yes](#Email-received-service-name)
- [No](#Recovery-Form-service-name

### Email-received-service-name

> Please check if there is a "recover your account" link. Is it there?

- [Yes](#Recovery-Link-Found-service-name)
- [No](#Recovery-Form-service-name)

### Recovery-Link-Found-service-name

> Please use the [Recovery Link Description](URL) link to recover your account.

Were you able to recover your account with "[Recovery Link Description]" link?

- [Yes](#resolved_end)
- [No](#Recovery-Form-service-name)

### Recovery-Form-service-name

> Please try this recovery form to recover this account: [Link to the standard recovery form].
>
> Please note that it might take some time to receive a response to your requests. Save this page in your bookmarks and come back to this workflow in a few days.

Has the recovery procedure worked?

- [Yes](#resolved_end)
- [No](#account_end)

-->
