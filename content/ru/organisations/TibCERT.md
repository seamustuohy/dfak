---
name: TibCERT - Tibetan Computer Emergency Readiness Team
website: https://tibcert.org/
logo: tibcert-logo.png
languages: English, Tibetan
services: in_person_training, org_security, assessment, digital_support, secure_comms, device_security, advocacy
beneficiaries: journalists, hrds, activists, cso
hours: понедельник — пятница, GMT+5.30
response_time: 24 часа в нерабочее время, 2 часа в рабочее время
contact_methods: email, pgp, phone, whatsapp, signal, mail
email: info@tibcert.org
pgp: 0xF34C6C41A569F186
pgp_key_fingerprint: D1C5 8DE6 E45B 4DD7 92EF  F970 F34C 6C41 A569 F186
mail: Dhangshar House, Temple Road, McleodGanj, Distt. Kangra, HP - 176219 - India
phone: "Mobile: +919816170738 Office: +911892292177"
whatsapp: +919816170738
signal: +919816170738
initial_intake: yes
---

Тибетская компьютерная команда оперативного реагирования (TibCERT) стремится стать той коалиционной структурой, которая будет снижать и обходить онлайновые угрозы в тибетском сообществе, повышать технический потенциал тибетцев по противостоянию угрозам в диаспорах и слежке/цензуре в самом Тибете, а в конечном счёте — добиваться большей свободы и безопасности для сообщества Тибета в целом.

Миссия TibCERT включает:

- Создание и поддержку платформы для долгосрочного сотрудничества между участниками сообщества Тибета по вопросам цифровой безопасности.
- Укрепление связей и сотрудничество между тибетцами и исследователями в области вредоносного кода и цифровой безопасности по всему миру ради взаимной пользы.
- Развитие доступных тибетцам ресурсов для предотвращения и защиты от онлайновых атак, регулярное публикование информации и рекомендаций об угрозах сообществу.
- Помощь тибетцам в самом Тибете для обхода цензуры и слежки путём предоставления регулярной и подробной информации и аналитики, а также перспективных решений.