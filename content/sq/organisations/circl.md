---
name: Qendra e Reagimit ndaj Incidenteve kompjuterike në Luksemburg (Computer Incident Response Center Luxembourg - CIRCL)
website: https://circl.lu
logo: circl.png
languages: English, Deutsch, Français, Luxembourgish
services: org_security, vulnerabilities_malware, forensic
beneficiaries: activists, lgbti, women, youth, cso
hours: office hours, UTC+2
response_time: 4 hours
contact_methods: web_form, email, pgp, mail, phone
web_form: https://www.circl.lu/contact/
email: info@circl.lu
pgp_key_fingerprint: CA57 2205 C002 4E06 BA70 BE89 EAAD CFFC 22BD 4CD5
phone: +352 247 88444
mail: 16, bd d'Avranches, L-1160 Luxembourg, Grand-Duchy of Luxembourg
initial_intake: yes
---

CIRCL është CERT-i për sektorin privat, komunat dhe subjektet joqeveritare në Luksemburg.

CIRCL ofron një pikë kontakti të besueshme për çdo përdorues, kompani dhe organizatë me seli në Luksemburg, për trajtimin e sulmeve dhe incidenteve. Ekipi i tij i ekspertëve vepron si një ekip zjarrfikës, me aftësinë për të reaguar menjëherë dhe me efikasitet sa herë ka dyshime, zbulohen ose ndodhin kërcënime.

Qëllimi i CIRCL është të mbledhë, shqyrtojë, raportojë dhe të përgjigjet ndaj kërcënimeve kibernetike në mënyrë sistematike dhe të shpejtë.

