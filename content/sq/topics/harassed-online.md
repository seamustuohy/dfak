---
layout: page
title: "A jeni në shënjestër të ngacmimeve në internet?"
author: Flo Pagano, Natalia Krapiva
language: sq
summary: "A jeni në shënjestër të ngacmimeve në internet?"
date: 2023-04
permalink: /sq/topics/harassed-online/
parent: Home
---

# A jeni në shënjestër të ngacmimeve në internet?

Interneti, dhe platformat e mediave sociale në veçanti, janë bërë një hapësirë kritike për anëtarët dhe organizatat e shoqërisë civile, veçanërisht për gratë, personat LGBTQIA dhe grupe të tjera të margjinalizuara, për t'u shprehur dhe për të bërë zërin e tyre të dëgjohet. Por në të njëjtën kohë, ato janë bërë edhe hapësira ku këto grupe synohen lehtësisht për shprehjen e pikëpamjeve të tyre. Dhuna dhe abuzimi në internet u mohon grave, personave LGBTQIA dhe shumë njerëzve të tjerë të paprivilegjuar të drejtën për t'u shprehur në mënyrë të barabartë, të lirë dhe pa frikë.

Dhuna dhe abuzimi në internet kanë forma të ndryshme dhe subjektet keqdashëse shpesh mund të mbështeten në mosndëshkimin për shkak të mungesës së ligjeve që i mbrojnë viktimat e ngacmimit në shumë vende, dhe kryesisht për shkak se nuk ka strategji universale të mbrojtjes: ato duhet të përshtaten në mënyrë kreative në varësi të llojit të sulmit që kryhet. Prandaj është e rëndësishme të identifikoni tipologjinë e sulmit që ju synon për të vendosur se çfarë hapash mund të ndërmerren.

Ky seksion i Veglërisë së Ndihmës së Parë Digjitale do t'ju njoftojë me disa hapa themelorë për të kuptuar se çfarë lloj sulmi po përjetoni dhe për të planifikuar se si të mbroheni nga ai, ose të gjeni një qendër ndihmëse të sigurisë digjitale që mund t'ju mbështesë.

Nëse jeni në shënjestër të ngacmimeve në internet, ju sugjerojmë të lexoni rekomandimet për atë se [si të kujdeseni për veten](/../../self-care) dhe [si të dokumentoni sulmet](/../../ dokumentation), dhe më pas ndiqni këtë pyetësor për të identifikuar natyrën e problemit tuaj dhe për të gjetur zgjidhjet e mundshme.

## Workflow

### physical-wellbeing

A keni frikë për integritetin fizik apo mirëqenien tuaj?

- [Po](#physical-risk_end)
- [Jo](#no-physical-risk)

### location

A duket se sulmuesi e di vendndodhjen tuaj fizike?

- [Po](#location_tips)
- [Jo](#device)

### location_tips

> Kontrolloni postimet tuaja të fundit në mediat sociale: a përfshijnë ato vendndodhjen tuaj të saktë? Nëse po, çaktivizoni qasjen GPS për aplikacionet e mediave sociale dhe shërbimet e tjera në telefonin tuaj në mënyrë që kur të postoni përditësimet tuaja, vendndodhja juaj të mos shfaqet.
>
> Kontrolloni fotot që keni postuar për veten tuaj në internet: a përfshijnë ato detaje të vendeve që dallohen qartë dhe mund të tregojnë qartë se ku jeni? Për të mbrojtur veten nga përndjekësit e mundshëm, është më mirë të mos tregoni vendndodhjen tuaj të saktë kur postoni foto ose video të vetes.
>
> Si një masë paraprake shtesë, është gjithashtu një ide e mirë të mbani GPS të fikur gjatë gjithë kohës – përveç vetëm për një kohë të shkurtër kur vërtet duhet të gjeni pozicionin tuaj në hartë.

A mund t'ju ketë ndjekur sulmuesi përmes informacionit që keni publikuar në internet? Apo mendoni ende se sulmuesi e di vendndodhjen tuaj fizike përmes mjeteve të tjera?

 - [Jo, mendoj se i kam zgjidhur problemet e mia](#resolved_end)
 - [Po, ende mendoj se sulmuesi e di se ku jam](#device)
 - [Jo, por kam probleme të tjera](#account)

### device

A mendoni se sulmuesi ka hyrë ose po futet në pajisjen tuaj?

 - [Po](#device-compromised)
 - [Jo](#account)


### device-compromised

> Ndryshoni fjalëkalimin për të hyrë në pajisjen tuaj me një fjalëkalim unik, të gjatë dhe të ndërlikuar:
>
> - [Mac OS](https://support.apple.com/en-us/HT202860)
> - [Windows](https://support.microsoft.com/en-us/help/4490115/windows-change-or-reset-your-password)
> - [iOS - Apple ID](https://support.apple.com/en-us/HT201355)
> - [Android](https://support.google.com/accounts/answer/41078?co=GENIE.Platform%3DAndroid&hl=en)

A keni ende ndjenjën se sulmuesi mund të jetë duke kontrolluar pajisjen tuaj?

 - [Jo, mendoj se i kam zgjidhur problemet e mia](#resolved_end)
 - [Jo, por kam probleme të tjera](#account)
 - [Jo](#info_stalkerware)

### info_stalkerware

> [Stalkerware](https://en.wikipedia.org/wiki/Stalkerware) është çdo softuer që përdoret për të monitoruar aktivitetin ose vendndodhjen e një personi me qëllimin e ndjekjes ose kontrollit të tyre.

Nëse mendoni se dikush mund të jetë duke ju spiunuar përmes një aplikacioni që ai ka instaluar në pajisjen tuaj celulare, [kjo sekuencë veprimesh nga Veglëria e Ndihmës së Parë Digjitale do t'ju ndihmojë të vendosni nëse pajisja juaj është e infektuar me softuer keqdashës (malware) dhe si të ndërmerrni hapa për ta pastruar atë](../../../device-acting-suspiciously).

Keni nevojë për më shumë ndihmë për të kuptuar sulmin që po përjetoni?

 - [Jo, mendoj se i kam zgjidhur problemet e mia](#resolved_end)
 - [Po, kam ende disa probleme që do të doja t'i zgjidhja](#account)

### account

> Pavarësisht nëse dikush ka qasje në pajisjen tuaj apo jo, ekziston mundësia që ai të ketë hyrë në llogaritë tuaja në internet duke i hakuar ato ose sepse e dinte ose e kishte thyer fjalëkalimin tuaj.
>
> Nëse dikush ka qasje në një ose më shumë nga llogaritë tuaja në internet, ai mund të lexojë mesazhet tuaja private, të identifikojë kontaktet tuaja, të publikojë postimet, fotot ose videot tuaja private ose të fillojë t'ju imitojë.
>
> Rishikoni aktivitetin e llogarive tuaja në internet dhe kutinë mbërritëse të e-mailit (duke përfshirë folderët Sent dhe Trash) për aktivitete të mundshme të dyshimta.

A keni vënë re postime ose mesazhe që po zhduken, ose aktivitete të tjera që ju japin arsye të mirë për të menduar se llogaria juaj mund të jetë komprometuar?

 - [Po](#account-compromised)
 - [Jo](#private-contact)


### change passwords

> Provoni të ndryshoni fjalëkalimin në secilën prej llogarive tuaja në internet me një fjalëkalim të fortë dhe unik.
>
> Mund të mësoni më shumë se si të krijoni dhe menaxhoni fjalëkalimet në këtë [Udhëzues për vetë-mbrojtja nën mbikëqyrje](https://ssd.eff.org/module/creating-strong-passwords).
>
> Është gjithashtu një ide shumë e mirë të shtoni një shtresë të dytë mbrojtjeje në llogaritë tuaja në internet duke aktivizuar vërtetimin me 2 faktorë (2FA) kurdo që të jetë e mundur.
>
> Zbuloni se si të aktivizoni vërtetimin me 2 faktorë në këtë [Udhëzues për vetë-mbrojtja nën mbikëqyrje](https://ssd.eff.org/module/how-enable-two-factor-authentication).

A keni ende ndjenjën se dikush mund të ketë qasje në llogarinë tuaj?

 - [Po](#hacked-account)
 - [Jo](#private-contact)


### hacked-account

> Nëse nuk jeni në gjendje të hyni në llogarinë tuaj, ekziston mundësia që llogaria juaj të jetë hakuar dhe hakeri të ketë ndryshuar fjalëkalimin tuaj.

Nëse mendoni se llogaria juaj mund të jetë hakuar, provoni të ndiqni sekuencën e veprimeve nga Veglëria e Ndihmës së Parë Digjitale që mund t'ju ndihmojë të zgjidhni problemet me hyrjen në llogarinë tuaj.

 - [Më dërgo në sekuencën e veprimeve për të zgjidhur problemet e hyrjes në llogari](../../../account-access-issues)
 - [Mendoj se i kam zgjidhur problemet e mia](#resolved_end)
 - [Llogaria ime është në rregull, por kam probleme të tjera](#private-contact)


### private-contact

A po merrni telefonata ose mesazhe të padëshiruara në një aplikacion mesazhesh?

 - [Po](#change_contact)
 - [Jo](#threats)

### change_contact

> Nëse merrni telefonata, mesazhe SMS ose mesazhe të padëshiruara në aplikacione që lidhen me numrin tuaj të telefonit, e-mailin tuaj ose informacione të tjera private kontakti, mund të provoni të ndryshoni numrin tuaj, kartën SIM, e-mailin ose informacione të tjera kontakti të lidhura me llogarinë.
>
> Ju gjithashtu duhet të konsideroni raportimin dhe bllokimin e mesazheve dhe të llogarisë së lidhur në platformën përkatëse.

A i keni ndërprerë me sukses telefonatat ose mesazhet e padëshiruara?

 - [Po](#legal)
 - [Jo, kam probleme të tjera](#threats)
 - [Jo, kam nevojë për ndihmë](#harassment_end)

### threats

A jeni duke u shantazhuar ose duke marrë kërcënime përmes e-maileve ose mesazheve në një llogari të mediave sociale?

 - [Po](#threats_tips)
 - [Jo](#impersonation)

### threats_tips

> Nëse merrni mesazhe që përmbajnë kërcënime, duke përfshirë kërcënime për dhunë fizike ose seksuale, ose shantazh, duhet të dokumentoni atë që ka ndodhur sa më shumë që të jetë e mundur, duke përfshirë regjistrimin e lidhjeve dhe pamjeve të ekranit, raportimin e personit në platformën përkatëse ose ofruesin e shërbimit dhe bllokimin e sulmuesit.

A i keni ndalur me sukses kërcënimet?

 - [Po](#legal)
 - [Po, por kam edhe probleme të tjera](#impersonation)
 - [Jo, kam nevojë për mbështetje ligjore](#legal-warning)

### impersonation

> Një formë tjetër e ngacmimit të cilës mund t'i jeni nënshtruar është imitimi (shtirja nga dikush sikur jeni ju).
>
> Për shembull, dikush mund të ketë krijuar një llogari nën emrin tuaj dhe është duke dërguar mesazhe private ose duke sulmuar publikisht dikë në platformat e mediave sociale, duke përhapur dezinformata ose gjuhë të urrejtjes, ose duke vepruar në mënyra të tjera për t'ju bërë juve, organizatën tuaj ose të tjerët afër jush të cenueshëm ndaj rreziqeve të reputacionit dhe të sigurisë.
>
> Nëse mendoni se dikush po ju imiton juve ose organizatën tuaj, mund të ndiqni këtë sekuencë veprimesh nga Veglëria e Ndihmës së Parë Digjitale, duke ju udhëhequr përmes formave të ndryshme të imitimit për të identifikuar strategjinë më të mirë të mundshme për të përballuar problemin tuaj.

Çfarë do të dëshironit të bënit?

 - [Dikuhs është duke më imituar dhe do të doja të gjeja një zgjidhje](../../../impersonated)
 - [Po përballem me një formë tjetër ngacmimi](#defamation)

### defamation

A po përpiqet dikush të dëmtojë reputacionin tuaj duke përhapur informacione të rreme?

 - [Po](#defamation-yes)
 - [Po përballem me një formë tjetër ngacmimi](#doxing)

### defamation-yes

> Shpifja në përgjithësi përkufizohet si të bësh një deklaratë që dëmton reputacionin e dikujt. Shpifja mund të jetë në formë gojore ose të shkruar. Ligjet në vende të ndryshme mund të ndryshojnë në lidhje me atë se çfarë përbën shpifje për të ndërmarrë veprime ligjore. Për shembull, disa vende kanë ligje që janë shumë mbrojtëse për lirinë e shprehjes, të cilat i lejojnë mediat dhe individët privatë të thonë gjëra për zyrtarët publikë që mund të jenë turpëruese ose të dëmshme për sa kohë që ata besojnë se një informacion i tillë është i vërtetë. Vende të tjera mund t'ju lejojnë më lehtë të padisni të tjerët për përhapjen e informacionit për ju që nuk ju pëlqen.
>
> Nëse dikush po përpiqet të dëmtojë reputacionin tuaj ose të organizatës suaj, ju mund të ndiqni sekuencën e veprimeve nga Veglëria e Ndihmës së Parë Digjitale për shpifjen, duke ju udhëhequr nëpër strategji të ndryshme për të adresuar fushatat e shpifjes.

Çfarë do të dëshironit të bënit?

 - [Do të doja të gjeja një strategji kundër një fushate shpifjeje](../../../defamation)
 - [Po përballem me një formë tjetër ngacmimi](#doxing)

### doxing

A ka publikuar dikush informacion ose material mediatik (fotografi, video) privat për ju pa pëlqimin tuaj?

 - [Po](#doxing-yes)
 - [Jo](#hate-speech)

### doxing-yes

> Nëse dikush ka publikuar informacione private për ju ose po qarkullon video, fotografi ose materiale të tjera mediatike rreth jush, mund të ndiqni sekuencën e veprimeve nga Veglëria e Ndihmës së Parë Digjitale që mund t'ju ndihmojë të kuptoni se çfarë po ndodh dhe si t'i përgjigjeni një sulmi të tillë.

Çfarë do të dëshironit të bënit?

 - [Do të doja të kuptoja se çfarë po ndodh dhe çfarë mund të bëj për këtë](../../../doxing)
 - [Do të doja të merrja mbështetje](#harassment_end)

### hate-speech

A po përhap dikush mesazhe urrejtjeje kundër jush bazuar në atribute si raca, gjinia ose feja?

 - [Po](#one-more-persons)
 - [Jo](#harassment_end)


### one-more-persons

Jeni sulmuar nga një ose më shumë persona?

 - [Jam sulmuar nga një person](#one-person)
 - [Jam sulmuar nga më shumë se një person](#campaign)

### one-person

> Nëse gjuha e urrejtjes vjen nga një person i vetëm, mënyra më e lehtë dhe më e shpejtë për të frenuar sulmin dhe për të parandaluar përdoruesin që të vazhdojë t'ju dërgojë mesazhe urrejtjeje është raportimi dhe bllokimi i tyre. Mos harroni se nëse e bllokoni përdoruesin, nuk mund të hyni në përmbajtjen e tij për ta dokumentuar atë. Përpara bllokimit, lexoni [këshillat tona për dokumentimin e sulmeve digjitale](/../../documentation).
>
> Pavarësisht nëse e dini se kush është ngacmuesi juaj apo jo, është gjithmonë një ide e mirë t'i bllokoni ata në platformat e rrjeteve sociale sa herë që është e mundur.
>
> - [Facebook](https://www.facebook.com/help/168009843260943)
> - [Google](https://support.google.com/accounts/answer/6388749?co=GENIE.Platform%3DDesktop&hl=en)
> - [Instagram](https://help.instagram.com/426700567389543)
> - [TikTok](https://support.tiktok.com/en/using-tiktok/followers-and-following/blocking-the-users)
> - [Twitter](https://help.twitter.com/en/using-twitter/blocking-and-unblocking-accounts)
> - [Whatsapp](https://faq.whatsapp.com/1142481766359885/?cms_platform=android)

A e keni bllokuar në mënyrë efektive ngacmuesin tuaj?

- [Po](#legal)
- [Jo](#campaign)


### legal

> Nëse e dini se kush po ju ngacmon, mund t'i raportoni ata te autoritetet e vendit tuaj nëse këtë e konsideroni të sigurt dhe të përshtatshme në kontekstin tuaj. Çdo vend ka ligje të ndryshme për mbrojtjen e njerëzve nga ngacmimet në internet dhe ju duhet të eksploroni legjislacionin në vendin tuaj ose të kërkoni këshilla ligjore për t'ju ndihmuar të vendosni se çfarë të bëni.
>
> Nëse nuk e dini se kush ju ngacmon, në disa raste, mund të gjurmoni identitetin e sulmuesit përmes një analize eksperte të gjurmëve që mund t’i kenë lënë pas.
>
> Sidoqoftë, nëse po mendoni të ndërmerrni veprime ligjore, do të jetë shumë e rëndësishme të mbani provat për sulmet ndaj të cilave jeni nënshtruar. Prandaj, rekomandohet fuqimisht të ndiqni [rekomandimet në faqen e Veglërisë së Ndihmës së Parë Digjitale për regjistrimin e informacionit mbi sulmet](/../../documentation).

Çfarë do të dëshironit të bënit?

 - [Do të doja të merrja ndihmë ligjore për të paditur sulmuesin tim](#legal_end)
 - [Mendoj se i kam zgjidhur problemet e mia](#resolved_end)


### campaign

> Nëse jeni duke u sulmuar nga më shumë se një person, mund të jeni në shënjestër të një fushate të gjuhës së urrejtjes ose ngacmimit dhe do t'ju duhet të reflektoni se cila është strategjia më e mirë që mund të zbatohet për rastin tuaj.
>
> Për të mësuar rreth të gjitha strategjive të mundshme, lexoni [faqen “Take Back The Tech” mbi strategjitë kundër gjuhës së urrejtjes](https://www.takebackthetech.net/be-safe/hate-speech-strategies).

A keni identifikuar strategjinë më të mirë për ju?

 - [Po](#resolved_end)
 - [Jo, kam nevojë për ndihmë për të zgjidhur më shumë probleme](#harassment_end)
 - [Jo, kam nevojë për ndihmë ligjore](#legal_end)

### harassment_end

> Nëse jeni ende duke u ngacmzuar dhe keni nevojë për një zgjidhje të personalizuar, ju lutemi kontaktoni organizatat e mëposhtme që mund t'ju mbështesin.

:[](organisations?services=harassment)


### physical-risk_end

> Nëse jeni në rrezik fizik dhe keni nevojë për ndihmë të menjëhershme, ju lutemi kontaktoni organizatat e mëposhtme që mund t'ju mbështesin.

:[](organisations?services=physical_security)


### legal_end

> Nëse keni nevojë për mbështetje ligjore, ju lutemi kontaktoni organizatat e mëposhtme që mund t'ju mbështesin.
>
> Nëse mendoni të ndërmerrni veprime ligjore, do të jetë shumë e rëndësishme të mbani prova për sulmet të cilave u jeni nënshtruar. Prandaj, rekomandohet fuqimisht të ndiqni [rekomandimet në faqen e Veglërisë së Ndihmës së Parë Digjitale për regjistrimin e informacionit mbi sulmet](/../../documentation).

:[](organisations?services=legal)

### resolved_end

Shpresojmë se ky udhëzues për zgjidhjen e problemeve ishte i dobishëm. Ju lutemi jepni komente [përmes e-mailit](mailto:incoming+rarenet-dfak-8220223-issue-@incoming.gitlab.com)

### final_tips

- *Dokumentoni ngacmimin.** Është e dobishme të dokumentoni sulmet ose çdo incident tjetër që mund ta dëshmoni: merrni pamje nga ekrani, ruani mesazhet që merrni nga ngacmuesit, etj. Nëse është e mundur, krijoni një ditar për të sistemuar këtë dokumentacion, regjistrimin e datave, orëve, platformave dhe URL-ve, ID-në e përdoruesit , pamjet e ekranit, përshkrimin e asaj që ka ndodhur, etj. Ditarët mund t'ju ndihmojnë të zbuloni modelet dhe indikacionet e mundshme për sulmuesit tuaj të mundshëm. Nëse ndiheni të mbingarkuar, mendoni për dikë që keni besim, i cili mund t'i dokumentojë incidentet për ju për një kohë. Duhet t'i besoni thellë personit që do ta menaxhojë këtë dokumentacion, pasi do t'ju duhet t'ia dorëzoni të dhënat për të hyrë në llogaritë tuaja personale. Përpara se të ndani fjalëkalimin tuaj me këtë person, ndryshojeni atë në diçka pak më ndryshe dhe ndajeni atë përmes një mjeti të sigurt - si p.sh. duke përdorur një mjet me [enkriptim nga skaji-në-skaj](https://www.frontlinedefenders.org/en/resource-publication/guide-secure-group-chat-and-conferencing-tools). Pasi të ndjeni se mund të rifitoni kontrollin e llogarisë, mos harroni të ndryshoni fjalëkalimin tuaj përsëri në një fjalëkalim unik, [të sigurt](https://ssd.eff.org/en/module/creating-strong-passwords), dhe të cilin vetëm ju e dini.

    - Ndiqni [rekomandimet në faqen e Veglërisë së Ndihmës së Parë Digjitale për regjistrimin e informacionit mbi sulmet](/../../documentation) për të ruajtur informacionin në lidhje me sulmin ndaj jush në mënyrën më të mirë të mundshme.

- **Vendosni vërtetimin me 2 faktorë ** në të gjitha llogaritë tuaja. Vërtetimi me 2 faktorë mund të jetë shumë efektiv për të ndaluar dikë që të hyjë në llogaritë tuaja pa lejen tuaj. Nëse mund të zgjidhni, mos e përdorni vërtetimin me 2 faktorë të bazuar në SMS dhe zgjidhni një mundësi tjetër, të bazuar në një aplikacion telefoni ose në një çelës sigurie.

    - Nëse nuk e dini se cila zgjidhje është më e mira për ju, mund të shikoni [infografikun e Access Now "Cili është lloji më i mirë i vërtetimit me shumë faktorë për mua?"](https://www.accessnow.org/cms/assets/uploads/2017/09/Choose-the-Best-MFA-for-you.png) dhe ["Udhëzuesin për llojet e zakonshme të vërtetimit me dy faktorë në ueb” të EFF-së](https://www.eff.org/deeplinks/2017/09/guide-common-types-two-factor-authentication-web).
    - Mund të gjeni udhëzime për konfigurimin e vërtetimit me 2 faktorë në platformat kryesore në [12 ditët e 2FA: Si të aktivizoni vërtetimin me dy faktorë për llogaritë tuaja në internet](https://www.eff.org/deeplinks/2016/12/12-days-2fa-how-enable-two-factor-authentication-your-online-accounts).

- ** Hartëzoni praninë tuaj në internet**. Vetë-doksimi konsiston në eksplorimin e inteligjencës me burim të hapur mbi veten për të parandaluar aktorët keqdashës që të gjejnë dhe përdorin këtë informacion për t'ju imituar. Mësoni më shumë se si të kërkoni gjurmët tuaja në internet [Access Now Helpline's Guide's Guide to prevent doxing](https://guides.accessnow.org/self-doxing.html)
- **Mos pranoni mesazhe nga dërgues të panjohur.** Disa platforma të mesazheve, si WhatsApp, Signal ose Facebook Messenger, ju lejojnë të shikoni paraprakisht mesazhet përpara se të pranoni dërguesin si të besuar. Apple iMessage ju mundëson gjithashtu të ndryshoni cilësimet për të filtruar mesazhet nga dërguesit e panjohur. Asnjëherë mos e pranoni mesazhin ose kontaktin nëse ato duken të dyshimta ose nëse nuk e njihni dërguesin.

#### Resources

- [Access Now Helpline Community Documentation: Udhëzues për parandalimin e doxxing-ut](https://guides.accessnow.org/self-doxing.html)
- [Access Now Helpline Community Documentation: Pyetjet e shpeshta - Ngacmimi në internet që synon një anëtar të shoqërisë civile](https://communitydocs.accessnow.org/234-FAQ-Online_Harassment.html)
- [PEN America: Doracak për ngacmimin në internet](https://onlineharassmentfieldmanual.pen.org/)
- [Equality Labs: Udhëzues kundër doxxing-ut për aktivistët që përballen me sulme nga Alt-Right](https://medium.com/@EqualityLabs/anti-doxing-guide-for-activists-facing-attacks-from-the-alt-right-ec6c290f543c)
- [FemTechNet: Mbyllja e identitetit tuaj digjital](https://femtechnet.org/csov/lock-down-your-digital-identity/)
- [Rrjeti Kombëtar për t'i Dhënë Fund Dhunës në Familje: Këshilla për dokumentim për të mbijetuarit e abuzimit dhe ndjekjes përmes teknologjisë](https://www.techsafety.org/documentationtips)
