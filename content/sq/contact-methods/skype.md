---
layout: page
title: Skype
author: mfc
language: en
summary: Metodat e kontaktit
date: 2018-09
permalink: /en/contact-methods/skype.md
parent: /en/
published: true
---

Përmbajtja e mesazhit tuaj si dhe fakti që keni kontaktuar organizatën mund të jenë të aksesueshme nga qeveritë ose agjencitë e zbatimit të ligjit.